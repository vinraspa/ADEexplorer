import datetime
import math
import re
import sys
import typing
from collections import OrderedDict, defaultdict
from dataclasses import dataclass
from datetime import timedelta
from enum import Enum

import arrow
import pyexcel
# noinspection PyUnresolvedReferences
import pyexcel_ods3
# noinspection PyUnresolvedReferences
import pyexcel_xls
# noinspection PyUnresolvedReferences
import pyexcel_xlsx
import randomcolor
from PyQt5.QtChart import QChart, QChartView, QPieSeries, QPieSlice
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from arrow.arrow import Arrow

import pathsManagement as pM
from ADEcore import ADEcalendar, ADEevent, EventFilter
from eventWidget import EventWidget
from ui.oneDayGUI import Ui_OneDay
from ui.popupEventDetailsGUI import Ui_PopupEventDetails


def dest(o: QObject):
    return
    print("DESTROYED:", o.objectName())


def formatDuration_hhmm(duration: timedelta):
    hh, ss = divmod(duration.seconds, 3600)
    hh += duration.days * 24
    mm, ss = divmod(ss, 60)
    return f"{hh}h{mm:02d}" if mm else f"{hh}h"


def takeScreenShot(widget: QWidget):
    pix = widget.grab()

    dial = QDialog()
    dial.setWindowTitle("Agenda capturé")
    dial.setWindowModality(Qt.WindowModality.ApplicationModal)
    dial.setLayout(QVBoxLayout())
    lab = QLabel()
    lab.setPixmap(pix.scaled(1000, 200, Qt.AspectRatioMode.KeepAspectRatio))
    but_box = QDialogButtonBox(QDialogButtonBox.StandardButton.Save)
    but_box.addButton(QPushButton("Copier dans le presse-papier"), QDialogButtonBox.ButtonRole.RejectRole)
    but_box.accepted.connect(dial.accept)
    but_box.rejected.connect(dial.reject)

    dial.layout().addWidget(lab)
    dial.layout().addWidget(but_box)
    dial.layout().setSizeConstraint(QLayout.SizeConstraint.SetFixedSize)

    if dial.exec() == QDialog.DialogCode.Accepted:
        print('save')
        file, filter = QFileDialog.getSaveFileName(widget, "Sauvegarde de l'image", QDir.homePath(),
                                                   "Format png (*.png);;", "Format png (*.png);;")
        if not file.endswith('.png'):
            file += '.png'
        pix.save(file, 'png')
    else:
        clip = QGuiApplication.clipboard()
        clip.setPixmap(pix)
    dial.close()


class AgendaStyle(str, Enum):
    GRAPHIC = "Mode graphique"
    TABLE = "Mode liste"
    STATISTICS = "Mode statistiques"


class AutoClosingDialog(QMessageBox):
    def __init__(self, *args, **kwargs):
        super(AutoClosingDialog, self).__init__(*args, **kwargs)
        self.oneShotTimer = QTimer(self)
        self.oneShotTimer.setSingleShot(True)
        self.oneShotTimer.timeout.connect(self.close)

    def exec(self, timeOut: int = None) -> int:
        if timeOut:
            self.oneShotTimer.start(timeOut)
        return super(AutoClosingDialog, self).exec()

    def close(self) -> bool:
        super(AutoClosingDialog, self).close()


class PopupEventDetails(QDialog, Ui_PopupEventDetails):
    re_module_code = re.compile("\\b(M[\\d\\.?]+\\w{1,2}/?)\\b")
    # signalMoveEvent = pyqtSignal(EventDetails)
    signalSearchSameName = pyqtSignal(str)
    signalSearchSameModule = pyqtSignal(str)
    signalSearchSameFamily = pyqtSignal(str)
    copyModeSimple = "simple"
    copyModeSimpleMono = "simple_mono"
    copyModeFramedMono = "framed"
    copyModeHTML = "html"

    def __init__(self, event: ADEevent):
        super(PopupEventDetails, self).__init__()
        self.setParent(None)
        self.destroyed.connect(dest)
        self.event = event

        # Initialisation de la GUI
        self.setupUi(self)

        self.setWindowTitle("Détails de l'événement")
        self.menuSearchSame = QMenu(self)
        self.actionSearchSameName = QAction(f"... du même nom ({self.event.name})")
        self.actionSearchSameModule = QAction(f"... du même module ({self.event.module})")
        self.actionSearchSameFamily = QAction(f"... de la même famille (uid ADE {self.event.uidFamily})")
        self.actionSearchSameName.triggered.connect(lambda: self.signalSearchSameName.emit(self.event.name))
        self.actionSearchSameModule.triggered.connect(lambda: self.signalSearchSameModule.emit(self.event.module))
        self.actionSearchSameFamily.triggered.connect(lambda: self.signalSearchSameFamily.emit(self.event.uidFamily))
        self.menuSearchSame.addActions([self.actionSearchSameName,
                                        self.actionSearchSameModule,
                                        self.actionSearchSameFamily])
        if not self.event.name:
            self.actionSearchSameName.setDisabled(True)
        if not self.event.module:
            self.actionSearchSameModule.setDisabled(True)
        if not self.event.uidFamily:
            self.actionSearchSameFamily.setDisabled(True)

        self.pushButton_chercher_autres_seances.setMenu(self.menuSearchSame)
        self.pushButtonDeplacer.clicked.connect(self.moveTheEvent)
        self.pushButtonDeplacer.setDisabled(True)

        self.menuCopy = QMenu(self)
        self.copySimple = QAction("Copie simple")
        self.copySimpleMono = QAction("Copie simple pour font monospace")
        self.copyFramed = QAction("Copie avec cadre")
        self.copyHTML = QAction("Tableau HTML")
        self.copySimple.triggered.connect(lambda: self.copyToClipboard(self.copyModeSimple))
        self.copySimpleMono.triggered.connect(lambda: self.copyToClipboard(self.copyModeSimpleMono))
        self.copyFramed.triggered.connect(lambda: self.copyToClipboard(self.copyModeFramedMono))
        self.copyHTML.triggered.connect(lambda: self.copyToClipboard(self.copyModeHTML))
        self.menuCopy.addActions([self.copySimple, self.copySimpleMono, self.copyFramed, self.copyHTML])

        self.copierPushButton.setMenu(self.menuCopy)

        if event.name == "Créneau libre":
            self.pushButtonDeplacer.hide()
            self.copierPushButton.hide()
            self.pushButton_chercher_autres_seances.hide()
            self.UIDLineEdit.hide()
            self.UIDLabel.hide()

        # Écriture des données de l'événement dans le formulaire
        self.nomLineEdit.setText(self.event.nom)
        self.seanceLineEdit.setText(self.event.seance)
        self.commentairesTextEdit.setText(self.event.comments)
        self.typeLineEdit.setText(self.event.type)
        self.moduleLineEdit.setText(self.event.module)
        self.horaireLineEdit.setText(self.event.horaireDMYHm)
        self.dureeLineEdit.setText(self.event.dureeJJHHMM)
        self.lieuLineEdit.setText(self.event.location)
        self.participantsTextEdit.setText(self.event.participants)
        self.modifieLineEdit.setText(self.event.modifie)
        self.UIDLineEdit.setText(self.event.uid)

    def close(self):
        ADEevent.updateComment(self.event.uid, self.commentairesTextEdit.toPlainText())
        super(PopupEventDetails, self).close()

    def copyToClipboard(self, copy_type: str = None):
        clipboard = QApplication.clipboard()
        mime = QMimeData()
        match copy_type:
            case self.copyModeSimple:
                text = self.event.clipboard_simple
                mime.setText(text)
            case self.copyModeSimpleMono:
                text = self.event.clipboard_simple_mono
                mime.setText(text)
            case self.copyModeFramedMono:
                text = self.event.clipboard_framed_mono
                mime.setText(text)
            case self.copyModeHTML:
                text = self.event.clipboard_simple
                mime.setText(text)
                html = self.event.clipboard_html
                mime.setHtml(html)
            case _:
                return
        clipboard.setMimeData(mime)
        w = AutoClosingDialog()
        if mime.hasHtml():
            w.setText(mime.html())
        else:
            w.setText(mime.text())
        # w.setText('<tt>' + clipboard.mimeData().text().replace('\n','<br>').replace('\r', '').replace(" " , "&nbsp;") +'</tt>')
        w.setWindowFlags(Qt.Popup)
        w.exec(2000)

    def moveTheEvent(self):
        self.close()
        # self.signalMoveEvent.emit(self.event)


class PopupEventDetailsExtra(PopupEventDetails):
    def __init__(self, event: ADEevent):
        super(PopupEventDetailsExtra, self).__init__(event)
        self.participantsLabel.setText("Description :")


class TimeScaleBackground(QLabel):
    def __init__(self, begin_time: arrow.Arrow, end_time: arrow.Arrow, font: QFont, color: QColor = QColor(0, 0, 0)):
        super(QLabel, self).__init__()
        self.setParent(None)
        self.destroyed.connect(dest)
        self.begin_time = begin_time
        self.end_time = end_time
        self.color = color
        self.font = font
        self.beg = self.begin_time.time().hour + self.begin_time.time().minute / 60 + self.begin_time.time().second / 3600
        self.end = self.end_time.time().hour + self.end_time.time().minute / 60 + self.end_time.time().second / 3600

    def paintEvent(self, a0: QPaintEvent) -> None:
        slope = self.height() / ((self.end_time - self.begin_time).total_seconds() / 3600)
        heures = list(range(math.ceil(self.beg + 1e-5), math.ceil(self.end)))
        y_list = [int((t - self.beg) * slope) for t in heures]

        painter = QPainter(self)
        painter.setPen(QPen(self.color, 1))
        painter.setFont(self.font)
        for y, t in zip(y_list, heures):
            rectangle = QRect(0, y - 20, self.width(), 40)
            painter.drawText(rectangle, Qt.AlignmentFlag.AlignRight | Qt.AlignmentFlag.AlignVCenter, f"{t}h")


class TimeScale(QWidget, Ui_OneDay):
    def __init__(self, begin_time: arrow.Arrow, end_time: arrow.Arrow):
        super(TimeScale, self).__init__()
        self.setParent(None)
        self.destroyed.connect(dest)
        # Initialisation de la GUI
        self.setupUi(self)

        # Déclaration des variables d'instance
        self.beginDayTime = begin_time
        self.endDayTime = end_time

        # Définition des polices (dérivées de la police principale)
        self.timeFont = QApplication.font()
        self.timeFont.setPointSize(8)

        # La date est vide
        self.label_date.setText("")

        # Les titres de calendrier sont vides
        _label_cal_title = QLabel("")
        self.HLayout_calendarTitle.addWidget(_label_cal_title)

        label = TimeScaleBackground(self.beginDayTime, self.endDayTime, font=self.timeFont)
        label.setObjectName(f"TimeScaleBackground")
        self.gridLayout.addWidget(label, 0, 0, 1, 1)


class OneDayBackground(QLabel):
    def __init__(self, cols: int, begin_time: arrow.Arrow, end_time: arrow.Arrow,
                 color: QColor = QColor(255, 255, 255, 0), *args, **kwargs):  # d871b56f
        super(OneDayBackground, self).__init__(*args, **kwargs)
        self.setParent(None)
        self.destroyed.connect(dest)
        self.nbCols = cols
        self.begin_time = begin_time
        self.end_time = end_time
        self.color_base = color
        self.color_dark = QColor()
        self.color_dark.setHsl(
            self.color_base.getHsl()[0],
            self.color_base.getHsl()[1],
            self.color_base.getHsl()[2] - 5,
        )
        self.color_light = QColor()
        self.color_light.setHsl(
            self.color_base.getHsl()[0],
            self.color_base.getHsl()[1],
            self.color_base.getHsl()[2] + 10
        )
        self.beg = self.begin_time.time().hour + self.begin_time.time().minute / 60 + self.begin_time.time().second / 3600
        self.end = self.end_time.time().hour + self.end_time.time().minute / 60 + self.end_time.time().second / 3600

    def paintEvent(self, a0: QPaintEvent) -> None:
        slope = self.height() / ((self.end_time - self.begin_time).total_seconds() / 3600)
        heures = list(range(math.ceil(self.beg - 1e-5), math.floor(self.end + 1e-5)))
        y_list = [(t - self.beg) * slope for t in heures]

        painter = QPainter(self)
        painter.setPen(QPen(self.color_base, 1))

        grad = QLinearGradient(0, y_list[0], 0, y_list[0] + slope)
        grad.setColorAt(0, self.color_dark)
        grad.setColorAt(0.10, self.color_base)
        grad.setColorAt(0.90, self.color_base)
        grad.setColorAt(1, self.color_light)
        grad.setSpread(QGradient.Spread.RepeatSpread)
        painter.setBrush(grad)
        painter.drawRect(0, 0, self.width(), self.height())

        painter.setPen(QPen(self.color_light, 1))
        for x in range(1, self.nbCols):
            painter.drawLine(round(x * self.width() / self.nbCols), 0,
                             round(x * self.width() / self.nbCols), self.height())


class OneDay(QWidget, Ui_OneDay):
    # signalMoveEvent = pyqtSignal(EventDetails)
    signalOpenPreferences = pyqtSignal()

    def __init__(self, cal_list: list[ADEcalendar], date: Arrow | QDate, begin_time: QTime(), end_time: QTime(),
                 resolution_in_minutes: int = 15, is_holiday=False,
                 event_filter=EventFilter(), **kwargs):

        super(OneDay, self).__init__(**kwargs)
        self.setParent(None)
        self.destroyed.connect(dest)
        # Initialisation de la GUI
        self.setupUi(self)

        # Déclaration des variables d'instance
        self.date = arrow.get(date.toPyDate()) if isinstance(date, QDate) else date
        self.beginDayTime = begin_time
        self.endDayTime = end_time
        self.resolution = resolution_in_minutes
        self.nbCol = len(cal_list)
        self.nbRow = int((self.endDayTime - self.beginDayTime).total_seconds() / 60 / self.resolution)
        self.calList = cal_list
        self.countEvents: int = 0
        self.totalDuration = timedelta(seconds=0)
        self.eventFilter = event_filter
        self.isToday: bool = False

        # Définition des polices (dérivées de la police principale)
        self.calendarTitleFont = QApplication.font()
        self.calendarTitleFont.setPointSize(8)
        self.calendarTitleFont.setStyle(QFont.Style.StyleOblique)
        self.calendarTitleFont.setWeight(30)
        self.eventNameFont = QApplication.font()
        self.eventNameFont.setPointSize(8)
        self.eventWithCommentsNameFont = QApplication.font()
        self.eventWithCommentsNameFont.setPointSize(8)
        self.eventWithCommentsNameFont.setBold(True)
        self.dateFont = QApplication.font()

        # Définition des feuilles de style
        if is_holiday:
            self.background_color = QColor().fromHsl(10, 200, 200, 255)
        elif self.date.date() == arrow.Arrow.utcnow().date():
            self.background_color = QColor().fromHsl(170, 140, 160, 255)
            self.isToday = True
        else:
            self.background_color = QColor().fromHsl(84, 120, 180, 255)

        # On récupère chaque liste d'événements dans le 'subCalByDate' qui est un 'DefaultDict(lambda: list())'.
        # Si la clé n'existe pas, une 'list()' vide est créée (car 'DefaultDict()')
        # La clé est la date du jour au format donné par 'ADEcalendar.SubCalendarsByDateKeyFormat'.
        _key_date = self.date.format(ADEcalendar.date_key_format)
        self.eventsByDateList = [cal.events_sorted_by_date[_key_date] for cal in self.calList]

        _sizePolicy = QSizePolicy(QSizePolicy.Policy.Expanding, QSizePolicy.Policy.Expanding)
        _sizePolicy.setHorizontalStretch(1)
        _sizePolicy.setVerticalStretch(1)
        self.setSizePolicy(_sizePolicy)

        # Appel des fonctions d'initialisation de l'instance
        self.fillDate()
        self.fillCalTitles()
        self.prepareGrid()
        self.fillGridWithEvents()

    def fillDate(self) -> None:
        self.label_date.setText(self.date.format("ddd DD MMM YY", "fr_fr"))

    def fillCalTitles(self) -> None:
        # TODO: il y a un sérieux problème d'alignement des titres d'agendas lorsqu'ils sont nombreux !
        if len(self.calList) == 0:
            if sys.platform == "win32":
                _label_cal_title = QLabel("—", parent=self)
            else:
                _label_cal_title = QLabel("∅")
            _label_cal_title.setFont(self.calendarTitleFont)
            _label_cal_title.setAlignment(Qt.AlignmentFlag.AlignCenter)
            self.HLayout_calendarTitle.addWidget(_label_cal_title)
            return
        for cal in self.calList:
            try:
                if cal.altTitle:
                    _label_cal_title = QLabel(" " + cal.altTitle + " ")
                else:
                    _label_cal_title = QLabel(" " + cal.title + " ")
            except AttributeError:
                _label_cal_title = QLabel("")
            _label_cal_title.setFont(self.calendarTitleFont)
            _label_cal_title.setAlignment(Qt.AlignmentFlag.AlignCenter)
            self.HLayout_calendarTitle.addWidget(_label_cal_title)

    def prepareGrid(self) -> None:
        _sizePolicy = QSizePolicy()

        # On place d'abord un widget s'étendant sur toutes les cellules de la grille
        _grid_bkg_widget = OneDayBackground(parent=self.gridwidget,
                                            cols=self.nbCol,
                                            begin_time=self.beginDayTime,
                                            end_time=self.endDayTime,
                                            color=self.background_color)
        _grid_bkg_widget.setObjectName(f"OneDayBackground {self.date}")
        _grid_bkg_widget.setMinimumSize(1, 1)
        _sizePolicy = QSizePolicy(QSizePolicy.Policy.MinimumExpanding, QSizePolicy.Policy.Expanding)
        _sizePolicy.setHorizontalStretch(self.nbCol)
        _sizePolicy.setVerticalStretch(self.nbRow)
        # _sizePolicy.setHeightForWidth(_grid_bkg_widget.sizePolicy().hasHeightForWidth())
        _grid_bkg_widget.setSizePolicy(_sizePolicy)
        _grid_bkg_widget.setObjectName("gridBackground")
        _grid_bkg_widget.setEnabled(False)
        self.gridLayout.addWidget(_grid_bkg_widget, 0, 0, self.nbRow, self.nbCol)

    def fillGridWithEvents(self) -> None:
        _sizePolicy = QSizePolicy()
        _sizePolicy = QSizePolicy(QSizePolicy.Policy.MinimumExpanding, QSizePolicy.Policy.Expanding)
        _currCal_data, _lastCal_data = [dict()] * 2
        _todayEventDurationDictByUID = dict()
        _filtered_out: bool = False
        _widget_stack: list[EventWidget] = list()

        for calIndex, eventListByDate in enumerate(self.eventsByDateList):
            _lastCal_data = _currCal_data.copy()
            _currCal_data.clear()
            if not eventListByDate:
                continue
            for event in sorted(eventListByDate, key=lambda e: e.duration, reverse=True):
                assert isinstance(event, ADEevent)
                _filtered_out = not (self.eventFilter.isEnabled and self.eventFilter.isValid(event))

                if not _filtered_out:
                    _todayEventDurationDictByUID[event.uid] = event.duration
                # Si l'événement était présent dans la colonne précédente, ...
                if event.uid in _lastCal_data:
                    _currCal_data[event.uid] = _lastCal_data[event.uid].copy()  # ... on le récupère ...
                    _currEvt = _currCal_data[event.uid]
                    _currEvt["width"] += 1  # ... on l'élargit d'une unité ...
                    _sizePolicy = _currEvt["widgetRef"].sizePolicy()  # ... on ajuste sa sizePolicy ...
                    _HStretch = _sizePolicy.horizontalStretch() + 1
                    _sizePolicy.setHorizontalStretch(_HStretch)
                    _currEvt["widgetRef"].setSizePolicy(_sizePolicy)

                # Sinon on le crée
                else:
                    _evt_cut_off: bool = False
                    _evt_beg_h = int(arrow.get(event.begin).to(ADEcalendar.TimeZone).format('H'))
                    _evt_beg_m = int(arrow.get(event.begin).to(ADEcalendar.TimeZone).format('m'))
                    _evt_row = (_evt_beg_h * 60
                                + _evt_beg_m
                                - self.beginDayTime.time().hour * 60
                                - self.beginDayTime.time().minute) // self.resolution
                    _evt_nb_row = round(event.duration.total_seconds() / 60 / self.resolution)

                    # Si l'événement commence avant la 1re heure affichée, il faut :
                    # -- décaler le widget à l'index 0 (au lieu d'un index négatif)
                    # -- diminuer la taille du widget d'autant que le décalage
                    if _evt_row < 0:
                        _evt_nb_row += _evt_row
                        _evt_row = 0
                        # Enfin, si la fin est antérieure à la 1re heure (taille négative),
                        # on ne crée tout simplement pas le widget !
                        if _evt_nb_row <= 0:
                            continue

                    # Si l'événement finit après la dernière heure affichée, il faut
                    # diminuer la taille du widget
                    if _evt_row + _evt_nb_row >= self.nbRow:
                        _evt_cut_off = True
                        _evt_nb_row = self.nbRow - _evt_row
                        # Enfin, si la début est postérieure à la dernière heure (taille négative),
                        # on ne crée tout simplement pas le widget !
                        if _evt_nb_row <= 0:
                            continue

                    _evt_col = calIndex
                    _evt_nb_col: int = 1

                    # On crée le widget qui s'affichera
                    _evtWidget = EventWidget(None)
                    _evtWidget.setToolTip(event.tool_tip())
                    if event.color:
                        _evtWidget.setColor(QColor(event.color))

                    # On l'ajoute à la pile
                    _widget_stack.append(_evtWidget)

                    # On greffe l'événement en tant qu'attribut du widget... (pas propre je pense...)
                    _evtWidget.event = event
                    # ... afin que l'événement puisse être accédé depuis le slot 'popupEventInfo()'
                    # après avoir identifié le 'widget sender'.
                    _evtWidget.clicked.connect(self.popupEventInfo)

                    # on ajoute l'icône si nécessaire
                    if _evt_cut_off:
                        # TODO: add visual effect for cut-off events
                        pass
                    if event.name == "Créneau libre":
                        _evtWidget.isFreeTime = True
                        _evtWidget.setIcon(QIcon(pM.icon("mode_free")))
                        _evtWidget.setIconSize(QSize(48, 48))

                    if event.name != "Créneau libre":
                        _evtWidget.setText(event.widget_text)

                    _evtWidget.setObjectName(f"EventWidget {event.uid}")
                    if _filtered_out:
                        _evtWidget.setDisabled(True)
                    _evtWidget.setMinimumSize(1, 20)
                    if event.comments:
                        _evtWidget.setFont(self.eventWithCommentsNameFont)
                    else:
                        _evtWidget.setFont(self.eventNameFont)
                    _sizePolicy.setHorizontalStretch(1)
                    _sizePolicy.setVerticalStretch(1)
                    _sizePolicy.setHeightForWidth(_evtWidget.sizePolicy().hasHeightForWidth())
                    _evtWidget.setSizePolicy(_sizePolicy)

                    # On remplit le dictionnaire contenant les informations
                    # du widget associé à l'événement.
                    _currCal_data[event.uid] = {
                        "widgetRef": _evtWidget,
                        "row": _evt_row,
                        "col": _evt_col,
                        "length": _evt_nb_row,
                        "width": _evt_nb_col
                    }
                    _currEvt = _currCal_data[event.uid]

                # On place le widget dans la grille avec tous ses attributs
                self.gridLayout.addWidget(
                    _currEvt["widgetRef"],
                    _currEvt["row"],
                    _currEvt["col"],
                    _currEvt["length"],
                    _currEvt["width"],
                )

        # On fait les comptes du nombre d'événements distincts du jour et
        # de la durée totale
        self.countEvents = len(_todayEventDurationDictByUID)
        for duration in _todayEventDurationDictByUID.values():
            self.totalDuration += duration

        for i, w in enumerate(_widget_stack[:-1]):
            w.stacked_under = _widget_stack[i + 1]

    def popupEventInfo(self) -> None:
        event = self.sender().event
        assert isinstance(event, ADEevent)
        self.popup = PopupEventDetails(event) if event.isADEevent else PopupEventDetailsExtra(event)
        # self.popup.signalMoveEvent.connect(lambda details: self.signalMoveEvent.emit(details))
        self.popup.signalSearchSameName.connect(lambda val: self.searchSame('name', val))
        self.popup.signalSearchSameModule.connect(lambda val: self.searchSame('module', val))
        self.popup.signalSearchSameFamily.connect(lambda val: self.searchSame('family', val))
        self.popup.exec()

    def searchSame(self, what: str, value):
        result = ADEcalendar()
        match what:
            case 'name':
                for cal in self.calList:
                    for event in cal.events_sorted_by_name[value]:
                        result.events.add(event)
            case 'family':
                for cal in self.calList:
                    for event in cal.events_sorted_by_uid_family[value]:
                        result.events.add(event)
            case 'module':
                for cal in self.calList:
                    for event in cal.events_sorted_by_module[value]:
                        result.events.add(event)
            case _:
                return

        result.fillSortedEventDictionaries()
        w = OneAgendaTableView(cal_list=[result], displayed_columns=["Nom", "Module",
                                                                     "Date", "Début", "Fin",
                                                                     "Semaine", "Lieu",
                                                                     "Participants"])
        w.build()
        self.popup.agendaViewHolder.layout().takeAt(0)
        self.popup.agendaViewHolder.layout().addWidget(w)

        sizePolicy = self.popup.sizePolicy()
        sizePolicy.setVerticalStretch(10)
        self.popup.agendaViewHolder.setSizePolicy(sizePolicy)

    def contextMenuEvent(self, event):
        contextMenu = QMenu(self)
        openPreferences = contextMenu.addAction("Modifier les préférences")
        action = contextMenu.exec(self.mapToGlobal(event.pos()))
        if action == openPreferences:
            self.signalOpenPreferences.emit()


class OneWeek(QWidget):
    # signalMoveEvent = pyqtSignal(EventDetails)
    signalOpenPreferences = pyqtSignal()

    def __init__(self, cal_list: list[ADEcalendar], any_date_in_the_week: arrow.Arrow, begin_time: arrow.Arrow,
                 end_time: arrow.Arrow, resolution_in_minutes: int = 15, displayed_week_days: list = range(5),
                 holidays=None, event_filter=EventFilter(), **kwargs):
        super(OneWeek, self).__init__(**kwargs)

        self.setParent(None)
        self.destroyed.connect(dest)
        if holidays is None:
            holidays = []
        self.calList = cal_list
        self.aDateInWeek = any_date_in_the_week
        self.weekNumber = self.aDateInWeek.isocalendar()[1]
        self.monday = self.aDateInWeek.shift(days=-self.aDateInWeek.isoweekday() + 1)
        self.countEvents: int = 0
        self.totalDuration: timedelta = timedelta(seconds=0.0)
        self.containsToday: bool = False
        self.dayDict: dict[OneDay] = dict()

        v_box = QVBoxLayout(self)
        v_box.setContentsMargins(0, 0, 0, 0)
        v_box.setSpacing(0)

        widget = QWidget()
        v_box.addWidget(widget)

        h_box = QHBoxLayout(widget)
        h_box.setSpacing(0)
        h_box.setContentsMargins(0, 0, 0, 0)
        timeScale = TimeScale(begin_time, end_time)
        timeScale.setSizePolicy(QSizePolicy.Policy.Preferred, QSizePolicy.Policy.Expanding)
        timeScale.setFixedWidth(20)
        h_box.addWidget(timeScale)
        line = QFrame()
        line.setFrameShape(QFrame.Shape.VLine)
        line.setFrameShadow(QFrame.Shadow.Raised)
        h_box.addWidget(line)
        for weekDay in displayed_week_days:
            _the_day = self.monday.shift(days=weekDay)
            _Y = _the_day.date().year
            _M = _the_day.date().month
            _D = _the_day.date().day
            oneDay = OneDay(cal_list=self.calList, date=QDate(_Y, _M, _D), begin_time=begin_time, end_time=end_time,
                            resolution_in_minutes=resolution_in_minutes,
                            is_holiday=_the_day in holidays, event_filter=event_filter, parent=None)
            oneDay.setObjectName(f"OneDay {oneDay.date}")
            if oneDay.isToday:
                self.containsToday = True
            h_box.addWidget(oneDay)
            # oneDay.signalMoveEvent.connect(lambda details: self.signalMoveEvent.emit(details))
            oneDay.signalOpenPreferences.connect(self.signalOpenPreferences.emit)
            line = QFrame()
            line.setFrameShape(QFrame.Shape.VLine)
            line.setFrameShadow(QFrame.Shadow.Raised)
            h_box.addWidget(line)

            # On fait les comptes du nombre d'événements dictincts du jour et
            # de la durée totale
            self.countEvents += oneDay.countEvents
            self.totalDuration += oneDay.totalDuration

            self.dayDict[oneDay.date.timestamp] = oneDay
        self.setWindowTitle("Semaine " + str(self.aDateInWeek.isocalendar()[1]))


class OneAgendaGraphicView(QTabWidget):
    # TODO: Possibilité d'enrichir l'affichage avec des couleurs, en créant des
    #  filtres selon les différents critères déjà présents

    activeDate = arrow.Arrow.utcnow().shift(days=-arrow.Arrow.utcnow().isoweekday() + 1).timestamp()
    # signalMoveEvent = pyqtSignal(EventDetails)
    signalOpenPreferences = pyqtSignal()
    signalProgressMessage = pyqtSignal(int, str)
    signalMessage = pyqtSignal(str)

    def __init__(self, cal_list: list[ADEcalendar], begin_date: arrow.Arrow, end_date: arrow.Arrow,
                 begin_day_time: arrow.Arrow, end_day_time: arrow.Arrow, show_empty_weeks: bool = False,
                 displayed_week_days: list = None, holidays=None, event_filter=EventFilter(), **kwargs):
        super(OneAgendaGraphicView, self).__init__(**kwargs)

        self.setParent(None)
        self.destroyed.connect(dest)
        if holidays is None:
            holidays = []
        if displayed_week_days is None:
            displayed_week_days = list(range(5))
        self.calList = cal_list
        self.beginDate = begin_date
        self.endDate = end_date
        self.beginDayTime = begin_day_time
        self.endDayTime = end_day_time
        self.showEmptyWeeks = show_empty_weeks
        self.displayWeekDays = displayed_week_days
        self.holidays = holidays
        self.event_filter = event_filter
        self.countEvent: int = 0
        self.totalDuration = datetime.timedelta(seconds=0.0)
        self.weekDict: dict[OneWeek] = dict()
        self.setObjectName("oneAgenda")
        self._font = QApplication.font()
        self._font.setPointSize(8)
        self.setFont(self._font)

        sizePolicy = QSizePolicy(QSizePolicy.Policy.Expanding,
                                 QSizePolicy.Policy.Expanding)
        sizePolicy.setHorizontalStretch(1)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.sizePolicy().hasHeightForWidth())
        self.setSizePolicy(sizePolicy)

        self.setTabPosition(QTabWidget.TabPosition.North)
        self.setTabShape(QTabWidget.TabShape.Rounded)
        self.setDocumentMode(True)
        self.setTabsClosable(False)
        self.setMovable(False)

    def build(self):
        # Ce dictionnaire va contenir les widgets 'OneWeek'. La clé de chaque entrée est
        # le timestamp du lundi de la semaine concernée.
        firstWeekMonday = (self.beginDate - timedelta(days=self.beginDate.weekday()))
        lastWeekMonday = (self.endDate - timedelta(days=self.endDate.weekday()))
        mondays: list[arrow.Arrow] = list(arrow.Arrow.range('week', firstWeekMonday, lastWeekMonday))

        for idx, monday in enumerate(mondays):
            assert isinstance(monday, Arrow)
            self.signalProgressMessage.emit(int(100 * (1 + idx) / len(mondays)),
                                            f"Construction de la semaine {monday.isocalendar()[1]}")
            week = OneWeek(cal_list=self.calList, any_date_in_the_week=monday, begin_time=self.beginDayTime,
                           end_time=self.endDayTime, displayed_week_days=self.displayWeekDays, holidays=self.holidays,
                           event_filter=self.event_filter, parent=None)
            week.setObjectName(f"OneWeek {week.weekNumber}")
            week.setContentsMargins(0, 0, 0, 0)
            # week.signalMoveEvent.connect(lambda details: self.signalMoveEvent.emit(details))
            week.signalOpenPreferences.connect(self.signalOpenPreferences.emit)
            if week.countEvents or self.showEmptyWeeks:
                self.addTab(week, f"s{week.weekNumber}\n{week.countEvents}")
            if timedelta(seconds=0) <= arrow.Arrow.utcnow() - monday <= timedelta(days=7):
                self.tabBar().setTabTextColor(self.indexOf(week), QColor(0, 0, 255))
            self.setStyleSheet("QTabBar::tab:hover{border: 2px solid #C4C4C3;}"
                               "QTabBar::tab { height: 30; }")

            if week.countEvents:
                _tooltip = (f"Semaine {week.weekNumber}\n\n"
                            f"Événements : {week.countEvents}\n"
                            f"Durée cumulée : {formatDuration_hhmm(week.totalDuration)}")
            else:
                _tooltip = (f"Semaine {week.weekNumber}\n\n"
                            f"Aucun événement prévu.")
            self.tabBar().setTabToolTip(self.indexOf(week), _tooltip)

            self.update()
            self.countEvent += week.countEvents
            self.totalDuration += week.totalDuration
            self.weekDict[week.monday.timestamp()] = week

        # On cherche la clé la plus proche de la clé de l'instance précédente et on active le widget correspondant.
        try:
            self.setCurrentWidget(
                self.weekDict[min(self.weekDict.keys(), key=lambda x: abs(x - OneAgendaGraphicView.activeDate))])
        except ValueError:
            # Cette exception survient si aucun GRAPHIC n'était en mémoire (1er affichage)
            # Dans ce cas, on favorise une date proche de celle du jour
            pass
        self.currentTabChanged()
        self.currentChanged.connect(self.currentTabChanged)

    def currentTabChanged(self):
        if self.currentWidget():
            OneAgendaGraphicView.activeDate = self.currentWidget().monday.timestamp()
            week = self.currentWidget()
            if not isinstance(week, OneWeek):
                return
            self.signalMessage.emit(f"Semaine {week.weekNumber}" + " " * 10 + "—" + " " * 10 +
                                    f"Événements répertoriés : {week.countEvents}" + " " * 10 + "—" + " " * 10 +
                                    f"Durée cumulée : {formatDuration_hhmm(week.totalDuration)}")

    def close(self):
        self.deleteLater()

    def closeEvent(self, a0: QCloseEvent) -> None:
        self.close()


class OneAgendaTableModel(QAbstractTableModel):
    @dataclass
    class Column:
        header: str
        event_attr: str
        alignment: Qt.AlignmentFlag
        sort_key: typing.Callable

    # Préférences temporaires
    bkg_role_column = (None, None)

    def __init__(self, events: list[ADEevent]):
        super(OneAgendaTableModel, self).__init__()
        self._events: list[ADEevent] = events
        self.columns: list[OneAgendaTableModel.Column] = [
            self.Column(header='Nom',
                        event_attr='nom',
                        alignment=Qt.AlignmentFlag.AlignLeft,
                        sort_key=lambda event: event.name),
            self.Column(header='Agenda',
                        event_attr='calendar_name',
                        alignment=Qt.AlignmentFlag.AlignLeft,
                        sort_key=lambda event: event.calendar_name),
            self.Column(header='Séance',
                        event_attr='seance',
                        alignment=Qt.AlignmentFlag.AlignCenter,
                        sort_key=lambda event: event.seance),
            self.Column(header='Type',
                        event_attr='type',
                        alignment=Qt.AlignmentFlag.AlignCenter,
                        sort_key=lambda event: event.type),
            self.Column(header='Module',
                        event_attr='module',
                        alignment=Qt.AlignmentFlag.AlignHCenter,
                        sort_key=lambda event: event.module),
            self.Column(header='Jour',
                        event_attr='jour',
                        alignment=Qt.AlignmentFlag.AlignHCenter,
                        sort_key=lambda event: event.isoWeekDay),
            self.Column(header='Date',
                        event_attr='dateDMY',
                        alignment=Qt.AlignmentFlag.AlignHCenter,
                        sort_key=lambda event: event.begin),
            self.Column(header='Début',
                        event_attr='heure',
                        alignment=Qt.AlignmentFlag.AlignHCenter,
                        sort_key=lambda event: event.heure),
            self.Column(header='Fin',
                        event_attr='fin',
                        alignment=Qt.AlignmentFlag.AlignHCenter,
                        sort_key=lambda event: event.fin),
            self.Column(header='Durée',
                        event_attr='dureeJJHHMM',
                        alignment=Qt.AlignmentFlag.AlignHCenter,
                        sort_key=lambda event: event.dureeJJHHMM),
            self.Column(header='Semaine',
                        event_attr='isoWeekNumber',
                        alignment=Qt.AlignmentFlag.AlignHCenter,
                        sort_key=lambda event: int(event.isoWeekNumber)),
            self.Column(header='Lieu',
                        event_attr='location',
                        alignment=Qt.AlignmentFlag.AlignLeft,
                        sort_key=lambda event: event.location),
            self.Column(header='Participants',
                        event_attr='participantsOneLine',
                        alignment=Qt.AlignmentFlag.AlignLeft,
                        sort_key=lambda event: event.participantsOneLine)
        ]

    def headerData(self, section: int, orientation: Qt.Orientation, role: int = ...) -> typing.Any:
        if orientation == Qt.Orientation.Horizontal:
            if role == Qt.ItemDataRole.DisplayRole:
                return self.columns[section].header
            if role == Qt.ItemDataRole.DecorationRole:
                if section == self.bkg_role_column:
                    return QIcon(pM.icon("color_square.svg"))
            if role == Qt.ItemDataRole.TextAlignmentRole:
                return Qt.AlignmentFlag.AlignLeft
        if orientation == Qt.Orientation.Vertical:
            return super(OneAgendaTableModel, self).headerData(section, orientation, role)

    def data(self, index: QModelIndex, role: int = ...) -> typing.Any:
        row = index.row()
        col = index.column()
        if role == Qt.ItemDataRole.DisplayRole:
            return getattr(self._events[row], self.columns[col].event_attr)
        if role == Qt.ItemDataRole.TextAlignmentRole:
            return self.columns[col].alignment | Qt.AlignmentFlag.AlignBottom
        if role == Qt.ItemDataRole.ToolTipRole:
            return str(self._events[row].tool_tip())
        if role == Qt.ItemDataRole.BackgroundRole:
            ref_col, value = OneAgendaTableModel.bkg_role_column
            if ref_col is None:
                return
            assert isinstance(ref_col, int)
            if value is None:
                seed = getattr(self._events[row], self.columns[ref_col].event_attr) * 2
                return QColor(randomcolor.RandomColor(seed).generate(luminosity="light")[0])
            else:
                if getattr(self._events[row], self.columns[ref_col].event_attr) == value:
                    return QColor(Qt.GlobalColor.yellow)

    def sort(self, column: int, order: Qt.SortOrder = ...) -> None:
        self._events.sort(key=self.columns[column].sort_key, reverse=bool(order == Qt.SortOrder.DescendingOrder))
        self.layoutChanged.emit()

    def rowCount(self, parent: QModelIndex = ...) -> int:
        return len(self._events)

    def columnCount(self, parent: QModelIndex = ...) -> int:
        return len(self.columns)


class OneAgendaTableView(QTableView):
    # TODO: l'entête des colonnes pourrait directement servir de filtre, comme dans Calc ou Excel...

    signalProgressMessage = pyqtSignal(int, str)
    signalMessage = pyqtSignal(str)

    # Préférences temporaires
    temp_displayed_columns: list[str] = None
    temp_save_state: QByteArray = None

    def __init__(self, cal_list: list[ADEcalendar], begin_date: arrow.Arrow = None, end_date: arrow.Arrow = None,
                 event_filter=EventFilter(), displayed_columns=None, preferences_changed: bool = False, **kwargs):
        super(OneAgendaTableView, self).__init__()
        self.setAlternatingRowColors(True)
        self.verticalHeader().setSectionResizeMode(QHeaderView.ResizeMode.Fixed)
        self.verticalHeader().setDefaultSectionSize(10)
        self.nameFont = QApplication.font()
        self.nameFont.setPointSize(9)
        self.setFont(self.nameFont)
        self.setSortingEnabled(True)
        self.horizontalHeader().setSectionsMovable(True)
        self.setSelectionMode(QAbstractItemView.SelectionMode.ContiguousSelection)

        self.calList = cal_list
        self.beginDate = begin_date or self.getVeryFirstDate()
        self.endDate = end_date or self.getVeryLastDate()
        self.countEvent: int = 0
        self.totalDuration: datetime.timedelta = datetime.timedelta(seconds=0.0)
        self.eventFilter = event_filter
        self.setObjectName("oneAgenda")
        if (OneAgendaTableView.temp_displayed_columns is None) or preferences_changed:
            OneAgendaTableView.temp_displayed_columns = displayed_columns or []
        self.eventListByRow: list[ADEevent] = list()
        self.eventDicByUID: dict[ADEevent] = dict()

        self.setContextMenuPolicy(Qt.ContextMenuPolicy.CustomContextMenu)
        self.customContextMenuRequested.connect(self.tableCustomMenuRequested)
        self.horizontalHeader().setContextMenuPolicy(Qt.ContextMenuPolicy.CustomContextMenu)
        self.horizontalHeader().customContextMenuRequested.connect(self.headerCustomMenuRequested)

    def build(self):
        # Remplissage du tableau
        dates = list(Arrow.range('day', self.beginDate, self.endDate))
        _evt_count = 0
        for idx, date in enumerate(dates):
            self.signalProgressMessage.emit(int(100 * (1 + idx) / len(dates)),
                                            f"Traitement de la date {date.format('ddd DD MMM', 'fr_fr')}")
            if self.eventFilter.isEnabled and date.weekday() not in self.eventFilter.weekdays:
                continue
            for cal in self.calList:
                _key = date.format(ADEcalendar.date_key_format)
                for event in cal.events_sorted_by_date[_key]:
                    if event.uid in self.eventDicByUID.keys() \
                            or (not self.eventFilter.isContentValid(event)) \
                            or (not self.eventFilter.isAttendeesValid(event)) \
                            or (not self.eventFilter.isLocationValid(event)) \
                            or (not self.eventFilter.isDurationValid(event)):
                        continue
                    self.eventDicByUID[event.uid] = event
                    self.eventListByRow.append(event)

        self._model = OneAgendaTableModel(events=self.eventListByRow)
        self.setModel(self._model)
        self.model().layoutChanged.emit()
        self.horizontalHeader().sectionMoved.connect(self.saveState)
        self.horizontalHeader().sortIndicatorChanged.connect(self.saveState)

        self.countEvent = self._model.rowCount()
        self.totalDuration = datetime.timedelta(seconds=sum([e.duration.seconds for e in self._model._events]))

        self.resizeColumnsToContents()
        if OneAgendaTableView.temp_save_state:
            self.horizontalHeader().restoreState(OneAgendaTableView.temp_save_state)
        self.selectionChanged(QItemSelection(), QItemSelection())

    def saveState(self, *args, **kwargs):
        OneAgendaTableView.temp_save_state = self.horizontalHeader().saveState()

    def selectionChanged(self, selected: QItemSelection, unselected: QItemSelection):
        selected_rows: set[int] = set()
        for index in self.selectedIndexes():
            selected_rows.add(index.row())
        duration = timedelta(0)
        for row in selected_rows:
            duration += self._model._events[row].duration

        self.signalMessage.emit(f"{len(selected_rows)} événements sélectionnés, "
                                f"d'une durée totale de {formatDuration_hhmm(duration)}.")
        super(OneAgendaTableView, self).selectionChanged(selected, unselected)

    def getVeryFirstDate(self):
        return min([cal.firstLastDates()[0] for cal in self.calList]).floor('day')

    def getVeryLastDate(self):
        return max([cal.firstLastDates()[1] for cal in self.calList]).ceil('day')

    def copy(self, copy_all: bool = False):
        # Supposes a contiguousSelection mode!
        text = ""
        if not copy_all:
            cols = list(OrderedDict((index.column(), True) for index in self.selectionModel().selectedIndexes()).keys())
            rows = list(OrderedDict((index.row(), True) for index in self.selectionModel().selectedIndexes()).keys())
        else:
            cols = [self.horizontalHeader().logicalIndex(col) for col in range(self.model().columnCount())]
            rows = [self.verticalHeader().logicalIndex(row) for row in range(self.model().rowCount())]
            text += "\t".join([self.model().headerData(col, Qt.Horizontal, Qt.DisplayRole) for col in cols]) + '\n'

        # construction du texte à copier, ligne par ligne et colonne par colonne
        text += "\n".join([
            "\t".join([
                self.model().index(row, col).data(Qt.DisplayRole) for col in cols
            ]) for row in rows
        ])

        # enregistrement dans le clipboard
        QApplication.clipboard().setText(text)

    def save_to_spreadsheet(self):
        """"
        Sauve une copie complète du tableau aux formats ods ou xls ou xlsx.
        Toutes les cellules sont sauvées en tant que texte.
        """
        self.copy(copy_all=True)
        file_types = {'ods': 'Open Document',
                      'xlsx': 'Excel 2007-365',
                      'xls': 'Excel 97-2003'}
        filter_names = {f'{v} .{k} (*.{k})': k for k, v in file_types.items()}

        _file, _filter = QFileDialog.getSaveFileName(self, filter=';;'.join(filter_names.keys()),
                                                     directory=QDir.homePath())

        if not _file:
            return
        if not _file.endswith('.' + filter_names[_filter]):
            _file += '.' + filter_names[_filter]
        data = [row.split('\t') for row in QApplication.clipboard().text().split('\n')]
        for r in range(len(data)):
            for c in range(len(data[r])):
                try:
                    data[r][c] = int(data[r][c])
                except ValueError:
                    pass

        pyexcel.save_as(array=data, dest_file_name=_file)

        url = QUrl.fromLocalFile(_file)
        QDesktopServices.openUrl(url)

    def save_to_spreadsheet_datetime(self):
        """"
        Sauve les événements vers un fichier xlsx avec préservation des
        champs de type date, heure et durée.
        """
        file_types = {'ods': 'Open Document',
                      'xlsx': 'Excel 2007-365',
                      'xls': 'Excel 97-2003'}
        filter_names = {f'{v} .{k} (*.{k})': k for k, v in file_types.items()}

        _file, _filter = QFileDialog.getSaveFileName(self, filter=';;'.join(filter_names.keys()),
                                                     directory=QDir.homePath())

        if not _file:
            return
        if not _file.endswith('.' + filter_names[_filter]):
            _file += '.' + filter_names[_filter]

        data = [['Nom', 'Type', 'Module', 'Semaine', 'Début',
                 'Fin', 'Durée', 'Lieu', 'Participants']]
        tz = arrow.now(ADEcalendar.TimeZone).tzinfo
        for event in self.model()._events:
            assert isinstance(event, ADEevent)
            line = [event.name,
                    event.type,
                    event.module,
                    int(event.isoWeekNumber),
                    event.begin.astimezone(tz).replace(tzinfo=None),
                    event.end.astimezone(tz).replace(tzinfo=None),
                    event.duration,
                    event.location,
                    event.participantsOneLine,
                    ]
            data.append(line)

        sheet = pyexcel.Sheet(data)
        sheet.name_columns_by_row(0)
        sheet.save_as(filename=_file)

        url = QUrl.fromLocalFile(_file)
        QDesktopServices.openUrl(url)

    def keyPressEvent(self, event):
        if self.hasFocus():
            # Ctrl-C : copy
            if event.key() == Qt.Key_C and (event.modifiers() & Qt.ControlModifier):
                self.copy()
                event.accept()
            # Ctrl-E : export
            elif event.key() == Qt.Key_E and (event.modifiers() & Qt.ControlModifier):
                self.save_to_spreadsheet_datetime()
                event.accept()
            else:
                event.ignore()
        else:
            event.ignore()

    def tableCustomMenuRequested(self, pos: QPoint):
        index = self.indexAt(pos)
        if not index.isValid():
            return
        data = self.model().data(index, Qt.ItemDataRole.DisplayRole)
        data_elided = data if len(data) < 25 else data[:25]
        menu = QMenu(self)
        subMenuSaveSpreadSheet = QMenu("Sauver le tableau dans une feuille de calcul...", self)
        actionCopy = QAction("Copier la sélection", self)
        actionCopyAll = QAction("Copier tout le tableau", self)
        actionSaveSpreadsheet = QAction("Chaque cellule au format texte", self)
        actionSaveSpreadsheetDatetime = QAction("Champs date, heure et durée préservés", self)
        actionColorSame = QAction(f"Colorier les lignes de même valeur ({data_elided})", self)
        actionStopColor = QAction("Ne plus colorier les lignes", self)
        subMenuSaveSpreadSheet.setIcon(QIcon(pM.icon("spreadsheet.svg")))
        menu.addAction(actionCopy)
        menu.addAction(actionCopyAll)
        menu.addMenu(subMenuSaveSpreadSheet)
        subMenuSaveSpreadSheet.addAction(actionSaveSpreadsheet)
        subMenuSaveSpreadSheet.addAction(actionSaveSpreadsheetDatetime)
        menu.addSeparator()
        menu.addAction(actionColorSame)
        menu.addAction(actionStopColor)
        menu.popup(self.viewport().mapToGlobal(pos))
        actionCopy.triggered.connect(self.copy)
        actionCopyAll.triggered.connect(lambda: self.copy(copy_all=True))
        actionSaveSpreadsheet.triggered.connect(self.save_to_spreadsheet)
        actionSaveSpreadsheetDatetime.triggered.connect(self.save_to_spreadsheet_datetime)
        actionColorSame.triggered.connect(lambda: self.rowColoringChanged(index.column(), data))
        actionStopColor.triggered.connect(lambda: self.rowColoringChanged(None))

    def headerCustomMenuRequested(self, pos: QPoint):
        column_number = self.horizontalHeader().logicalIndexAt(pos)
        column_header = self.model().headerData(column_number, Qt.Orientation.Horizontal, Qt.ItemDataRole.DisplayRole)
        menu = QMenu(self)
        actionColorRowsStart = QAction(f"Colorier les lignes selon le contenu de la colonne {column_header}", self)
        actionColorRowsStart.setIcon(QIcon(pM.icon("color_square.svg")))
        actionColorRowsStop = QAction("Arrêter de colorier les lignes", self)
        menu.addAction(actionColorRowsStart)
        menu.addAction(actionColorRowsStop)
        menu.popup(self.viewport().mapToGlobal(pos))
        actionColorRowsStart.triggered.connect(lambda: self.rowColoringChanged(column_number))
        actionColorRowsStop.triggered.connect(lambda: self.rowColoringChanged(None))

    def rowColoringChanged(self, col, val=None):
        OneAgendaTableModel.bkg_role_column = (col, val)
        self.model().layoutChanged.emit()

    def close(self) -> bool:
        self.deleteLater()
        return True

    def closeEvent(self, a0: QCloseEvent) -> None:
        self.close()


class OneAgendaStatistics(QWidget):
    signalProgressMessage = pyqtSignal(int, str)
    signalMessage = pyqtSignal(str)
    critereBoxIndex: int = 0
    cumulBoxIndex: int = 0
    sortBoxIndex: int = 0
    animation: bool = True

    def __init__(self, cal_list: list[ADEcalendar], begin_date: arrow.Arrow = None, end_date: arrow.Arrow = None,
                 event_filter=EventFilter(), **kwargs):
        super(OneAgendaStatistics, self).__init__(**kwargs)
        self.event_filter = event_filter
        self.calList = cal_list
        self.countEvent = 0
        self.totalDuration = timedelta(seconds=0)

        comboHolder = QWidget(self)
        hbox = QHBoxLayout(comboHolder)
        hbox.setContentsMargins(0, 0, 0, 0)
        self.critereBox = QComboBox(self)
        self.critereBox.addItem("Nom d'événement", "name")
        self.critereBox.addItem("Module", "module")
        self.critereBox.addItem("Type", "type")
        self.critereBox.addItem("Lieu", "location")
        self.critereBox.addItem("Jour de la semaine", "jour")
        self.critereBox.addItem("Mois de l'année", "mois")
        self.critereBox.addItem("Semaine", "isoWeekNumber")
        self.critereBox.addItem("Durée", "dureeJJHHMM")
        self.critereBox.addItem("Participants", "participants_sorted")
        self.cumulBox = QComboBox(self)
        self.cumulBox.addItem("Durée", "duration")
        self.cumulBox.addItem("Nombre de séances", "number")
        self.sortBox = QComboBox(self)
        self.sortBox.addItem("Grandeur cumulée", "cumul")
        self.sortBox.addItem("Critère", "critere")

        line1 = QFrame(self)
        line1.setFrameShape(QFrame.Shape.VLine)
        line1.setFrameShadow(QFrame.Shadow.Sunken)
        line2 = QFrame(self)
        line2.setFrameShape(QFrame.Shape.VLine)
        line2.setFrameShadow(QFrame.Shadow.Sunken)

        self.animationBox = QCheckBox("Animation des graphiques", self)

        hbox.addWidget(QLabel("Critère :"))
        hbox.addWidget(self.critereBox)
        hbox.addWidget(line1)
        hbox.addWidget(QLabel("Grandeur cumulée :"))
        hbox.addWidget(self.cumulBox)
        hbox.addWidget(line2)
        hbox.addWidget(QLabel("Tri :"))
        hbox.addWidget(self.sortBox)

        hbox.addStretch(0)
        hbox.addWidget(self.animationBox)

        self.chartview = QChartView()
        self.chartview.setRenderHint(QPainter.RenderHint.Antialiasing)
        self.chartview.setContentsMargins(0, 0, 0, 0)
        # self.chartview.setRubberBand(QChartView.RectangleRubberBand)

        vbox = QVBoxLayout(self)
        vbox.setContentsMargins(0, 0, 0, 0)
        vbox.addWidget(comboHolder)
        vbox.addWidget(self.chartview)

        self.labelFont_normal = QApplication.font()
        self.labelFont_normal.setPointSize(8)
        self.labelFont_normal.setBold(False)
        self.labelFont_emph = QApplication.font()
        self.labelFont_emph.setPointSize(10)
        self.labelFont_emph.setBold(True)
        self.labelColor_normal = QColor(Qt.GlobalColor.black)
        self.labelColor_emph = QColor(Qt.GlobalColor.red)

        self.setLayout(vbox)
        self.critereBox.setCurrentIndex(OneAgendaStatistics.critereBoxIndex)
        self.cumulBox.setCurrentIndex(OneAgendaStatistics.cumulBoxIndex)
        self.sortBox.setCurrentIndex(OneAgendaStatistics.sortBoxIndex)
        self.animationBox.setChecked(OneAgendaStatistics.animation)
        self.critereBox.currentIndexChanged.connect(self.comboIndexChanged)
        self.cumulBox.currentIndexChanged.connect(self.comboIndexChanged)
        self.sortBox.currentIndexChanged.connect(self.comboIndexChanged)
        self.animationBox.stateChanged.connect(self.toggleAnimation)

    def build(self):
        cumul = self.cumulBox.currentData(Qt.ItemDataRole.UserRole)
        critere = self.critereBox.currentData(Qt.ItemDataRole.UserRole)
        events: dict[str, ADEevent] = dict()
        dic_slices: defaultdict[str, PieSlice] = defaultdict(lambda: PieSlice(cumul))

        # Création d'un dictionnaire d'événements UNIQUES identifiés par leur uid.
        for cal in self.calList:
            for event in cal.events:
                if self.event_filter.isValid(event):
                    events[event.uid] = event

        self.countEvent = len(events)
        self.totalDuration = timedelta(seconds=sum([event.duration.total_seconds() for event in events.values()]))

        # On passe chaque événement en revue et on l'ajoute à la liste de la bonne PieSlice.
        for event in events.values():
            dic_slices[getattr(event, critere)].addEvent(event)
        # On passe chaque PieSlice et on lui assigne le bon label.
        for label, slice_ in dic_slices.items():
            slice_.setLabel_(label)

        # On crée et paramètre une QPieSeries
        series = QPieSeries()
        series.setHoleSize(0.30)
        series.setPieSize(0.60)
        series.setPieStartAngle(80)
        series.setPieEndAngle(80 + 360 - 5)
        series.clicked.connect(self.sliceClicked)

        # On trie les PiesSlice et on les ajoute à notre QPieSeries
        match self.sortBox.currentData(Qt.ItemDataRole.UserRole):
            case 'critere':
                sorted_items = self.sort(dic_slices.items(), critere=critere)
            case 'cumul':
                sorted_items = self.sort(dic_slices.items(), cumul=cumul)
            case _:
                sorted_items = sorted(dic_slices.items())
        for label, slice_ in sorted_items:
            series.append(slice_)

        # create QChart object
        chart = QChart()
        if OneAgendaStatistics.animation:
            chart.setAnimationOptions(QChart.AnimationOption.SeriesAnimations)
        else:
            chart.setAnimationOptions(QChart.AnimationOption.NoAnimation)
        chart.setAnimationDuration(500)

        chart.addSeries(series)
        chart.setTheme(QChart.ChartTheme.ChartThemeLight)
        chart.legend().setVisible(False)

        # slices
        font = QApplication.font()
        font.setPointSize(8)
        for n, slice_ in enumerate(series.slices()):
            slice_.setLabelVisible(True)
            slice_.setLabelPosition(QPieSlice.LabelPosition.LabelOutside)
            slice_.setLabelFont(font)
            slice_.setBorderWidth(0)
            slice_.setLabelArmLengthFactor(0.5)
            slice_.setExplodeDistanceFactor(0.05)

        try:
            self.labelColor_normal = series.slices()[0].labelColor()
        except IndexError:
            pass

        # create QChartView object and add chart in thier
        self.chartview.setChart(chart)
        self.signalMessage.emit('')

    def sliceClicked(self, pie_slice: QPieSlice):
        series = pie_slice.series()
        if pie_slice.isExploded():
            for s in series.slices():
                self.unemphasize(s)
                self.signalMessage.emit('')
        else:
            other_slices = series.slices().copy()
            other_slices.remove(pie_slice)
            for s in other_slices:
                self.hide_(s)
            self.emphasize(pie_slice)
            self.signalMessage.emit(f"{len(pie_slice.events)} événements sélectionnés, "
                                    f"d'une durée totale de {formatDuration_hhmm(pie_slice.duration)}.")

    def emphasize(self, pie_slice: QPieSlice):
        pie_slice.setExploded(True)
        pie_slice.setLabelFont(self.labelFont_emph)
        pie_slice.setLabelColor(QColor(Qt.GlobalColor.red))

    def unemphasize(self, pie_slice: QPieSlice):
        pie_slice.setExploded(False)
        pie_slice.setLabelFont(self.labelFont_normal)
        pie_slice.setLabelColor(self.labelColor_normal)

    def hide_(self, pie_slice: QPieSlice):
        pie_slice.setExploded(False)
        pie_slice.setLabelFont(self.labelFont_normal)
        c = QColor(self.labelColor_normal)
        c.setAlphaF(0.5)
        pie_slice.setLabelColor(c)

    def comboIndexChanged(self):
        OneAgendaStatistics.cumulBoxIndex = self.cumulBox.currentIndex()
        OneAgendaStatistics.critereBoxIndex = self.critereBox.currentIndex()
        OneAgendaStatistics.sortBoxIndex = self.sortBox.currentIndex()
        self.build()

    def toggleAnimation(self, state: bool):
        OneAgendaStatistics.animation = state

    @staticmethod
    def sort(items: typing.ItemsView[str, typing.Any], critere: str = None, cumul: str = None):
        if critere:
            if critere == 'jour':
                return [tupl for x in ['lun.', 'mar.', 'mer.', 'jeu.', 'ven.', 'sam.'] for tupl in items if
                        tupl[0] == x]
            if critere == 'mois':
                return [tupl for x in ['septembre', 'octobre', 'novembre', 'décembre',
                                       'janvier', 'février', 'mars', 'avril',
                                       'mai', 'juin', 'juillet', 'août'] for tupl in items if tupl[0] == x]
            if critere == 'isoWeekNumber':
                return [tupl for x in list(range(34, 54)) + list(range(1, 34)) for tupl in items if int(tupl[0]) == x]
            return sorted(items, key=lambda item: item[0])

        if cumul:
            return sorted(items, key=lambda item: item[1].value())


class PieSlice(QPieSlice):
    def __init__(self, cumul: str):
        super(PieSlice, self).__init__()
        self.cumul = cumul
        self.events: list[ADEevent] = list()
        self.hovered.connect(self.hovered_)
        self.duration: timedelta = timedelta(seconds=0)

    def setLabel_(self, label):
        label = label or '∅'
        match self.cumul:
            case 'number':
                self.setLabel(f"{label} ×{int(self.value())}")
            case 'duration':
                self.setLabel(f"{label} : {formatDuration_hhmm(timedelta(seconds=self.value()))}")
            case _:
                self.setLabel(label)

    def addEvent(self, event: ADEevent):
        self.events.append(event)
        self.duration += event.duration
        if self.cumul == 'number':
            self.setValue(self.value() + 1)
        if self.cumul == 'duration':
            self.setValue(self.value() + event.duration.total_seconds())

    def hovered_(self, state: bool) -> None:
        text = self.label()
        QToolTip.showText(QCursor.pos(), text)

    def __repr__(self):
        return f"{self.label()} ({self.cumul}) : {len(self.events)} events. Value={self.value()}"
