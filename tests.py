#!/usr/bin/env python3

import itertools
import os
from getpass import getpass
from pprint import pprint
from xml.etree import cElementTree as ET

import openpyxl
import requests
from openpyxl import Workbook

from pyade import ADEWebAPI, Config



class cd:
    """Context manager for changing the current working directory"""

    def __init__(self, newPath):
        self.newPath = os.path.expanduser(newPath)

    def __enter__(self):
        self.savedPath = os.getcwd()
        os.chdir(self.newPath)

    def __exit__(self, etype, value, traceback):
        os.chdir(self.savedPath)


def downloadIcalsRes(b: int, e: int, project: int):
    for resource in range(b, e):
        url = (f'https://plannings.u-bourgogne.fr/jsp/custom/modules/plannings/anonymous_cal.jsp?resources={resource}'
               f'&projectId={project}&firstDate=2018-09-01&lastDate=2036-12-31&calType=ical')
        r = requests.get(url, allow_redirects=True)
        open(f'{resource}.ical', 'wb').write(r.content)
        print(f'{resource}.ical   ({round(100 * (resource - b) / (e - b), 2)} %)')


def downloadIcalsPro(b: int, e: int, resource: int):
    for project in range(b, e + 1):
        url = (f'https://plannings.u-bourgogne.fr/jsp/custom/modules/plannings/anonymous_cal.jsp?resources={resource}'
               f'&projectId={project}&firstDate=2020-09-01&lastDate=2021-08-31&calType=ical')
        r = requests.get(url, allow_redirects=True)
        open(f'{resource}-{project}.ical', 'wb').write(r.content)
        print(f'{resource}-{project}.ical   ({round(100 * (resource - b) / (e - b), 2)} %)')




def getSessionId(login: str, password: str):
    url = f'https://plannings.u-bourgogne.fr/jsp/webapi?function=connect&login={login}&password={password}'
    sessionId_xml = (requests.get(url, timeout=2).content.decode(encoding='utf8', errors='replace'))
    sessionId = ET.fromstring(sessionId_xml).attrib['id']
    return sessionId


def setProjectId(sessionId: str, projectId: int = 1) -> str:
    url = f'https://plannings.u-bourgogne.fr/jsp/webapi?sessionId={sessionId}&function=setProject&projectId={projectId}'
    setProject_xml = (requests.get(url, timeout=2).content.decode(encoding='utf8', errors='replace'))
    rep = ET.fromstring(setProject_xml)
    return rep.attrib


def extract_base(sessionId: str, search: str = "", details: int = 10) -> list:
    """
    Possible search fields:
        id, url, fatherName, fatherId, nbEventsPlaced, type, name, category, size, path, email
    If empty, returns all elements from the base.
    Examples:
          'id=78452'
    """
    if search:
        search = '&' + search
    url = f'https://plannings.u-bourgogne.fr/jsp/webapi?sessionId={sessionId}&function=getResources={search}&detail={details}'
    getResources_xml = (requests.get(url, timeout=20).content.decode(encoding='utf8', errors='replace'))
    resources = ET.fromstring(getResources_xml)

    ret = list()
    for child in resources:
        ret.append(child.attrib)
    return ret


def write_xls(data, filename: str):
    filename = filename or 'base.xlsx'
    if not filename.endswith('.xlsx'):
        filename = filename + '.xlsx'
    wb = Workbook(write_only=True)
    ws = wb.create_sheet()

    headers = list(set(itertools.chain.from_iterable(data)))
    ws.append(headers)

    for elements in data:
        ws.append([elements.get(h) for h in headers])

    wb.save(filename)


if __name__ == '__main__':
    with cd("~/tmp"):
        # defaultwebub
        # 4d16...
        while True:
            try:
                # sessionId = getSessionId(input("Login :"), getpass(prompt='Password: ', stream=None))
                sessionId = getSessionId('defaultwebub', '4d16pr1')
                break
            except KeyError:
                print("Bad login and/or password.")
        setProjectId(sessionId, 6)
        base = extract_base(sessionId,
                            search=input("Search (e.g. id=3860). Leave empty to download the whole database: "))
        write_xls(base, input("Filename. If empty, defaults to 'base.xlsx': "))