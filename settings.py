import os
import re
import typing
import urllib.parse

from PyQt5 import QtWidgets, QtCore
from PyQt5.QtCore import QSettings, pyqtSignal, QByteArray, pyqtBoundSignal
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import *

import pathsManagement as pM
from dateTimeWidgets import DateRangeWidget, TimeRangeWidget, DateRange, TimeRange


class NoSignalConnectedError(Exception):
    pass


class ADEsetting:
    qSettings = QSettings(pM.settings_file, QSettings.Format.IniFormat)

    def __init__(self, key: str, val_type: type, default_value: typing.Any, widget_type: type = None,
                 signal: pyqtSignal | pyqtBoundSignal = None):
        """
        Crée un nouveau paramètre identifié par sa clé (key) dont la valeur est de type val_type et la valeur par
        défaut est default_value.

        S'il est fourni, widget_type permet de créer un QWidget associé à la clé.

        S'il est fourni, le signal sera utilisé pour déclencher l'enregistrement de la valeur dans le fichier.
        S'il n'est pas fourni, un signal par défaut dépendant du type de QWidget sera utilisé à la place.

        :param key: clé du paramètre
        :param val_type:
        :param default_value:
        :param widget_type:
        :param signal:
        """
        self.default_value = default_value
        self.widget_type = widget_type
        self.widget = widget_type() if self.widget_type else None
        if self.widget_type is QGroupBox and val_type is bool:
            self.widget.setCheckable(True)
        self.val_type = val_type
        self.key = key
        self._value: typing.Any = default_value
        self.widget_signals: list[pyqtSignal] = list()

        # Identification des signaux pertinents marquant la fin de l'édition du widget
        # et ainsi l'enregistrement de la valeur
        if self.widget:
            if signal:
                self.widget_signals.append(signal)
            else:
                if isinstance(self.widget, (DateRangeWidget,
                                            TimeRangeWidget,
                                            QLineEdit,
                                            QSpinBox)):
                    self.widget_signals.append(self.widget.editingFinished)
                if isinstance(self.widget, QCheckBox):
                    self.widget_signals.append(self.widget.toggled)
                else:
                    raise NoSignalConnectedError()

        # Initialisation à partir de QSettings vers self._value puis QWidget
        self.from_qsettings()
        self.to_qwidget()

        # Connexion des signaux afin de propager la nouvelle valeur ...
        for signal in self.widget_signals:
            # ... du QWidget vers self._value ...
            signal.connect(self.from_qwidget)
            # ... **puis** de self._value vers QSettings (l'ordre de connexion est primordial)
            signal.connect(self.to_qsettings)

    def from_qwidget(self):
        if not self.widget:
            return
        if isinstance(self.widget, DateRangeWidget):
            assert isinstance(self._value, DateRange)
            self._value.begin = self.widget.beginDateEdit.date()
            self._value.end = self.widget.endDateEdit.date()
        if isinstance(self.widget, TimeRangeWidget):
            assert isinstance(self._value, TimeRange)
            self._value.begin = self.widget.beginTimeEdit.time()
            self._value.end = self.widget.endTimeEdit.time()
        if isinstance(self.widget, QAbstractButton):  # inherited by QCheckBox, QPushButton, QRadioButton, QToolButton
            assert isinstance(self._value, bool)
            self._value = self.widget.isChecked()
        if isinstance(self.widget, QGroupBox):
            assert isinstance(self._value, bool)
            self._value = self.widget.isChecked()
        if isinstance(self.widget, QLineEdit):
            assert isinstance(self._value, str)
            self._value = self.widget.text()
        if isinstance(self.widget, QSpinBox):  # Inherited by QDateTimeEdit, QDoubleSpinBox, QSpinBox
            assert isinstance(self._value, int)
            self._value = self.widget.value()
        if isinstance(self.widget, QComboBox):
            assert isinstance(self._value, int)
            self._value = self.widget.currentData()

    def to_qwidget(self):
        if not self.widget:
            return
        # On ne veut pas que le changement de valeur du widget se re-propage en sens inverse !
        # On bloque donc temporairement les signaux...
        self.widget.blockSignals(True)
        if isinstance(self.widget, DateRangeWidget):
            assert isinstance(self._value, DateRange)
            self.widget.beginDateEdit.setDate(self._value.begin)
            self.widget.endDateEdit.setDate(self._value.end)
        if isinstance(self.widget, TimeRangeWidget):
            assert isinstance(self._value, TimeRange)
            self.widget.beginTimeEdit.setTime(self._value.begin)
            self.widget.endTimeEdit.setTime(self._value.end)
        if isinstance(self.widget, QAbstractButton):  # inherited by QCheckBox, QPushButton, QRadioButton, QToolButton
            assert isinstance(self._value, bool)
            self.widget.setChecked(self._value)
        if isinstance(self.widget, QGroupBox):
            assert isinstance(self._value, bool)
            self.widget.setChecked(self._value)
        if isinstance(self.widget, QLineEdit):
            assert isinstance(self._value, str)
            self.widget.setText(self._value)
        if isinstance(self.widget, QSpinBox):
            assert isinstance(self._value, int)
            self.widget.setValue(self._value)
        if isinstance(self.widget, QComboBox):
            assert isinstance(self._value, int)
            for i in range(self.widget.count()):
                self.widget.setCurrentIndex(i)
                if self.widget.currentData() == self._value:
                    break
        # ... pour finalement les réactiver à la fin
        self.widget.blockSignals(False)

    def from_qsettings(self):
        if isinstance(self._value, (DateRange, TimeRange)):
            self._value.unserialize(ADEsetting.qSettings.value(self.key, self.default_value, str))
        else:
            self._value = ADEsetting.qSettings.value(self.key, self.default_value, self.val_type)

    def to_qsettings(self):
        if isinstance(self._value, (DateRange, TimeRange)):
            serialized = self._value.serialize
        else:
            serialized = self._value
        ADEsetting.qSettings.setValue(self.key, serialized)

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, val: typing.Any):
        """
        Updates self._value and propagate to QWidget and QSettings
        """
        try:
            assert isinstance(val, self.val_type)
        except AssertionError:
            print(type(val), self.val_type)
            raise
        self._value = val
        if self.widget:
            self.to_qwidget()
        self.to_qsettings()

    def __repr__(self):
        return self.key, str(self._value)

    def __hash__(self):
        return hash(self.key)


class ADEsettingSet:
    def __init__(self, ade_settings: typing.Iterable[ADEsetting] = None):
        self._key_setting_dict: dict[str, ADEsetting] = {}
        self._current_index: int
        if ade_settings:
            self.update(ade_settings)

    def update(self, new_settings: typing.Iterable[ADEsetting]):
        self._key_setting_dict |= {s.key: s for s in new_settings}

    def refresh_widgets(self):
        for setting in self.settings():
            setting.to_qwidget()

    def setting(self, key):
        return self._key_setting_dict[key]

    def widget(self, key):
        return self._key_setting_dict[key].widget

    def __getitem__(self, key):
        return self.setting(key)

    def __len__(self):
        return len(self._key_setting_dict)

    def __iter__(self):
        return iter(self._key_setting_dict.values())

    def keys(self):
        return self._key_setting_dict.keys()

    def items(self):
        return self._key_setting_dict.items()

    def settings(self):
        return self._key_setting_dict.values()


class VBoxLayout(QVBoxLayout):
    def __init__(self, *args, margins: int = 0, sep: int = 3, **kwargs):
        super(VBoxLayout, self).__init__(*args, **kwargs)
        self.setContentsMargins(*[margins] * 4)
        self.setSpacing(sep)


class HBoxLayout(QHBoxLayout):
    def __init__(self, *args, margins: int = 0, sep: int = 3, **kwargs):
        super(HBoxLayout, self).__init__(*args, **kwargs)
        self.setContentsMargins(*[margins] * 4)
        self.setSpacing(sep)


class HBoxWidget(QtWidgets.QWidget):
    def __init__(self, *args, margins: int = 0, sep: int = 3):
        super(HBoxWidget, self).__init__()
        self.setLayout(HBoxLayout(margins=margins, sep=sep))
        for arg in args:
            if isinstance(arg, QtWidgets.QWidget):
                self.layout().addWidget(arg)
            if isinstance(arg, int):
                self.layout().addStretch(arg)
            if isinstance(arg, QSpacerItem):
                self.layout().addSpacerItem(arg)


class FormLayout(QFormLayout):
    def __init__(self, *args, margins: int = 0, sep: int = 3, **kwargs):
        super(FormLayout, self).__init__(*args, **kwargs)
        self.setContentsMargins(*[margins] * 4)
        self.setVerticalSpacing(sep)


class SettingDialog(QDialog):
    def __init__(self):
        super(SettingDialog, self).__init__()
        self.setObjectName("Settings")
        self.setWindowTitle("Préférences")
        self.resize(850, 650)

        #        *** self ***
        # +------------------------+
        # |   self.centralWidget   |
        # +------------------------+
        # |     self.buttonBox     |
        # +------------------------+
        self.setLayout(VBoxLayout(margins=9, sep=6))
        self.centralWidget = QtWidgets.QWidget(self)
        self.buttonBox = QtWidgets.QDialogButtonBox(self)
        self.buttonBox.setOrientation(QtCore.Qt.Orientation.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.StandardButton.Cancel |
                                          QtWidgets.QDialogButtonBox.StandardButton.Ok |
                                          QtWidgets.QDialogButtonBox.StandardButton.Reset)
        self.buttonBox.button(
            QtWidgets.QDialogButtonBox.StandardButton.Reset).setText("Restaurer les réglages enregistrés")
        self.layout().addWidget(self.centralWidget)
        self.layout().addWidget(self.buttonBox)

        #       *** self.centralWidget ***
        # +-----------------+----------------------+
        # | self.listWidget | self.widget_settings |
        # +-----------------+----------------------+
        self.centralWidget.setLayout(HBoxLayout(margins=0, sep=3))
        self.listWidget = QtWidgets.QListWidget(self)
        self.listWidget.setFixedWidth(250)
        self.listWidget.setStyleSheet("QListView::item::enabled { height: 50px; }")
        self.widget_settings = QtWidgets.QWidget(self)
        self.centralWidget.layout().addWidget(self.listWidget)
        self.centralWidget.layout().addWidget(self.widget_settings)
        self.widget_settings.setLayout(VBoxLayout())

        self.settings = ADEsettingSet([
            ADEsetting("annee/annee", DateRange, DateRange(), DateRangeWidget, self.accepted),
            ADEsetting("annee/semestre1", DateRange, DateRange(), DateRangeWidget, self.accepted),
            ADEsetting("annee/semestre2", DateRange, DateRange(), DateRangeWidget, self.accepted),
            ADEsetting("vacances/noel", DateRange, DateRange(), DateRangeWidget, self.accepted),
            ADEsetting("vacances/toussaint", DateRange, DateRange(), DateRangeWidget, self.accepted),
            ADEsetting("vacances/hiver", DateRange, DateRange(), DateRangeWidget, self.accepted),
            ADEsetting("vacances/paques", DateRange, DateRange(), DateRangeWidget, self.accepted),

            ADEsetting("horaires/1", bool, True, QGroupBox, self.accepted),
            ADEsetting("horaires/2", bool, True, QGroupBox, self.accepted),
            ADEsetting("horaires/3", bool, True, QGroupBox, self.accepted),
            ADEsetting("horaires/4", bool, True, QGroupBox, self.accepted),
            ADEsetting("horaires/5", bool, True, QGroupBox, self.accepted),
            ADEsetting("horaires/6", bool, True, QGroupBox, self.accepted),
            ADEsetting("horaires/7", bool, False, QGroupBox, self.accepted),
            ADEsetting("horaires/1/AM", bool, True, QCheckBox, self.accepted),
            ADEsetting("horaires/2/AM", bool, True, QCheckBox, self.accepted),
            ADEsetting("horaires/3/AM", bool, True, QCheckBox, self.accepted),
            ADEsetting("horaires/4/AM", bool, True, QCheckBox, self.accepted),
            ADEsetting("horaires/5/AM", bool, True, QCheckBox, self.accepted),
            ADEsetting("horaires/6/AM", bool, True, QCheckBox, self.accepted),
            ADEsetting("horaires/7/AM", bool, False, QCheckBox, self.accepted),
            ADEsetting("horaires/1/PM", bool, True, QCheckBox, self.accepted),
            ADEsetting("horaires/2/PM", bool, True, QCheckBox, self.accepted),
            ADEsetting("horaires/3/PM", bool, True, QCheckBox, self.accepted),
            ADEsetting("horaires/4/PM", bool, True, QCheckBox, self.accepted),
            ADEsetting("horaires/5/PM", bool, True, QCheckBox, self.accepted),
            ADEsetting("horaires/6/PM", bool, False, QCheckBox, self.accepted),
            ADEsetting("horaires/7/PM", bool, False, QCheckBox, self.accepted),
            ADEsetting("horaires/1/AMtime", TimeRange, TimeRange(), TimeRangeWidget, self.accepted),
            ADEsetting("horaires/2/AMtime", TimeRange, TimeRange(), TimeRangeWidget, self.accepted),
            ADEsetting("horaires/3/AMtime", TimeRange, TimeRange(), TimeRangeWidget, self.accepted),
            ADEsetting("horaires/4/AMtime", TimeRange, TimeRange(), TimeRangeWidget, self.accepted),
            ADEsetting("horaires/5/AMtime", TimeRange, TimeRange(), TimeRangeWidget, self.accepted),
            ADEsetting("horaires/6/AMtime", TimeRange, TimeRange(), TimeRangeWidget, self.accepted),
            ADEsetting("horaires/7/AMtime", TimeRange, TimeRange(), TimeRangeWidget, self.accepted),
            ADEsetting("horaires/1/PMtime", TimeRange, TimeRange(), TimeRangeWidget, self.accepted),
            ADEsetting("horaires/2/PMtime", TimeRange, TimeRange(), TimeRangeWidget, self.accepted),
            ADEsetting("horaires/3/PMtime", TimeRange, TimeRange(), TimeRangeWidget, self.accepted),
            ADEsetting("horaires/4/PMtime", TimeRange, TimeRange(), TimeRangeWidget, self.accepted),
            ADEsetting("horaires/5/PMtime", TimeRange, TimeRange(), TimeRangeWidget, self.accepted),
            ADEsetting("horaires/6/PMtime", TimeRange, TimeRange(), TimeRangeWidget, self.accepted),
            ADEsetting("horaires/7/PMtime", TimeRange, TimeRange(), TimeRangeWidget, self.accepted),

            ADEsetting("affichage/1", bool, True, QCheckBox, self.accepted),
            ADEsetting("affichage/2", bool, True, QCheckBox, self.accepted),
            ADEsetting("affichage/3", bool, True, QCheckBox, self.accepted),
            ADEsetting("affichage/4", bool, True, QCheckBox, self.accepted),
            ADEsetting("affichage/5", bool, True, QCheckBox, self.accepted),
            ADEsetting("affichage/6", bool, True, QCheckBox, self.accepted),
            ADEsetting("affichage/7", bool, False, QCheckBox, self.accepted),
            ADEsetting("affichage/journeeHoraires", TimeRange, TimeRange(), TimeRangeWidget, self.accepted),
            ADEsetting("affichage/semainesVides", bool, True, QCheckBox, self.accepted),
            ADEsetting("affichage/journeesVides", bool, True, QCheckBox, self.accepted),

            ADEsetting("gestion/url_root", str, "", QLineEdit, self.accepted),
            ADEsetting("gestion/projectID", int, 0, QSpinBox, self.accepted),
            ADEsetting("gestion/telechargement/AllAtStartUp", bool, False, QRadioButton, self.accepted),
            ADEsetting("gestion/telechargement/IfNeeded", bool, True, QRadioButton, self.accepted),
            ADEsetting("gestion/peremption", int, 0, QComboBox, self.accepted),
            ADEsetting("gestion/no_warning", bool, False, QCheckBox, self.accepted),

            ADEsetting("ressources/file", str, "", QLineEdit, self.accepted),
            ADEsetting("ressources/separator", str, "", QLineEdit, self.accepted),

            ADEsetting("updates/auto", bool, True, QCheckBox, self.accepted),
            ADEsetting("updates/dev", bool, False, QCheckBox, self.accepted),

            ADEsetting("wizards/newYearOnOff", bool, True, QCheckBox, self.accepted),
            ADEsetting("wizards/newYearDays", int, 30, QSpinBox, self.accepted),

            ADEsetting("Window/geometry", QByteArray, b''),
            ADEsetting("Window/state", QByteArray, b''),
            ADEsetting("Window/displayExtraCalendars", bool, False, QCheckBox),

            ADEsetting("Version/version", str, ''),
        ])
        # Suppression d'anciennes clés obsolètes
        # (lignes introduites en version 1.4.2, à supprimer "plus tard"...)
        ADEsetting.qSettings.remove("ressources/headerCollegues")
        ADEsetting.qSettings.remove("ressources/headerGroupes")
        ADEsetting.qSettings.remove("ressources/headerSalles")
        ADEsetting.qSettings.remove("affichage/journeesVides")
        ADEsetting.qSettings.remove("updates/no_auto")

        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)
        self.buttonBox.clicked.connect(
            lambda button: self.settings.refresh_widgets()
            if self.buttonBox.buttonRole(button) == QDialogButtonBox.ButtonRole.ResetRole
            else ...)
        self.rejected.connect(self.settings.refresh_widgets)

        self.widget_CAT_annee: QtWidgets.QWidget = QtWidgets.QWidget()
        self.widget_CAT_horaires: QtWidgets.QWidget = QtWidgets.QWidget()
        self.widget_CAT_affichage_graphique: QtWidgets.QWidget = QtWidgets.QWidget()
        self.widget_CAT_gestion_agendas: QtWidgets.QWidget = QtWidgets.QWidget()
        self.widget_CAT_ressources: QtWidgets.QWidget = QtWidgets.QWidget()
        self.widget_CAT_mise_a_jour: QtWidgets.QWidget = QtWidgets.QWidget()
        self.widget_CAT_assistants: QtWidgets.QWidget = QtWidgets.QWidget()

        self.CAT_annee_semestres_vacances()
        self.CAT_affichage_graphique()
        self.CAT_mise_a_jour()
        self.CAT_horaires()
        self.CAT_ressources()
        self.CAT_gestion_agendas()
        self.CAT_assistants()

        self.categories: dict[str, tuple[QListWidgetItem, QtWidgets.QWidget]] = {
            "Gestion des agendas":
                (QListWidgetItem(QIcon(pM.icon("ADEicon.svg")), "Gestion des agendas"),
                 self.widget_CAT_gestion_agendas),
            "Ressources":
                (QListWidgetItem(QIcon(pM.icon("mode_simple.svg")), "Ressources"),
                 self.widget_CAT_ressources),
            "Année, semestres et vacances":
                (QListWidgetItem(QIcon(pM.icon("calendar.svg")), "Année, semestres et vacances"),
                 self.widget_CAT_annee),
            "Horaires travaillés":
                (QListWidgetItem(QIcon(pM.icon("clock.svg")), "Horaires travaillés"),
                 self.widget_CAT_horaires),
            "Affichage graphique":
                (QListWidgetItem(QIcon(pM.icon("affichage_graphique.svg")), "Affichage graphique"),
                 self.widget_CAT_affichage_graphique),
            "Mise à jour":
                (QListWidgetItem(QIcon(pM.icon('update.svg')), "Mise à jour"),
                 self.widget_CAT_mise_a_jour),
            "Assistants":
                (QListWidgetItem(QIcon(pM.icon('wizard_new_year.svg')), "Assistants"),
                 self.widget_CAT_assistants)
        }
        for item, widget in self.categories.values():
            self.listWidget.addItem(item)
            self.widget_settings.layout().addWidget(widget)
            widget.hide()

        self.listWidget.currentItemChanged.connect(self.refresh_settings_widget_content)
        self.listWidget.setCurrentRow(0)

    def selectCategory(self, cat: str):
        try:
            self.listWidget.setCurrentItem(self.categories[cat][0])
        except KeyError:
            pass

    def exec(self, cat: str = None):
        if cat:
            self.selectCategory(cat)
        super(SettingDialog, self).exec()

    def refresh_settings_widget_content(self, current: QListWidgetItem, previous: QListWidgetItem | None):
        if previous:
            self.categories[previous.text()][1].hide()
        self.categories[current.text()][1].show()

    def CAT_annee_semestres_vacances(self):
        #  *** self.widget_CAT_annee ***
        # +-------------+--------------+
        # |    left     |    right     |
        # +-------------+--------------+
        self.widget_CAT_annee.setLayout(HBoxLayout(margins=9, sep=25))
        left = QtWidgets.QWidget()
        right = QtWidgets.QWidget()
        self.widget_CAT_annee.layout().addWidget(left)
        self.widget_CAT_annee.layout().addWidget(right)
        self.widget_CAT_annee.layout().addStretch(1)

        #       left    |    right
        # +------------------------+
        # |    année    | toussaint|
        # +------------------------+
        # |  semestre1  |   Noël   |
        # +------------------------+
        # |  semestre2  |   hiver  |
        # +------------------------+
        #               |  Pâques  |
        #               +----------+
        left.setLayout(VBoxLayout(sep=15))
        for key, title in [("annee/annee", "Année universitaire"),
                           ("annee/semestre1", "Semestre 1"),
                           ("annee/semestre2", "Semestre 2")]:
            left.layout().addWidget(self.settings.widget(key))
            self.settings.widget(key).setBoxTitle(title)
        left.layout().addStretch(1)

        right.setLayout(VBoxLayout(sep=15))
        for key, title in [("vacances/toussaint", "Vacances de la Toussaint"),
                           ("vacances/noel", "Vacances de Noël"),
                           ("vacances/hiver", "Vacances d'hiver"),
                           ("vacances/paques", "Vacances de Pâques")]:
            right.layout().addWidget(self.settings.widget(key))
            self.settings.widget(key).setBoxTitle(title)
        right.layout().addStretch(1)

    def CAT_horaires(self):
        self.widget_CAT_horaires.setLayout(VBoxLayout(margins=9, sep=9))
        for key, title in [("horaires/1", "Lundi"),
                           ("horaires/2", "Mardi"),
                           ("horaires/3", "Mercredi"),
                           ("horaires/4", "Jeudi"),
                           ("horaires/5", "Vendredi"),
                           ("horaires/6", "Samedi"),
                           ("horaires/7", "Dimanche")]:
            assert isinstance(self.settings.widget(key), QGroupBox)
            groupBox: QGroupBox = self.settings.widget(key)
            self.widget_CAT_horaires.layout().addWidget(groupBox)
            groupBox.setTitle(title)
            groupBox.setLayout(HBoxLayout(margins=3, sep=0))
            groupBox.layout().addWidget(self.settings.widget(key + '/AM'))
            self.settings.widget(key + '/AM').setText('Matin :')
            groupBox.layout().addWidget(self.settings.widget(key + '/AMtime'))
            self.settings.widget(key + '/AMtime').beginLabel.setText("")
            self.settings.widget(key + '/AMtime').endLabel.setText("→")
            groupBox.layout().addSpacerItem(QtWidgets.QSpacerItem(30, 0))
            groupBox.layout().addWidget(self.settings.widget(key + '/PM'))
            self.settings.widget(key + '/PM').setText('Après-midi :')
            groupBox.layout().addWidget(self.settings.widget(key + '/PMtime'))
            self.settings.widget(key + '/PMtime').beginLabel.setText("")
            self.settings.widget(key + '/PMtime').endLabel.setText("→")
            self.widget_CAT_horaires.layout().addSpacing(5)
        self.widget_CAT_horaires.layout().addStretch(1)

    def CAT_affichage_graphique(self):
        self.widget_CAT_affichage_graphique.setLayout(VBoxLayout(margins=9, sep=25))
        gB = QtWidgets.QGroupBox("Jours de la semaine affichés")
        gB.setLayout(QtWidgets.QGridLayout())
        gB.setMaximumWidth(400)
        for count, (key, title) in enumerate([("affichage/1", "Lundi"),
                                              ("affichage/2", "Mardi"),
                                              ("affichage/3", "Mercredi"),
                                              ("affichage/4", "Jeudi"),
                                              ("affichage/5", "Vendredi"),
                                              ("affichage/6", "Samedi"),
                                              ("affichage/7", "Dimanche")]):
            cB = self.settings.widget(key)
            cB.setText(title)
            gB.layout().addWidget(cB, count % 2, int(count / 2))
        self.widget_CAT_affichage_graphique.layout().addWidget(gB)

        gB = QtWidgets.QGroupBox("Plage horaire affichée")
        gB.setLayout(HBoxLayout(margins=9))
        gB.layout().addWidget(self.settings.widget("affichage/journeeHoraires"))
        gB.setMaximumWidth(400)
        self.widget_CAT_affichage_graphique.layout().addWidget(gB)

        self.settings.widget("affichage/semainesVides").setText('Afficher les semaines vides')
        self.widget_CAT_affichage_graphique.layout().addWidget(self.settings.widget("affichage/semainesVides"))
        self.widget_CAT_affichage_graphique.layout().addStretch(1)

    def CAT_gestion_agendas(self):
        self.widget_CAT_gestion_agendas.setLayout(FormLayout(margins=9, sep=25))
        form_layout = self.widget_CAT_gestion_agendas.layout()
        assert isinstance(form_layout, FormLayout)
        form_layout.addRow(QLabel("URL de téléchargement des agendas :"),
                           self.settings.widget('gestion/url_root'))
        form_layout.addRow(QLabel("ProjectId de l'année en cours :"),
                           self.settings.widget('gestion/projectID'))
        self.settings.widget('gestion/url_root').editingFinished.connect(self.sanitizeUrl_and_getProjectId)
        gB = QGroupBox("Téléchargement des agendas")
        gB.setLayout(VBoxLayout(margins=9, sep=6))
        gB.layout().addWidget(self.settings.widget('gestion/telechargement/AllAtStartUp'))
        self.settings.widget('gestion/telechargement/AllAtStartUp').setText(
            "Télécharger les agendas au démarrage de l'application")
        gB.layout().addWidget(HBoxWidget(self.settings.widget('gestion/telechargement/IfNeeded'),
                                         self.settings.widget('gestion/peremption'),
                                         1))
        self.settings.widget('gestion/telechargement/IfNeeded').setText(
            "Télécharger les agendas à l'usage")
        bB = QButtonGroup(gB)
        bB.addButton(self.settings.widget('gestion/telechargement/AllAtStartUp'))
        bB.addButton(self.settings.widget('gestion/telechargement/IfNeeded'))
        form_layout.addRow(gB)
        combo = self.settings.widget('gestion/peremption')
        combo.addItem("À chaque nouvelle sélection de la ressource (déconseillé)", userData=0)
        combo.addItem("S'il date de plus de 15 minutes", userData=15 * 60)
        combo.addItem("S'il date de plus d'une heure", userData=3600)
        combo.addItem("S'il date de plus d'un jour", userData=86400)
        combo.addItem("S'il date de plus d'une semaine", userData=86400 * 7)
        self.settings['gestion/peremption'].to_qwidget()

        form_layout.addRow(self.settings.widget('gestion/no_warning'))
        self.settings.widget('gestion/no_warning').setText("Ne plus afficher les messages d'erreur concernant le "
                                                           "téléchargement des agendas.")

    def CAT_ressources(self):
        self.widget_CAT_ressources.setLayout(FormLayout(margins=9, sep=25))
        bt = QtWidgets.QPushButton()
        bt.setIcon(QApplication.style().standardIcon(QStyle.StandardPixmap.SP_DirIcon))
        bt.setFixedSize(28, 28)
        bt.clicked.connect(self.fileDial)
        self.widget_CAT_ressources.layout().addRow(QLabel("Chemin d'accès vers le fichier ressources (.csv)"),
                                                   HBoxWidget(self.settings.widget('ressources/file'),
                                                              bt))
        self.widget_CAT_ressources.layout().addRow(QLabel("Séparateur de champs du fichier csv"),
                                                   self.settings.widget('ressources/separator'))
        self.settings.widget('ressources/file').textChanged.connect(self.ressourceFileChanged)

    def CAT_mise_a_jour(self):
        self.widget_CAT_mise_a_jour.setLayout(VBoxLayout(margins=9, sep=25))
        self.widget_CAT_mise_a_jour.layout().addWidget(self.settings.widget('updates/auto'))
        self.settings.widget('updates/auto').setText("Activer la recherche automatique des mises à jour au démarrage.")
        self.widget_CAT_mise_a_jour.layout().addWidget(self.settings.widget('updates/dev'))
        self.settings.widget('updates/dev').setText("Inclure les versions en développement (déconseillé).")
        self.widget_CAT_mise_a_jour.layout().addStretch(1)
        self.settings.widget('updates/dev').toggled.connect(
            lambda state: QMessageBox.warning(self, "Mise en garde",
                                              "Attention, en cochant cette case, vous recevrez des mises à jour en "
                                              "cours de développement, donc potentiellement instables. Ne cochez cette "
                                              "case que si vous savez exactement ce que vous faites.") if state
            else ...)

    def CAT_assistants(self):
        self.widget_CAT_assistants.setLayout(VBoxLayout(margins=9, sep=15))
        self.widget_CAT_assistants.layout().addWidget(self.settings.widget('wizards/newYearOnOff'))
        self.widget_CAT_assistants.layout().addWidget(w := HBoxWidget(QSpacerItem(15, 0),
                                                                      self.settings.widget('wizards/newYearDays'),
                                                                      QLabel("jours avant la fin de l'année en cours"),
                                                                      1))
        self.settings.widget('wizards/newYearOnOff').setText("Proposer l'assistant de migration vers une nouvelle "
                                                             "année universitaire au démarrage")
        self.settings.widget('wizards/newYearOnOff').toggled.connect(w.setEnabled)
        w.setEnabled(self.settings['wizards/newYearOnOff'].value)
        self.settings.widget('wizards/newYearDays').setMaximum(90)
        self.widget_CAT_assistants.layout().addStretch(1)

    def fileDial(self):
        dialog = QFileDialog()
        dialog.setDirectory(pM.exe_dir)
        dialog.setNameFilter("Texte CSV (*.csv)")
        if dialog.exec():
            self.settings.widget('ressources/file').setText(os.path.relpath(path=dialog.selectedFiles()[0],
                                                                            start=pM.exe_dir))

    def ressourceFileChanged(self):
        _file_full_path = os.path.join(pM.exe_dir, self.settings.widget('ressources/file').text())
        if os.path.isfile(_file_full_path):
            self.settings.widget('ressources/file').setStyleSheet(
                "color: green;"
            )
        else:
            self.settings.widget('ressources/file').setStyleSheet(
                "color: red;"
                "background-color: yellow;"
                "selection-color: yellow;"
                "selection-background-color: red;"
            )

    def sanitizeUrl_and_getProjectId(self):
        """
        Ne conserve que le "scheme" et le "netloc" de l'url de téléchargement des fichiers ics.
        Si un projectId est détecté, il est mis à jour sur l'interface.
        """
        regex = re.compile(r'projectId=\d+')
        urlparsed = urllib.parse.urlparse(self.settings.widget('gestion/url_root').text())
        for frag in urlparsed.query.split('&'):
            if regex.match(frag):
                self.settings.widget('gestion/projectID').setValue(int(regex.match(frag).string.split('=')[1]))
        urlparsed = urlparsed._replace(path='')
        urlparsed = urlparsed._replace(params='')
        urlparsed = urlparsed._replace(query='')
        urlparsed = urlparsed._replace(fragment='')
        self.settings.widget('gestion/url_root').setText(urlparsed.geturl())


if __name__ == '__main__':
    app = QApplication([])
    settings = SettingDialog()
    settings.show()
    # for debut, fin in [(settings.ade_settings[key].value.begin,
    #                     settings.ade_settings[key].value.end) for key in settings.ade_settings.keys()
    #                    if key.startswith('vacances/')]:
    #     print(arrow.Arrow.range('day', debut.replace(tzinfo="Europe/Paris"), fin.replace(tzinfo="Europe/Paris")))
    app.exec()
