# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/home/vincent/STORAGE_V/Seafile/ADEexplorer/src/ui/oneDayGUI.ui'
#
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_OneDay(object):
    def setupUi(self, OneDay):
        OneDay.setObjectName("OneDay")
        OneDay.resize(337, 479)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(OneDay.sizePolicy().hasHeightForWidth())
        OneDay.setSizePolicy(sizePolicy)
        self.verticalLayout = QtWidgets.QVBoxLayout(OneDay)
        self.verticalLayout.setSizeConstraint(QtWidgets.QLayout.SetDefaultConstraint)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setSpacing(0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.label_date = QtWidgets.QLabel(OneDay)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_date.sizePolicy().hasHeightForWidth())
        self.label_date.setSizePolicy(sizePolicy)
        self.label_date.setAlignment(QtCore.Qt.AlignCenter)
        self.label_date.setObjectName("label_date")
        self.verticalLayout.addWidget(self.label_date)
        self.HLayout_calendarTitle = QtWidgets.QHBoxLayout()
        self.HLayout_calendarTitle.setSpacing(0)
        self.HLayout_calendarTitle.setObjectName("HLayout_calendarTitle")
        self.verticalLayout.addLayout(self.HLayout_calendarTitle)
        self.gridwidget = QtWidgets.QWidget(OneDay)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.gridwidget.sizePolicy().hasHeightForWidth())
        self.gridwidget.setSizePolicy(sizePolicy)
        self.gridwidget.setObjectName("gridwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.gridwidget)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setSpacing(0)
        self.gridLayout.setObjectName("gridLayout")
        self.verticalLayout.addWidget(self.gridwidget)

        self.retranslateUi(OneDay)
        QtCore.QMetaObject.connectSlotsByName(OneDay)

    def retranslateUi(self, OneDay):
        _translate = QtCore.QCoreApplication.translate
        OneDay.setWindowTitle(_translate("OneDay", "Form"))
        self.label_date.setText(_translate("OneDay", "date"))
