import datetime
import os
import sys
import typing

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from anytree import PreOrderIter
from arrow import arrow

import pathsManagement as pM
from ADEcore import ADEcalendar
from calendrierScolaireFrancais import SchoolCalendar
from dateTimeWidgets import DateRangeWidget, DateRange
from resourceTreeStructure import ResourceTreeStructure, ResourceNode
from settings import SettingDialog, HBoxWidget


def annee_int_to_str(year: int) -> str:
    return str(year) + '-' + str(year + 1)


def HLine(parent) -> QFrame:
    line = QFrame(parent)
    line.setFrameShape(QFrame.Shape.HLine)
    line.setFrameShadow(QFrame.Shadow.Sunken)
    return line


class ProjectIdNotFoundException(Exception):
    pass


class NewYearWizard(QWizard):
    FIELD_YEAR_STR = 'year_str'
    FIELD_YEAR_INT = 'year_int'
    FIELD_ZONE = 'zone'
    FIELD_YEAR_BEGIN = 'year_begin'
    FIELD_YEAR_END = 'year_end'
    FIELD_S1_BEGIN = 's1_begin'
    FIELD_S1_END = 's1_end'
    FIELD_S2_BEGIN = 's2_begin'
    FIELD_S2_END = 's2_end'
    FIELD_TOUSSAINT_BEGIN = 'toussaint_begin'
    FIELD_TOUSSAINT_END = 'toussaint_end'
    FIELD_NOEL_BEGIN = 'noel_begin'
    FIELD_NOEL_END = 'noel_end'
    FIELD_HIVER_BEGIN = 'hiver_begin'
    FIELD_HIVER_END = 'hiver_end'
    FIELD_PAQUES_BEGIN = 'paques_begin'
    FIELD_PAQUES_END = 'paques_end'
    FIELD_PROJECTID = 'project_id'

    ZONES = ("", "Zone A", "Zone B", "Zone C")

    def __init__(self, preferences: SettingDialog, resourceTreeStructure: ResourceTreeStructure):
        super(NewYearWizard, self).__init__()
        self.resourceTreeStructure = resourceTreeStructure
        self.preferences = preferences

        self.setWizardStyle(QWizard.WizardStyle.ModernStyle)
        self.setPixmap(QWizard.WizardPixmap.WatermarkPixmap, QPixmap(pM.pix('new_year.png')).scaledToWidth(200))
        self.setPixmap(QWizard.WizardPixmap.LogoPixmap, QPixmap(pM.icon('ADE_icon.svg')))
        self.addPage(WelcomePage(self))
        self.addPage(Annee_Semestres(self))
        self.addPage(Vacances(self))
        self.addPage(ProjectId(self))
        self.setWindowTitle("Assistant de migration vers une nouvelle année universitaire")

        page_sizes = list()
        for page_id in self.pageIds():
            self.page(page_id).adjustSize()
            page_sizes.append(s := self.size())
        size = max(page_sizes, key=lambda size: (size.width(), size.height()))
        self.setFixedSize(size + QSize(120, 50))

    def close(self) -> None:
        self.reject()

    def reject(self) -> None:
        rep = QMessageBox.question(self, "Assistant de migration",
                                   "Êtes-vous sûr de vouloir quitter le processus ? Aucun des réglages effectués ne "
                                   "sera sauvegardé. Vous pourrez relancer cet assistant plus tard.",
                                   QMessageBox.StandardButton.No | QMessageBox.StandardButton.Yes)
        if rep == QMessageBox.StandardButton.Yes:
            super(NewYearWizard, self).reject()

    def accept(self) -> None:
        rep = QMessageBox.question(self, "Assistant de migration",
                                   "<p>Êtes-vous sûr de vouloir valider ces réglages ?</p>"
                                   "<p>Si vous voulez modifier ce paramétrage par la suite, vous pourrez toujours "
                                   "relancer cet assistant ou modifier les paramètres dans le menu "
                                   "<tt>Édition > Préférences</tt>.</p>",
                                   QMessageBox.StandardButton.No | QMessageBox.StandardButton.Yes,
                                   QMessageBox.StandardButton.Yes)
        if rep == QMessageBox.StandardButton.Yes:
            super(NewYearWizard, self).accept()

    @property
    def year_str(self) -> str:
        return self.field(NewYearWizard.FIELD_YEAR_STR)

    @property
    def year_int(self) -> int:
        return self.field(NewYearWizard.FIELD_YEAR_INT)

    @property
    def zone(self):
        return self.field(NewYearWizard.FIELD_ZONE)

    @property
    def year_begin(self):
        return self.field(NewYearWizard.FIELD_YEAR_BEGIN)

    @property
    def year_end(self):
        return self.field(NewYearWizard.FIELD_YEAR_END)

    @property
    def s1_begin(self):
        return self.field(NewYearWizard.FIELD_S1_BEGIN)

    @property
    def s1_end(self):
        return self.field(NewYearWizard.FIELD_S1_END)

    @property
    def s2_begin(self):
        return self.field(NewYearWizard.FIELD_S2_BEGIN)

    @property
    def s2_end(self):
        return self.field(NewYearWizard.FIELD_S2_END)

    @property
    def toussaint_begin(self):
        return self.field(NewYearWizard.FIELD_TOUSSAINT_BEGIN)

    @property
    def toussaint_end(self):
        return self.field(NewYearWizard.FIELD_TOUSSAINT_END)

    @property
    def noel_begin(self):
        return self.field(NewYearWizard.FIELD_NOEL_BEGIN)

    @property
    def noel_end(self):
        return self.field(NewYearWizard.FIELD_NOEL_END)

    @property
    def hiver_begin(self):
        return self.field(NewYearWizard.FIELD_HIVER_BEGIN)

    @property
    def hiver_end(self):
        return self.field(NewYearWizard.FIELD_HIVER_END)

    @property
    def paques_begin(self):
        return self.field(NewYearWizard.FIELD_PAQUES_BEGIN)

    @property
    def paques_end(self):
        return self.field(NewYearWizard.FIELD_PAQUES_END)

    @property
    def project_id(self):
        return self.field(NewYearWizard.FIELD_PROJECTID)

    # @year_str.setter
    # def year_str(self, page: QWizardPage, *args, **kwargs):
    #     page.registerField(NewYearWizard.FIELD_YEAR_STR, *args, **kwargs)
    # 
    # @year_int.setter
    # def year_int(self, page: QWizardPage, *args, **kwargs):
    #     page.registerField(NewYearWizard.FIELD_YEAR_INT, *args, **kwargs)
    # 
    # @zone.setter
    # def zone(self, page: QWizardPage, *args, **kwargs):
    #     page.registerField(NewYearWizard.FIELD_ZONE, *args, **kwargs)
    # 
    # @year_begin.setter
    # def year_begin(self, page: QWizardPage, *args, **kwargs):
    #     page.registerField(NewYearWizard.FIELD_YEAR_BEGIN, *args, **kwargs)
    # 
    # @year_end.setter
    # def year_end(self, page: QWizardPage, *args, **kwargs):
    #     page.registerField(NewYearWizard.FIELD_YEAR_END, *args, **kwargs)
    # 
    # @s1_begin.setter
    # def s1_begin(self, page: QWizardPage, *args, **kwargs):
    #     page.registerField(NewYearWizard.FIELD_S1_BEGIN, *args, **kwargs)
    # 
    # @s1_end.setter
    # def s1_end(self, page: QWizardPage, *args, **kwargs):
    #     page.registerField(NewYearWizard.FIELD_S1_END, *args, **kwargs)
    # 
    # @s2_begin.setter
    # def s2_begin(self, page: QWizardPage, *args, **kwargs):
    #     page.registerField(NewYearWizard.FIELD_S2_BEGIN, *args, **kwargs)
    # 
    # @s2_end.setter
    # def s2_end(self, page: QWizardPage, *args, **kwargs):
    #     page.registerField(NewYearWizard.FIELD_S2_END, *args, **kwargs)
    # 
    # @toussaint_begin.setter
    # def toussaint_begin(self, page: QWizardPage, *args, **kwargs):
    #     page.registerField(NewYearWizard.FIELD_TOUSSAINT_BEGIN, *args, **kwargs)
    # 
    # @toussaint_end.setter
    # def toussaint_end(self, page: QWizardPage, *args, **kwargs):
    #     page.registerField(NewYearWizard.FIELD_TOUSSAINT_END, *args, **kwargs)
    # 
    # @noel_begin.setter
    # def noel_begin(self, page: QWizardPage, *args, **kwargs):
    #     page.registerField(NewYearWizard.FIELD_NOEL_BEGIN, *args, **kwargs)
    # 
    # @noel_end.setter
    # def noel_end(self, page: QWizardPage, *args, **kwargs):
    #     page.registerField(NewYearWizard.FIELD_NOEL_END, *args, **kwargs)
    # 
    # @hiver_begin.setter
    # def hiver_begin(self, page: QWizardPage, *args, **kwargs):
    #     page.registerField(NewYearWizard.FIELD_HIVER_BEGIN, *args, **kwargs)
    # 
    # @hiver_end.setter
    # def hiver_end(self, page: QWizardPage, *args, **kwargs):
    #     page.registerField(NewYearWizard.FIELD_HIVER_END, *args, **kwargs)
    # 
    # @paques_begin.setter
    # def paques_begin(self, page: QWizardPage, *args, **kwargs):
    #     page.registerField(NewYearWizard.FIELD_PAQUES_BEGIN, *args, **kwargs)
    # 
    # @paques_end.setter
    # def paques_end(self, page: QWizardPage, *args, **kwargs):
    #     page.registerField(NewYearWizard.FIELD_PAQUES_END, *args, **kwargs)
    # 
    # @project_id.setter
    # def project_id(self, page: QWizardPage, *args, **kwargs):
    #     page.registerField(NewYearWizard.FIELD_PROJECTID, *args, **kwargs)


class WelcomePage(QWizardPage):
    def __init__(self, parent):
        super(WelcomePage, self).__init__(parent)
        self.setTitle("Bienvenue dans l'assistant de migration")
        self.label1 = QLabel("ADEexplorer va vous assister dans le paramétrage d'une nouvelle année universitaire.")
        self.label1.setWordWrap(True)
        self.label2 = QLabel("Pour cela, ADEexplorer a besoin de connaître les éléments suivants :")
        self.label2.setWordWrap(True)

        comboBox_zone = QComboBox()
        comboBox_zone.addItems(NewYearWizard.ZONES)
        HWidget_zone = HBoxWidget(QLabel("Zone de votre université :"), comboBox_zone, QSpacerItem(30, 0))

        comboBox_annee = QComboBox()
        comboBox_annee.addItem("", None)
        _this_year = datetime.date.today().year - (1 if datetime.date.today().month <= 4 else 0)
        for y in range(_this_year, _this_year - 4, -1):
            comboBox_annee.addItem(annee_int_to_str(y), y)
        HWidget_annee_universitaire = HBoxWidget(QLabel("Année universitaire :"), comboBox_annee, QSpacerItem(30, 0))

        layout = QVBoxLayout()
        layout.setContentsMargins(20, 20, 9, 20)
        layout.addWidget(self.label1)
        layout.addSpacing(60)
        layout.addWidget(self.label2)
        layout.addSpacing(15)
        layout.addWidget(HWidget_zone)
        layout.addSpacing(15)
        layout.addWidget(HWidget_annee_universitaire)
        self.setLayout(layout)

        self.registerField(NewYearWizard.FIELD_YEAR_STR + "*", comboBox_annee, "currentText",
                           comboBox_annee.currentIndexChanged)
        self.registerField(NewYearWizard.FIELD_YEAR_INT + "*", comboBox_annee, "currentData",
                           comboBox_annee.currentIndexChanged)
        self.registerField(NewYearWizard.FIELD_ZONE + "*", comboBox_zone, "currentText",
                           comboBox_zone.currentTextChanged)


class Annee_Semestres(QWizardPage):
    def __init__(self, parent):
        super(Annee_Semestres, self).__init__(parent)
        self.year_int: int = 0

        self.setTitle("1. Année universitaire et semestres")
        self.setLayout(QGridLayout())
        self.layout().setContentsMargins(15, 15, 15, 15)
        self.layout().setSpacing(15)
        self.dateRange_annee = DateRangeWidget()
        self.dateRange_annee.setBoxTitle('Année universitaire')
        self.dateRange_annee.setMaximumWidth(250)
        self.dateRange_annee.setFixedHeight(120)
        self.dateRange_sem1 = DateRangeWidget()
        self.dateRange_sem1.setBoxTitle('Semestre 1')
        self.dateRange_sem1.setMaximumWidth(250)
        # self.dateRange_sem1.endDateEdit.setStyleSheet("QDateEdit {background-color: rgb(255, 255, 128);}")
        self.dateRange_sem2 = DateRangeWidget()
        self.dateRange_sem2.setBoxTitle('Semestre 2')
        self.dateRange_sem2.setMaximumWidth(250)
        # self.dateRange_sem2.beginDateEdit.setStyleSheet("QDateEdit {background-color: rgb(255, 255, 128);}")
        curly_brace = QPixmap(pM.pix("curly_brace_right.png"))
        lb = QLabel()
        lb.setPixmap(curly_brace.scaledToWidth(20))

        explain = QLabel(f"<p><img width='24' src='{pM.icon('help.svg')}'/>  "
                         "Les dates de début et de fin ci-dessus sont <b>incluses</b> dans les périodes "
                         "considérées (1er et dernier jour d'année ou de semestre).</p>"
                         "<p>Les réglages par défaut sont les suivants :"
                         "<ul>"
                         "<li>l'année universitaire est fixée du 1er sept. au 31 août ;</li>"
                         "<li>le semestre 1 commence le 1er sept. et dure 20 semaines ;</li>"
                         "<li>le semestre 2 commence le lendemain du semestre 1 et dure 24 semaines.</li>"
                         "</p>")
        explain.setWordWrap(True)

        self.layout().addWidget(self.dateRange_annee, 0, 0, 2, 1)
        self.layout().addWidget(lb, 0, 1, 2, 1)
        self.layout().addWidget(self.dateRange_sem1, 0, 2)
        self.layout().addWidget(self.dateRange_sem2, 1, 2)
        self.layout().setRowStretch(2, 1)
        self.layout().addWidget(HLine(self), 3, 0, 1, 3)
        self.layout().addWidget(explain, 4, 0, 1, 3)

        self.dateRange_annee.beginDateEdit.date()

        self.registerField(NewYearWizard.FIELD_YEAR_BEGIN, self.dateRange_annee.beginDateEdit)
        self.registerField(NewYearWizard.FIELD_YEAR_END, self.dateRange_annee.endDateEdit)
        self.registerField(NewYearWizard.FIELD_S1_BEGIN, self.dateRange_sem1.beginDateEdit)
        self.registerField(NewYearWizard.FIELD_S1_END, self.dateRange_sem1.endDateEdit)
        self.registerField(NewYearWizard.FIELD_S2_BEGIN, self.dateRange_sem2.beginDateEdit)
        self.registerField(NewYearWizard.FIELD_S2_END, self.dateRange_sem2.endDateEdit)

    def initializePage(self) -> None:
        if self.year_int == self.field(NewYearWizard.FIELD_YEAR_INT):
            return
        self.year_int = self.field(NewYearWizard.FIELD_YEAR_INT)

        debut_annee = datetime.date(self.year_int, 9, 1)
        fin_annee = datetime.date(self.year_int + 1, 8, 31)
        debut_S1 = debut_annee
        fin_S1 = debut_S1 + datetime.timedelta(weeks=20)
        debut_S2 = fin_S1 + datetime.timedelta(days=1)
        fin_S2 = debut_S2 + datetime.timedelta(weeks=24)

        self.dateRange_annee.beginDateEdit.setDate(debut_annee)
        self.dateRange_annee.endDateEdit.setDate(fin_annee)
        self.dateRange_sem1.beginDateEdit.setDate(debut_S1)
        self.dateRange_sem1.endDateEdit.setDate(fin_S1)
        self.dateRange_sem2.beginDateEdit.setDate(debut_S2)
        self.dateRange_sem2.endDateEdit.setDate(fin_S2)

    def cleanupPage(self) -> None:
        pass


class Vacances(QWizardPage):
    def __init__(self, parent):
        super(Vacances, self).__init__(parent)
        self.year_str: str = ""
        self.zone: str = ""

        self.setTitle("2. Vacances")
        self.setLayout(QGridLayout())
        self.layout().setContentsMargins(15, 15, 15, 15)
        self.layout().setSpacing(15)
        self.dateRange_toussaint = DateRangeWidget()
        self.dateRange_toussaint.setBoxTitle('Vacances de la Toussaint')
        self.dateRange_toussaint.setMaximumWidth(250)
        self.dateRange_noel = DateRangeWidget()
        self.dateRange_noel.setBoxTitle('Vacances de Noël')
        self.dateRange_noel.setMaximumWidth(250)
        self.dateRange_hiver = DateRangeWidget()
        self.dateRange_hiver.setBoxTitle("Vacances d'hiver")
        self.dateRange_hiver.setMaximumWidth(250)
        self.dateRange_paques = DateRangeWidget()
        self.dateRange_paques.setBoxTitle("Vacances de Pâques")
        self.dateRange_paques.setMaximumWidth(250)
        explain = QLabel(f"<p><img width='24' src='{pM.icon('help.svg')}'/>  "
                         "Les dates début et de fin ci-dessus sont <b>incluses</b> dans les périodes "
                         "considérées (1er et dernier jour de vacances).</p>"
                         "<p>Les réglages par défaut sont basés sur le calendrier des vacances scolaires.<br>"
                         "<b>Attention !</b> Les vacances de Toussaint et d'hiver durent généralement une seule "
                         "semaine à l'université. Vérifiez si les réglages proposés correspondent aux vacances "
                         "de votre université.</p>")
        explain.setWordWrap(True)

        self.layout().addWidget(self.dateRange_toussaint, 0, 0)
        self.layout().addWidget(self.dateRange_noel, 1, 0)
        self.layout().addWidget(self.dateRange_hiver, 0, 1)
        self.layout().addWidget(self.dateRange_paques, 1, 1)
        self.layout().setRowStretch(2, 1)
        self.layout().addWidget(HLine(self), 3, 0, 1, 2)
        self.layout().addWidget(explain, 4, 0, 1, 2)

        self.registerField(NewYearWizard.FIELD_TOUSSAINT_BEGIN, self.dateRange_toussaint.beginDateEdit)
        self.registerField(NewYearWizard.FIELD_TOUSSAINT_END, self.dateRange_toussaint.endDateEdit)
        self.registerField(NewYearWizard.FIELD_NOEL_BEGIN, self.dateRange_noel.beginDateEdit)
        self.registerField(NewYearWizard.FIELD_NOEL_END, self.dateRange_noel.endDateEdit)
        self.registerField(NewYearWizard.FIELD_HIVER_BEGIN, self.dateRange_hiver.beginDateEdit)
        self.registerField(NewYearWizard.FIELD_HIVER_END, self.dateRange_hiver.endDateEdit)
        self.registerField(NewYearWizard.FIELD_PAQUES_BEGIN, self.dateRange_paques.beginDateEdit)
        self.registerField(NewYearWizard.FIELD_PAQUES_END, self.dateRange_paques.endDateEdit)

    def initializePage(self) -> None:
        if self.year_str == self.field(NewYearWizard.FIELD_YEAR_STR) and \
                self.zone == self.field(NewYearWizard.FIELD_ZONE):
            return

        self.year_str = self.field(NewYearWizard.FIELD_YEAR_STR)
        self.zone = self.field(NewYearWizard.FIELD_ZONE)

        one_week = datetime.timedelta(weeks=1)
        one_day = datetime.timedelta(days=1)

        QApplication.setOverrideCursor(Qt.CursorShape.WaitCursor)
        sc = SchoolCalendar(os.path.join(QDir.tempPath(), "calendrier_scolaire.csv"))

        not_found = []
        try:
            toussaint = sc.find(self.year_str, self.zone, "Vacances de la Toussaint")[0]
            self.dateRange_toussaint.beginDateEdit.setDate(toussaint.start_date + one_week + one_day)
            self.dateRange_toussaint.endDateEdit.setDate(toussaint.end_date - one_day)
        except IndexError:
            not_found.append('Toussaint')
        try:
            noel = sc.find(self.year_str, self.zone, "Vacances de Noël")[0]
            self.dateRange_noel.beginDateEdit.setDate(noel.start_date + one_day)
            self.dateRange_noel.endDateEdit.setDate(noel.end_date - one_day)
        except IndexError:
            not_found.append('Noël')
        try:
            hiver = sc.find(self.year_str, self.zone, "Vacances d'Hiver")[0]
            if (hiver.start_date - noel.start_date).days < 57:
                self.dateRange_hiver.beginDateEdit.setDate(hiver.start_date + one_week + one_day)
                self.dateRange_hiver.endDateEdit.setDate(hiver.end_date - one_day)
            else:
                self.dateRange_hiver.beginDateEdit.setDate(hiver.start_date + one_day)
                self.dateRange_hiver.endDateEdit.setDate(hiver.end_date - one_week - one_day)
        except IndexError:
            not_found.append('Hiver')
        try:
            paques = sc.find(self.year_str, self.zone, "Vacances de Printemps")[0]
            self.dateRange_paques.beginDateEdit.setDate(paques.start_date + one_day)
            self.dateRange_paques.endDateEdit.setDate(paques.end_date - one_day)
        except IndexError:
            not_found.append('Pâques')
        QApplication.restoreOverrideCursor()

        if not_found:
            QMessageBox.critical(self, "Données absentes",
                                 "<p><b>Attention !</b> Les dates des vacances suivantes n'ont pas été trouvées :"
                                 "<ul>" + "".join([f"<li>{v}</li>" for v in not_found]) + "</ul>"
                                                                                          "Veuillez les ajuster à la main.</p>")

    def cleanupPage(self) -> None:
        pass


class ProjectId(QWizardPage):
    def __init__(self, parent):
        super(ProjectId, self).__init__(parent)
        self.year_str: str = ""
        self._wizard: NewYearWizard | None = None
        self.preferences: NewYearWizard | None = None
        self.resourceTreeStructure: SettingDialog | None = None

        self.setTitle("3. Numéro de projet ADE (ProjectId)")
        self.setLayout(QVBoxLayout())
        self.layout().setContentsMargins(15, 15, 15, 15)
        self.layout().setSpacing(15)

        self.qspinbox_projectId = QSpinBox()
        self.qspinbox_projectId.setMinimum(-1)
        self.qspinbox_projectId.setValue(-1)
        self.label_projectId = QLabel()
        self.qspinbox_projectId.setMinimumWidth(80)
        self.search = QPushButton("Relancer la recherche")

        self.menuSearch = QMenu(self)
        self.search.setMenu(self.menuSearch)
        self.actionSearch1 = QAction(f"projectId entre 0 et 10")
        self.actionSearch2 = QAction(f"projectId entre 0 et 30")
        self.actionSearch3 = QAction(f"projectId entre 0 et 50")
        self.actionSearch1.triggered.connect(lambda: self.guessProjectId(range(0, 10)))
        self.actionSearch2.triggered.connect(lambda: self.guessProjectId(range(0, 30)))
        self.actionSearch3.triggered.connect(lambda: self.guessProjectId(range(0, 50)))
        self.menuSearch.addActions([self.actionSearch1,
                                    self.actionSearch2,
                                    self.actionSearch3])

        HWidget_projectId = HBoxWidget(self.label_projectId, 1, self.qspinbox_projectId)
        HWidget_search = HBoxWidget(1, self.search)

        explain = QLabel(f"<p><img width='24' src='{pM.icon('help.svg')}'/>  "
                         "Le ProjectId est un identifiant numérique utilisé par ADE pour identifier un projet (c'est-à-"
                         "dire une année universitaire).</p>"
                         "<p><b>Comment cet assistant fait-il pour deviner le ProjectId ?</b><br>"
                         "Tout simplement en tentant de télécharger un agenda pour les ressources de votre "
                         "arborescence personnelle avec des valeurs de ProjectId incrémentées d'une tentative à "
                         "la suivante. Si au moins un agenda téléchargé est non-vide pour l'année considérée alors "
                         "c'est que nous tenons le bon projectId.</p>"
                         "<p><b>Que faire si l'assistant échoue ?</b><br>"
                         "Vous pouvez relancer la recherche immédiatement en choisissant une plage plus ou moins "
                         "large de recherche (0 à 10, 0 à 30 ou 0 à 50). Si plusieurs tentatives à plusieurs minutes "
                         "d'intervalle échouent, le projet ADE n'est peut-être pas encore créé ou aucun événement "
                         "n'a encore été planifié. Retentez votre chance durant l'été précédant la nouvelle année "
                         "universitaire.</p>")
        explain.setWordWrap(True)
        self.layout().addWidget(HWidget_projectId)
        self.layout().addWidget(HWidget_search)
        self.layout().addStretch()
        self.layout().addWidget(HLine(self))
        self.layout().addWidget(explain)

        self.registerField(NewYearWizard.FIELD_PROJECTID + '*', self.qspinbox_projectId)

    def initializePage(self, required: bool = False) -> None:
        if not required and self.year_str == self.field("year_str"):
            return
        self.year_str = self.field("year_str")
        self._wizard = self.wizard()
        assert isinstance(self._wizard, NewYearWizard)
        self.preferences = self._wizard.preferences
        self.resourceTreeStructure = self._wizard.resourceTreeStructure
        self.label_projectId.setText(f"ProjectId de l'année universitaire {self.field('year_str')} :")
        self.guessProjectId()

    def cleanupPage(self) -> None:
        pass

    def guessProjectId(self, tested_pids: typing.Iterable = range(0, 10)):
        QApplication.setOverrideCursor(Qt.CursorShape.WaitCursor)

        self.year_int = self.field("year_int")
        debut_annee = datetime.date(self.year_int, 9, 1)
        fin_annee = datetime.date(self.year_int + 1, 7, 30)

        tested_nodes: list[ResourceNode] = list()
        tested_uids: set[int] = set()
        for cat in self.resourceTreeStructure.categories:
            tested_uids.update([node.uid for node in PreOrderIter(cat) if node.isResource()])
            tested_nodes += [node for node in PreOrderIter(cat) if node.isResource() and node.uid not in tested_uids]

        for uid in range(200):
            if uid in tested_uids:
                continue
            tested_nodes.append(ResourceNode(f'uid={uid}', None, uid))
            tested_uids.add(uid)

        progress = QProgressDialog(f"Tentative en cours : projectId=0 sur ressource ???", "Annuler...", 0,
                                   len(tested_nodes) * len(list(tested_pids)), self)
        progress.setWindowModality(Qt.WindowModal)
        progress.setValue(0)

        temp_dir = os.path.join(QDir.tempPath(), "guessProjectId", "")
        for node in tested_nodes:
            for pid in tested_pids:
                progress.setValue(progress.value() + 1)
                progress.setLabelText(f"Tentative en cours : projectId={pid} sur ressource {node.name}")
                if progress.wasCanceled():
                    self.projectIdNotFound()
                    QApplication.restoreOverrideCursor()
                    return
                c = ADEcalendar(cache_directory=temp_dir,
                                ressource_id=node.uid,
                                project_id=pid,
                                first_date=arrow.Arrow(debut_annee.year,
                                                       debut_annee.month,
                                                       debut_annee.day),
                                last_date=arrow.Arrow(fin_annee.year,
                                                      fin_annee.month,
                                                      fin_annee.day),
                                url_root=self.preferences.settings["gestion/url_root"].value)
                try:
                    assert (len(c.events) > 0)
                    self.qspinbox_projectId.setValue(pid)
                    progress.close()
                    QApplication.restoreOverrideCursor()
                    return
                except AssertionError:
                    pass
                finally:
                    try:
                        os.remove(c.pikcleFile)
                    except FileNotFoundError:
                        pass
        QApplication.restoreOverrideCursor()
        self.qspinbox_projectId.setValue(-1)
        self.projectIdNotFound()

    def projectIdNotFound(self):
        QMessageBox.critical(self, "Échec",
                             f"<p>La recherche du projectId de l'année universitaire {self.year_str} a échoué. Les "
                             "causes possibles sont :"
                             "<ul>"
                             "<li>le projet ADE n'a pas ecore été créé au niveau de l'université → "
                             "retentez votre chance dans quelques semaines ou quelques mois ;<br></li>"
                             "<li>le projet existe mais aucune séance n'est encore programmée pour les ressources "
                             "de votre arboresence personnelle → retentez votre chance dans quelques semaines "
                             "ou quelques mois ;<br></li>"
                             "<li>le serveur ADE est resté muet lors de la recherche → retentez votre chance "
                             "dans quelques minutes.</li>"
                             "</ul>"
                             "</p>")


if __name__ == '__main__':
    app = QApplication([])
    # Traduction des boutons standards
    qtTranslator = QTranslator()
    if qtTranslator.load(pM.tr_file, pM.tr_dir):
        app.installTranslator(qtTranslator)
    preferences = SettingDialog()
    resourceTreeStructure = ResourceTreeStructure(file=preferences.settings["ressources/file"].value,
                                                  delimiter=preferences.settings["ressources/separator"].value)
    transistion = NewYearWizard(preferences, resourceTreeStructure)
    print(r := transistion.exec())

    if r == QDialog.DialogCode.Rejected:
        sys.exit(1)

    print(DateRange(transistion.year_begin.date(), transistion.year_end.date()))
    print(DateRange(transistion.s1_begin.date(), transistion.s1_end.date()))
    print(DateRange(transistion.s2_begin.date(), transistion.s2_end.date()))

    print(DateRange(transistion.toussaint_begin.date(), transistion.toussaint_end.date()))
    print(DateRange(transistion.noel_begin.date(), transistion.noel_end.date()))
    print(DateRange(transistion.hiver_begin.date(), transistion.hiver_end.date()))
    print(DateRange(transistion.paques_begin.date(), transistion.paques_end.date()))
    print(transistion.project_id)
