# ADEexplorer v1.4.5.2 : notes de version


##  Nouveautés
 * Les agendas stockés en cache le sont au format
   [pickle](https://docs.python.org/3/library/pickle.html) et non plus au 
   format natif [ical](https://fr.wikipedia.org/wiki/ICalendar). Cela permet 
   un chargement beaucoup plus rapide.


 * Les agendas externes sont à présent également stockés en cache. Cela 
   permet de bénéficier de la vitesse de chargement accrue du format pickle. 
   Ces données sont **chiffrées**. Toutefois, si ces agendas contiennent des 
   données très sensibles, l'utilisateur ne devrait pas les synchroniser ou 
   alors s'assurer qu'un tiers ne pourra accéder au cache de l'application 
   (dossier, partition ou disque chiffrés par exemple).
 