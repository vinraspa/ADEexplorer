[TOC]

![Drag Racing](doc/mainWindow.png)

# 1.  Introduction

ADEexplorer est un logiciel dédié à l'affichage et à l'analyse croisée des 
agendas des ressources ADE.
ADEexplorer est écrit en Python 3 et s'appuie sur le PyQt5 pour
l'interface graphique.

# 2. Lancer ADEexplorer

Pour utiliser ADEexplorer, il existe 2 possibilités : 
 * à partir des fichiers source ;
 * exécuter une version compilée propre à un système d'exploitation.

## 2.1 Exécutable

L'application ADEexplorer est disponible sous forme compilée pour 3 
plateformes (https://drive.uca.fr/d/e68386c818f64720b918/?p=%2FReleases):
 * Linux,
 * Windows,
 * MacOS (uniquement v1.3.0 : besoin de contributeurs).

L'avantage est que l'exécutable est autonome, elle ne nécessite aucun 
logiciel ni aucune bibliothèque pour fonctionner (sauf l'exécutable Linux 
qui n'embarque pas la bibliothèque `glibc` et nécessite la version `2.31` ou 
une version supérieure sur le système qui l'exécute).
L'inconvénient est que le code source ne peut être contrôlé par 
l'utilisateur et la taille de l'exécutable est importante (quelques dizaines 
de mégaoctets).

Placer dans un même dossier l'éxécutable et, éventuellement, le fichier de 
paramétrage *ADEexplorer.ini* et un fichier de ressources (`*.csv`). Si vous ne 
possédez pas ces deux fichiers, ADEexplorer les créera pour vous.

## 2.2 Sources

Télécharger les sources ou cloner le dossier à partir de Gitlab grâce au 
logiciel Git 
```
git clone https://gitlab.com/vinraspa/ADEexplorer.git
```


ADEexplorer nécessite Python >= 3.6 pour fonctionner. On pourra 
éventuellement travailler avec un environnement virtuel (voir
https://docs.python.org/fr/3/library/venv.html).

Les modules python nécessaires à l'exécution d'ADEexplorer sont contenus 
dans le fichier `Requirements.txt`. Pour installer ces dépendances en 
respectant les conditions sur les versions, lancer dans un terminal
```
pip install -r Requirements.txt
```
Enfin, lancer 
```
python3 ADEexplorer.py
```

# 3. Paramétrer ADEexplorer

## 3.1 Ajustement des préférences

Lorsque vous lancez ADEexplorer pour la première fois (c'est-à-dire lorsque 
aucun fichier de paramétrage `ADEexplorer.ini` n'existe dans le dossier 
d'exécution), ADEexplorer ouvre automatiquement la fenêtre de réglage des 
préférences. Plusieurs onglets sont disponibles.

### 3.1.1 Gestion des agendas
Cet onglet est **primordial** ! Si les deux premiers champs sont mal 
renseignés, aucun agenda ne pourra être téléchargé.
 * L'**URL de téléchargement des agendas** dépend de 
   l'université. 
   Pour l'obtenir,
   1. se rendre sur l'interface classique de consultation de 
   l'emploi du temps de son université,
   2. sélectionner une ressource au hasard 
   (soi-même par exemple),
   3. cliquer sur Agenda Export (en bas en gauche 
   dans les options en général),
   4. cliquer sur Générer l'URL,
   5. copier l'adresse et ne garder que le protocole et le nom de domaine. Par 
      exemple, pour l'Université Clermont Auvergne, l'adresse retenue est : 
   `http://edt.uca.fr`. 
 
   Une fois renseignée, cette adresse n'aura plus jamais à être modifiée.


 * Le **Project Id** est un nombre. Il change chaque année et devra donc 
   être modifié à chaque nouvelle année universitaire. Pour le connaître, 
   reprendre l'adresse d'export de l'étape précédente. Quelque part *après* le 
   `?`, on trouve `&projectId=...`. Relever le nombre après le signe `=` et le 
   renseigner dans le champ dédié.


 * Le groupe nommé **Téléchargement des agendas** permet d'ajuster la 
   politique de gestion des fichiers `.ics` (contenant les 
   événements d'un agenda) téléchargés par ADEexplorer. Le logiciel conserve 
   une version de chaque agenda téléchargé afin de ne pas le re-télécharger 
   inutilement à chaque appel. Seulement, les emplois du temps ADE sont 
   mouvants et il est utile de rafraîchir les fichiers de temps en temps.

### 3.1.2 Ressources
ADEexplorer permet de travailler avec des ressources hiérarchisées en 
arborescence.
Mais contrairement à l'arborescence imposée par ADE, commune à toute 
l'université et, de ce fait, très touffue et longue à parcourir, ADEexplorer 
permet à chaque utilisateur de créer sa propre hiérarchie de ressources 
pertinentes. 
Cette hiérarchie est stockée dans un fichier `.csv` dont le nom est réglé 
dans cet onglet, ainsi que le caractère séparateur de colonne choisi 
(généralement la virgule `,` csv signifiant "*comma separated values*"). Il 
existe deux possibilités :
 * soit vous possédez déjà un fichier `.csv` (élaboré par vous ou un gentil 
   collègue) : renseigner alors le chemin vers ce fichier ;
 * soit vous n'en avez pas et souhaitez qu'ADEexplorer crée un fichier 
   vierge à votre place : laisser alors vide le chemin vers le fichier. Vous 
   ne pourrez commencer à utiliser ADEexplorer qu'une fois ajoutées des 
   ressources à votre arborescence.


### 3.1.3 Année et semestres
Dans cet onglet, ajuster les dates de l'année universitaire et des deux 
semestres qui la composent.

### 3.1.4 Vacances
Dans cet onglet, ajuster les dates des quatre périodes de vacances 
(Toussaint, Noël, Hiver, Pâques).

### 3.1.5 Horaires hebdomadaires
Dans cet onglet, cocher les jours et horaires potentiellement travaillés par 
les étudiants (utile pour la recherche de créneaux libres dans l'emploi du 
temps).

### 3.1.6 Mode graphique
Dans cet onglet, spécifique à l'affichage des emplois du temps en mode 
graphique, choisir les jours de la semaine ainsi que les horaires 
de la journée affichés. Vous pouvez également choisir de ne pas afficher les 
semaines ne contenant aucun événement (_semaines vides_).


## 3.2 Construire une arborescence de ressources

### 3.2.1 Notions de ressources, conteneurs et catégories

ADEexplorer travaille avec une arborescence de **ressources**. Une ressource peut désigner,
* un enseignant,
* un groupe d'étudiants,
* une salle,
* un matériel.

Ces ressources sont assemblées entre elles par des **conteneurs** qui 
rassemblent plusieurs ressources et d'autres conteneurs.

Pour une meilleure lisibilité et efficacité, ADEexplorer propose de séparer les 
arborescences de ressources en 
différentes **catégories**. Par exemple, une catégorie pour les collègues 
enseignants, une autre pour les groupes et une autre pour les salles (ce 
n'est qu'un exemple, on peut créer autant de catégories que souhaité et y 
placer les ressources que l'on veut).

### 3.2.2 Identifier le code d'une ressource
Dans ADE, chaque ressource est identifiée par une valeur numérique (que nous 
nomemrons ici **Id**). Pour construire une hiérarchie, la compétence de base 
consiste donc à savoir déterminer cet Id pour une ressource donnée.

Pour l'obtenir,
1. se rendre sur l'interface classique de consultation de 
   l'emploi du temps de son université,
2. sélectionner la ressource pertinente,
3. cliquer sur Agenda Export (en bas en gauche 
   dans les options en général),
4. cliquer sur Générer l'URL.
5. identifier l'Id de la ressource dans l'adresse. Par exemple, 
   si l'URL générée est : 
   `http://.../anonymous_cal.jsp?
   resources=525&projectId=6&nbWeeks=8&calType=ical`, l'Id recherchée est `525`.

### 3.2.3 Modifier une hiérachie existante

Dans ADEexplorer, la hiérarchie des ressources est affichée dans le volet à 
gauche. Chaque catégorie est matérialisée par une arborescence 
indépendante. Par un clic droit, il est possible d'ajouter, supprimer ou 
modifier des ressources ou des conteneurs.

Remarque : il n'est pas possible de créer, supprimer ou modifier des 
catégories par ce biais.


### 3.2.4 Bâtir un fichier csv de ressources

Lorsqu'on part de zéro, ou que l'on souhaite faire beaucoup de modifications en 
gagant du temps, il vaut mieux éditer soi-même le fichier `.csv` à l'aide d'un 
tableur. C'est ce que propose le menu `Édition > Éditer la liste des 
ressources`. Chaque ligne du fichier représente une catégorie, un conteneur 
ou une ressource. Chaque ligne vide sera ignorée.

 * **Catégories :** elles doivent apparaître dans la colonne de gauche 
   (colonne `A` sur un tableur) entre 
   des doubles chevrons `<<...>>`. Par exemple `<<Groupes>>` ou `<<Collègues>>`.

 * **Ressources :** le code de la ressource doit apparaître dans la colonne 
   de gauche (colonne `A` sur un tableur). Le nom de la ressource doit apparaître 
   dans la colonne à droite de la colonne de son conteneur. Un nom court 
   peut être précisé entre parenthèses : il sera utilisé en mode graphique 
   pour gagner de la place.

 * **Ressources :** même chose que pour les ressources, mais sans Id et sans 
   nom court.


L'exemple ci-dessous crée deux catégories (_Groupes_ et _Collègues_). La 
catégorie _Groupe_ contient deux conteneurs à la racine (_1re année_ et _2e 
année_). 
Le conteneur _1re année_ contient deux conteneurs (_TD A_ et _TD B_) qui 
contiennent 
chacun deux ressources (_TP A1_, ..., _TP B2_). La catégorie _Collègues_ 
contient 
directement des ressources à sa racine (_Dupond Jean_ et _Durand Yves_). Le 
nom court de _Dupond Jean_ sera _DJ_, celui de _Durand Yves_ sera _DY_.

| A             | B                | C    | D     |
|---------------|------------------|------|-------|
| <<Groupes>>   |                  |      |       |
| &nbsp;        | 1re année        |      |       |
| &nbsp;        |                  | TD A |       |
| 123           |                  |      | TP A1 |
| 234           |                  |      | TP A2 |
| &nbsp;        |                  | TD B |       |
| 345           |                  |      | TP B1 |
| 456           |                  |      | TP B2 |
| &nbsp;        | 2e année         |      |       |
| 567           |                  | G1   |       |
| 678           |                  | G2   |       |
| &nbsp;        |                  |      |       |
| &nbsp;        |                  |      |       |
| &nbsp;        |                  |      |       |
| <<Collègues>> |                  |      |       |
| 789           | Dupond Jean (DJ) |      |       |
| 890           | Durand Yves (DY) |      |       |

## 3.3 Associer des agendas externes à une ressource

### 3.3.1 De quoi s'agit-il ?
À partir de la version 1.4.0, il est possible d'associer à une 
   ressource (vous en général) un ou des agendas externes (par exemple issus 
   de Google). Cela permet plusieurs choses.
   1. _Une recherche améliorée des créneaux libres_ : elle prend en compte 
      vos contraintes personnelles.
   2. _Une surveillance automatique des collisions_ : si votre emploi du temps 
      ADE présente un événement qui empiète sur vos activités personnelles 
      (collision), une alerte sera lancée lorsque vous affichez votre emploi 
      du temps.

### 3.3.2 Comment faire ?
* __Étape 1__ : récupérer l'url de votre agenda externe. Dans le cas où le 
  fournisseur est Google :
  1. Suivre ce lien https://calendar.google.com/ et vous connecter à votre 
    compte ;
  2. dans le volet de gauche, survoler avec le pointeur l'agenda souhaité, 
     cliquer sur les 3 petits points " ⋮ " puis _Paramètres et Partage_ ;
  3. presqu'au fond de la page, copier l'_Adresse secrète au format ical_.
* __Étape 2__ : dans ADEexplorer
  1. faire un clic-droit sur la ressource à laquelle vous souhaitez 
     rattacher l'agenda externe, puis _Éditer la ressource_ ;
  2. coller l'url dans une des trois cases prévues à cet effet.

À l'issue de l'étape 2, la ressource apparait en __gras__.

Répéter les étapes 1 et 2 pour ajouter d'autres agendas (jusqu'à 3).

### 3.3.2 Vie privée
   Certains garde-fous sont prévus pour protéger votre vie privée :
   1. L'url de votre agenda personnel (qui doit absolument rester secrète !) 
      est stockée dans le porte-clés géré par le système d'exploitation (url 
      chiffrée). Pour ce faire, ADEexplorer s'appuie sur le module
      [`keyring`](https://pypi.org/project/keyring/).
   2. Contrairement aux agendas ADE, le contenu des agendas externes n'est 
      pas stocké en cache localement (ils sont re-téléchargés à chaque 
      session).
   3. Une case à cocher permet d'activer ou désactiver l'affichage des 
      agendas externes (désactivation utile en cas de consultation publique).


# 4. Utiliser ADEexplorer

ADEexplorer permet l'affichage et le traitement d'emplois du temps ADE sous 
différentes formes. En outre, une série de filtres permet de mener des 
recherches particulières sur les événements.

## 4.1 Traitement des emplois du temps

La barre d'outil fait apparaître trois méthodes de traitements différentes 
des emplois du temps.
 * **Simple :** tous les emplois du temps sélectionnés sont affichés tels 
   quels.


 * **Créneaux libres :** fait apparaître les plages horaires durant 
   lesquelles _toutes_ les ressources sont libres _simultanément_. La 
   recherche se limite aux plages horaires indiquées dans l'onglet "Horaires 
   hebdomadaires" de la fenêtre de réglage des préférences.


 * **Séances communes :** fait apparaître les événements auxquels _toutes_ les 
   ressources sélectionnées participent. Tous les autres événements sont 
   ignorés.

## 4.2 Modes d'affichage

La barre d'outil fait apparaître trois modes d'affichage des emplois du temps.
 * **Graphique :** reprend le principe d'affichage d'ADE en ligne. Chaque 
   ressource correspond à une colonne au sein d'un jour. Une seule semaine est 
   affichée à la fois. Chaque semaine est placée dans un onglet. L'étiquette 
   de l'onglet fait apparaître le numéro de semaine et le nombre 
   d'événements qu'elle contient. Les événements sont représentés par des 
   boutons sur lesquels l'utilisateur peut cliquer pour afficher plus de 
   détails et accéder à des actions plus poussées.


 * **Liste :** reprend le principe du tableur. Chaque événement est 
   représenté par une ligne. Les détails de l'événement sont découpés en 
   différentes colonnes. Il existe différentes manières d'interagir avec 
   l'afficgage en liste.
   * _Clic gauche sur un entête de colonne_ permet de trier les événements 
     par ordre (dé)croissant selon cette colonne.
   * _Clic droit sur un entête de colonne_ permet de colorier les événements 
     avec des couleurs différentes selon leurs valeurs pour cette colonne.
   * _Clic droit sur le tableau_ permet de copier tout ou partie du tableau, 
     exporter le tableau vers une feuille de calcul (formats supportés : `ods`,
     `xls`, `xlsx`) ou encore de colorier les événements faisant apparaître 
     la même valeur.


 * **Statistiques :**


## 4.3 Utilisation des filtres

L'usage des filtres est un outil puissant permettant de ne faire apparaître 
que les événements remplissant certains critères précis, tous les autres 
étant éliminés.

Tous les filtres peuvent être réinitialisés simultanément grâce au bouton 
**Effacer les filtres**.

### 4.3.1 Période calendaire
Lorsqu'ADEexplorer télécharge un agenda depuis le serveur d'ADE, il le fait 
pour l'année universitaire complète. Toutefois, il est rare qu'on 
s'intéresse à la totalite de cette période. On peut choisir de 
limiter l'affichage à celui d'un semestre (1 ou 2) complet, à la fin de la 
période en cours (aujourd'hui jusqu'à la fin du semestre en cours) ou encore 
à une période particulière définie par l'utilisateur.

**Valeur par défaut :** semestre en cours ou, à défaut, l'année complète.

### 4.2.2 Durée

Ce filtre permet de ne conserver que les événements dont la durée répond au 
critère défini par l'utilisateur.

**Valeur par défaut :** `>= 0:00` (donc tous les événements...)

### 4.2.3 Contenu, participants et lieu

Ces trois filtres fonctionnent de façon identique mais s'adressent à trois 
attributs différents du même nom. Ils peuvent être combinés. Chaque filtre est 
composé de 
deux lignes :
 * une ligne pour les termes à _inclure_ : **filtre inclusif**
 * une ligne pour les termes à _exclure_ : **filtre exclusif**

Chaque ligne comporte deux champs.
 * _Une zone de texte_ : dans laquelle l'utilisateur entre un ou plusieurs 
   termes séparés par des espaces ; chaque terme sera recherché en début de 
   mot. La casse n'est pas prise en compte.
 * _Un quantificateur_ : `all` ou `any`

Si le quantificateur est `all`, **tous les termes** entrés par l'utilisateur 
doivent être présents simultanément dans un événement pour activer le filtre 
(inclusif ou exclusif).

Si le quantificateur est `any`, **au moins un des termes** entrés par 
l'utilisateur doit être présent dans un événement pour activer le filtre 
(inclusif ou exclusif).

Pour rester affiché, un événement doit avoir activé le critère inclusif et ne 
pas avoir activé le critère exclusif.

### 4.2.4 Jour de la semaine

Ce filtre consiste en 7 cases à cocher, une par jour. En décochant une case, 
les événements dudit jour de la semaine sont exlus.

Attention ! Ce filtre est indépendant des jours de la semaine affichés 
en mode graphique (ce réglage est retrouvé dans l'onglet "Mode graphique" de 
la fenêtre de réglage des préférences).

**Valeur par défaut :** lundi au samedi cochés, dimanche décoché.
