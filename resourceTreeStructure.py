import colorsys
import csv
import re
import typing
import uuid
from dataclasses import dataclass, field
from datetime import timedelta
from random import random
from urllib.parse import urlparse

import keyring
import keyring.errors
import randomcolor
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from anytree import Node, PreOrderIter, Resolver, ResolverError
from arrow import utcnow

import pathsManagement as pM
from ADEcore import ADEcalendar, DataStatus
from ui.extraAgendaEditorGUI import Ui_ExtraAgendaEditor

"""
L'objet de ce module est de procurer une interface permettant de modifier
le fichier de ressources csv sans passer par un tableur.
"""


class ResourceNode(Node):
    def __init__(self, name: str, parent=None, uid=None):
        name = name.replace('/', '_')
        super(ResourceNode, self).__init__(name=name, parent=parent)
        self.uid = uid
        self.calendar: ADEcalendar | None = None
        self._extra_url: str | None = None
        self.extra_calendars: list[ADEcalendar] = list()
        self.merged_calendar: ADEcalendar | None = None
        self.isSelected: bool = False
        self._icon = None
        # l'attribut 'unique_node_identifier' permet de retrouver sans ambiguïté un ResourceNode dans un
        # QAbstractItemModel via la méthode 'match()'
        # Exemple :
        # index = model.match(model.index(0, 0, QModelIndex()), Qt.AccessibleDescriptionRole,
        #                     moved_node.unique_node_identifier, 1, Qt.MatchRecursive)[0]
        # ou dans par un Resolver :
        # Exemple :
        # Resolver('unique_node_identfier').get(self.root_item, data)
        self.unique_node_identifier = uuid.uuid4().hex
        # La recherche de l'extra_url dans le keyring est un peu longue et peut produire un léger freeze de la GUI.
        # On le fait une fois à l'initialisation pour gagner du temps par la suite. (extra_url est une @property)
        _ = self.extra_url

    def __repr__(self):
        return f'[{self.uid}] {self.name}' if self.uid else self.name

    def isResource(self) -> bool:
        return self.uid is not None

    def isContainer(self) -> bool:
        return self.uid is None

    def clear(self):
        self.calendar = None
        self.extra_calendars.clear()
        self.merged_calendar = None

    @property
    def extra_url(self):
        if self._extra_url is not None:
            return self._extra_url
        else:
            self._extra_url = keyring.get_password('ADEexplorer', 'extra_url-' + str(self.uid)) or ''
            return self._extra_url

    @extra_url.setter
    def extra_url(self, url: str):
        if url:
            url.strip()
            keyring.set_password('ADEexplorer', 'extra_url-' + str(self.uid), url)
            self._extra_url = url
        else:
            try:
                keyring.delete_password('ADEexplorer', 'extra_url-' + str(self.uid))
                self._extra_url = url
            except keyring.errors.PasswordDeleteError:
                pass

    @staticmethod
    def formatAge(age: timedelta) -> str:
        if age is None:
            return "inconnu"
        d = age.days
        h, rest = divmod(age.seconds, 3600)
        m, rest = divmod(rest, 60)
        s = int(rest)

        if d:
            return f"{d} jours" if d > 1 else "1 jour"
        elif h:
            return f"{h}h"
        elif m:
            return f"{m}min"
        else:
            return f"{s}s"
            # return f"<1 min" if s > 15 else "à l'instant"

    def data(self, column: int, role):
        match role:
            case Qt.ItemDataRole.DisplayRole:
                if column == 0:
                    return self.name
                if column == 1:
                    return self.uid
            case Qt.ItemDataRole.FontRole if self.extra_url:
                font = QFont()
                font.setBold(True)
                return font
            case Qt.ItemDataRole.DecorationRole if column == 0:
                return self.icon
            case Qt.ItemDataRole.ForegroundRole if column == 0:
                return self.textColor()
            case Qt.ItemDataRole.ToolTipRole:
                if self.isResource():
                    tooltip = "Identifiant ADE : " + str(self.uid)
                    if self.extra_url:
                        tooltip += '\nAgendas externes : ' + str(len(ExtraAgendasData(self.extra_url).extra_agendas))
                    return tooltip
                if self.isContainer():
                    return f'{self.childCount()} éléments'
            # l'attribut 'unique_node_identifier' permet de retrouver sans ambiguïté un ResourceNode dans un
            # QAbstractItemModel via la méthode 'match()'
            # Exemple :
            # index = model.match(model.index(0, 0, QModelIndex()), Qt.AccessibleDescriptionRole,
            #                     moved_node.unique_node_identifier, 1, Qt.MatchRecursive)[0]
            case Qt.ItemDataRole.AccessibleDescriptionRole:
                return self.unique_node_identifier

    def textColor(self):
        if self.isResource() and self.calendar:
            if self.calendar.dataStatus == DataStatus.downloaded:
                return None
            if self.calendar.dataStatus == DataStatus.fromDisk:
                return None
            if self.calendar.dataStatus == DataStatus.failedDownload | DataStatus.fromDisk:
                return QColor(Qt.GlobalColor.darkYellow)
            if self.calendar.dataStatus == DataStatus.noData or DataStatus.corruptedCache:
                return QColor(Qt.GlobalColor.red)
        return None

    @property
    def icon(self):
        if self.isContainer():
            return QIcon(QApplication.style().standardIcon(QStyle.StandardPixmap.SP_DirIcon))
        if self.isResource() and self.calendar:
            if self.calendar.dataStatus == DataStatus.downloaded:
                return QIcon(pM.icon("download.svg"))
            if self.calendar.dataStatus == DataStatus.fromDisk:
                return QIcon(pM.icon("disk_good.svg"))
            if self.calendar.dataStatus == DataStatus.failedDownload | DataStatus.fromDisk:
                return QIcon(pM.icon("disk_bad.svg"))
            if self.calendar.dataStatus == DataStatus.noData or DataStatus.corruptedCache:
                return QIcon(pM.icon("warning.svg"))
        return QIcon(pM.icon("empty.svg"))

    @icon.setter
    def icon(self, new_icon):
        self._icon = new_icon

    def setData(self, data, column: int):
        if column == 0:
            self.name = data
        if column == 1:
            self.uid = data

    def childCount(self):
        return len(self.children)

    def child(self, row):
        return self.children[row]

    def row(self):
        return 0

    def updateAge(self):
        if not self.isResource():
            return
        try:
            if self.calendar.fileTimeStamp is not None:
                self.age = utcnow() - self.calendar.fileTimeStamp
        except AttributeError:
            return

    def strMimeData(self) -> str:
        return '/'.join([''] + [n.unique_node_identifier for n in self.ancestors] + [self.unique_node_identifier])

    @staticmethod
    def columnCount():
        return 1


class ResourceTreeStructure:
    _CAT_REGEX = re.compile(r'^<<(\w+)>>$')

    class MultipleDefineCategory(Exception):
        """
        Catégorie déclarée plusieurs fois.
        """
        pass

    class StructureError(Exception):
        """
        Erreur de structure dans l'arborescence.
        """
        pass

    def __init__(self, file: str, delimiter=',', encoding: str = 'utf8'):
        self.file = file
        self.encoding = encoding
        self.delimiter = delimiter if len(delimiter) == 1 else ','
        self.root_item = ResourceNode('root')
        self.categories: list[ResourceNode] = list()
        self.read_file()

    def read_file(self, file: str = None):
        file = file or self.file
        self.categories.clear()
        _curr_container: ResourceNode
        _comments_on = False
        with open(file, encoding=self.encoding) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=self.delimiter)
            for idx, row in enumerate(csv_reader):
                if not row:
                    continue
                if row[0].startswith('//') or row[0].startswith('#'):
                    continue
                if row[0].startswith('*/'):
                    _comments_on = False
                if row[0].startswith('/*'):
                    _comments_on = True
                if _comments_on:
                    continue
                if self._is_ressource(row):
                    id_number, r, i = self._get_ressource(row)
                    try:
                        parent = _curr_container.path[i]
                    except IndexError:
                        raise ResourceTreeStructure.StructureError(f"Line {idx}: {row}")
                    node = ResourceNode(r, parent, id_number)
                    continue
                if self._is_container(row):
                    c, i = self._get_container(row)
                    try:
                        parent = _curr_container.path[i]
                    except IndexError:
                        raise ResourceTreeStructure.StructureError(f"Line {idx}: {row}")
                    _curr_container = ResourceNode(c, parent)
                    continue
                if self._is_category(row):
                    c = self._get_category(row)
                    _curr_container = ResourceNode(c, parent=self.root_item)
                    self.categories.append(_curr_container)
                    continue

    def write_file(self, file: str = None):
        file = file or self.file
        with open(file, mode='w+', encoding=self.encoding) as csv_file:
            csv_writer = csv.writer(csv_file, delimiter=self.delimiter)
            _first_cat = True
            for node in self.root_item.descendants:
                if node.depth == 1:
                    if not _first_cat:
                        csv_writer.writerows([[]] * 2)
                    row = [f'<<{node.name}>>']
                    _first_cat = False
                else:
                    row = [''] * (node.depth - 1)
                    row.append(node.name)
                    row[0] = node.uid
                csv_writer.writerow(row)
        print("Changes written to file:", file)

    @staticmethod
    def create_empty_file(file: str, categories: typing.Iterable[str], delimiter: str = ',', encoding: str = 'utf8'):
        print(delimiter)
        with open(file, mode='w+', encoding=encoding) as csv_file:
            csv_writer = csv.writer(csv_file, delimiter=delimiter)
            for cat in categories:
                rows = [[f'<<{cat}>>'], [], []]
                csv_writer.writerows(rows)

    @staticmethod
    def _is_ressource(row: list):
        """ S'assure que la ligne considérée est une ressource. C'est-à-dire :
         0. la ligne n'est pas vide
         1. le 1er élément doit être un nombre (id de la ressource),
         2. la ligne doit compter 2 éléments non vides (id et nom).
         """
        if not row:
            return False
        if not row[0].isnumeric():
            return False
        if sum(map(lambda x: x != '', row)) != 2:
            return False
        return True

    @staticmethod
    def _get_ressource(row: list) -> tuple[int, str, int]:
        """ Retourne un tuple ret :
         ret[0]: identifiant de la ressource
         ret[1]: nom de la ressource
         ret[2]: rang de la ressource
        """
        _row = row.copy()
        _id = _row.pop(0)
        for _i, _s in enumerate(_row):
            if _s: return _id, _s, _i + 1

    @staticmethod
    def _is_container(row: list):
        """ S'assure que la ligne considérée est un conteneur. C'est-à-dire :
         0. la ligne n'est pas vide
         1. le 1er élément doit être vide (pas d'id),
         2. la ligne doit compter 1 seul élément non vide (nom).
         """
        if not row:
            return False
        row = row.copy()
        if sum(map(lambda x: x != '', row)) != 1:
            return False
        if not row[0] == '':
            return False
        return True

    @staticmethod
    def _get_container(row: list):
        """ Retourne un tuple ret :
         ret[0]: nom du conteneur,
         ret[1]: rang du conteneur.
        """
        _row = row.copy()
        _ = _row.pop(0)
        for _i, _s in enumerate(_row):
            if _s: return _s, _i + 1

    @staticmethod
    def _is_category(row: list):
        """ S'assure que la ligne considérée est une catégorie. C'est-à-dire :
         0. la ligne n'est pas vide
         1. le 1er élément doit être vide (pas d'id),
         2. la ligne doit compter 1 seul élément non vide (nom).
         """
        if not row:
            return False
        if sum(1 for e in row if e not in [None, ""]) != 1:
            return False
        if not ResourceTreeStructure._CAT_REGEX.match(row[0]):
            return False
        return True

    @staticmethod
    def _get_category(row: list):
        """ Retourne la catégorie d'une ligne"""
        return ResourceTreeStructure._CAT_REGEX.search(row[0]).group(1)


class ResourceTreeModel(QAbstractItemModel):
    def __init__(self, rootNode: ResourceNode):
        super(ResourceTreeModel, self).__init__()
        self.root_item = rootNode
        self.headers = [rootNode.name.capitalize(), 'Id']

    @property
    def has_extra_url(self):
        return bool(self.nodes_with_extra_url())

    def nodes_with_extra_url(self) -> list[ResourceNode]:
        return [node for node in PreOrderIter(self.root_item) if node.extra_url]

    def index(self, row: int, column: int, parent: QModelIndex = ...) -> QModelIndex:
        if not self.hasIndex(row, column, parent):
            return QModelIndex()

        if not parent.isValid():
            parentItem = self.root_item
        else:
            parentItem = parent.internalPointer()

        childItem = parentItem.child(row)
        if childItem:
            return self.createIndex(row, column, childItem)
        else:
            return QModelIndex()

    def index_from_node_name(self, node: ResourceNode, model=None) -> QModelIndex:
        _model = model or self
        indexes = _model.match(_model.index(0, 0, QModelIndex()), Qt.ItemDataRole.DisplayRole,
                               node.name, 1, Qt.MatchFlag.MatchRecursive)
        return indexes[0] if indexes else None

    def index_from_node_unique_indentifier(self, node: ResourceNode, model=None) -> QModelIndex:
        _model = model or self
        indexes = _model.match(_model.index(0, 0, QModelIndex()), Qt.ItemDataRole.AccessibleDescriptionRole,
                               node.unique_node_identifier, 1, Qt.MatchFlag.MatchRecursive)
        return indexes[0] if indexes else None

    def all_children_indexes(self, parent: QModelIndex = ...) -> set[QModelIndex]:
        ls: set[QModelIndex] = set()
        for row in range(self.rowCount(parent)):
            child = self.index(row, parent.column(), parent)
            ls.add(child)
            ls |= self.all_children_indexes(child)
        return ls

    def parent(self, child: QModelIndex) -> QModelIndex:
        if not child.isValid():
            return QModelIndex()

        childItem = child.internalPointer()
        parentItem = childItem.parent

        if parentItem == self.root_item:
            return QModelIndex()

        try:
            return self.createIndex(parentItem.row(), 0, parentItem)
        except AttributeError:
            return QModelIndex()

    def rowCount(self, parent: QModelIndex = ..., *args, **kwargs) -> int:
        if parent.column() > 0:
            return 0

        if not parent.isValid():
            parentItem = self.root_item
        else:
            parentItem = parent.internalPointer()

        return parentItem.childCount()

    def columnCount(self, parent: QModelIndex = ..., *args, **kwargs) -> int:
        if parent.isValid():
            return parent.internalPointer().columnCount()
        else:
            return self.root_item.columnCount()

    def headerData(self, section: int, orientation: Qt.Orientation, role: int = ...) -> typing.Any:
        match role:
            case Qt.ItemDataRole.DisplayRole:
                try:
                    return self.headers[section]
                except IndexError:
                    return str(section + 1)
            case Qt.ItemDataRole.DecorationRole:
                if section == 0:
                    return QIcon.fromTheme("address-book-app-symbolic")

    def data(self, index: QModelIndex, role: int = ...) -> typing.Any:
        if not index.isValid():
            return None

        item = index.internalPointer()
        assert isinstance(item, ResourceNode)
        return item.data(index.column(), role)

    def setData(self, index: QModelIndex, data: typing.Any, role=None) -> bool:
        if not index.isValid():
            return False
        if not data:
            return False
        node = index.internalPointer()
        assert isinstance(node, ResourceNode)
        if role == Qt.ItemDataRole.EditRole:
            node.setData(data, index.column())
            return True

    def supportedDropActions(self):
        return Qt.DropAction.MoveAction  # | Qt.CopyAction

    def flags(self, index):
        if not index.isValid():
            return Qt.ItemFlag.NoItemFlags

        if index.column() == 0:
            return Qt.ItemFlag.ItemIsEnabled | Qt.ItemFlag.ItemIsSelectable | Qt.ItemFlag.ItemIsDragEnabled | \
                   Qt.ItemFlag.ItemIsDropEnabled

        return Qt.ItemFlag.ItemIsEnabled

    def insertNode(self, new_node: ResourceNode, destination_parent: ResourceNode, destination_index: int):
        new_node.parent = None
        children = list(destination_parent.children)
        children.insert(destination_index, new_node)
        destination_parent.children = children
        self.layoutChanged.emit([QPersistentModelIndex()], QAbstractItemModel.NoLayoutChangeHint)

    def moveNode(self, node: ResourceNode, destination_parent: ResourceNode, destination_index: int = None):
        destination_parent = destination_parent or self.root_item
        children = list(destination_parent.children)
        destination_index = len(destination_parent.children) if destination_index is None else destination_index
        if node.parent is not destination_parent:
            children.insert(destination_index, node)
        else:
            current_index = children.index(node)
            if current_index < destination_index:
                children.pop(current_index)
                children.insert(destination_index - 1, node)
            else:
                children.pop(current_index)
                children.insert(destination_index, node)
        destination_parent.children = children
        self.layoutChanged.emit([QPersistentModelIndex()], QAbstractItemModel.NoLayoutChangeHint)

    def removeNode(self, node: ResourceNode):
        node.parent = None
        self.layoutChanged.emit([QPersistentModelIndex()], QAbstractItemModel.NoLayoutChangeHint)

    def mimeTypes(self):
        return ['text/resourceNode']

    def mimeData(self, indexes: list[QModelIndex]) -> QMimeData:
        mimedata = QMimeData()
        data = []
        for index in indexes:
            node: ResourceNode = index.internalPointer()
            data.append(node.strMimeData())
        mimedata.setData("text/resourceNode", '\n'.join(data).encode('utf8'))
        return mimedata

    def nodeListFromMimeData(self, mimeData: QMimeData) -> list[ResourceNode]:
        decoded_data = bytes(mimeData.data('text/resourceNode')).decode('utf8')
        ret: list[ResourceNode] = list()
        for data in decoded_data.split('\n'):
            ret.append(Resolver('unique_node_identifier').get(self.root_item, data))
        return ret


class ResourceTreeViewStyle(QCommonStyle):
    """
    Cette class définit le style à appliquer aux instances de la classe ResourceTreeView (définie plus bas).
    """

    # Ne pas inclure 'styleHint' (https://github.com/qutebrowser/qutebrowser/issues/5124#issuecomment-1096356338)
    TranslateMethods = ['drawComplexControl', 'drawControl', 'drawItemText', 'generatedIconPixmap',
                        'hitTestComplexControl', 'pixelMetric', 'polish', 'sizeFromContents', 'standardPixmap',
                        'subControlRect', 'subElementRect', 'unpolish', "standardIcon"]

    def __init__(self, style: QStyle = None):
        super(ResourceTreeViewStyle, self).__init__()
        self._style = style or QApplication.style()

        for method_name in self.TranslateMethods:
            try:
                setattr(self, method_name, getattr(self._style, method_name))
            except AttributeError:
                pass

    def drawPrimitive(self, element, option: QStyleOption, painter, widget):
        if element == QStyle.PrimitiveElement.PE_IndicatorItemViewItemDrop:
            pen = QPen(Qt.GlobalColor.darkGray)
            if option.rect.height() == 0:
                pen.setWidth(5)
            else:
                pen.setWidth(1)
                brush = QBrush(QColor(250, 250, 0, 150))
                painter.setBrush(brush)
            painter.setPen(pen)
            if not option.rect.isNull():
                painter.drawRect(option.rect)
            return
        self._style.drawPrimitive(element, option, painter, widget)


class ResourceTreeView(QTreeView):
    resourceTreeStructureChanged = pyqtSignal()
    clearOtherTreeViewSelection = pyqtSignal()
    itemSelectionChanged = pyqtSignal()
    forceDownloadRequired = pyqtSignal(ResourceNode)
    showCollisions = pyqtSignal(ResourceNode)
    nodeEdited = pyqtSignal()
    nodeExpansionChanged = pyqtSignal()

    def __init__(self, *args, parent=None, **kwargs):
        super(ResourceTreeView, self).__init__(*args, **kwargs)
        # self.setDragDropMode(QAbstractItemView.InternalMove)
        self.parent = parent
        self.setAcceptDrops(True)
        self.setDragEnabled(True)
        self.dragStartPosition = QPoint()
        self.setDropIndicatorShown(True)
        self.setAutoScroll(True)
        self.setAutoScrollMargin(10)
        self.setStyle(ResourceTreeViewStyle())
        self.setStyleSheet("""
                QTreeView {
                    show-decoration-selected: 0;
                }
                
                QTreeView::item {
                    border: 1px solid #ffffff;
                }
                
                QTreeView::item:hover {
                    background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #eff3fe, stop: 1 #dbeaf9);
                    border: 1px solid #bfcde4;
                }
                
                QTreeView::item:selected:active{
                    background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #6ea1f1, stop: 1 #567dbc);
                }
                
                QTreeView::item:selected:!active {
                    background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #6b9be8, stop: 1 #577fbf);
                }
                """)

        sizePolicy = QSizePolicy(QSizePolicy.Policy.Preferred, QSizePolicy.Policy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.sizePolicy().hasHeightForWidth())
        self.setSizePolicy(sizePolicy)
        font = QFont()
        font.setPointSize(9)
        font.setBold(False)
        font.setWeight(50)
        self.setFont(font)
        self.setHorizontalScrollBarPolicy(Qt.ScrollBarPolicy.ScrollBarAlwaysOff)
        self.setProperty("showDropIndicator", True)
        self.setSelectionMode(QAbstractItemView.SelectionMode.ExtendedSelection)
        self.setIndentation(12)
        self.setUniformRowHeights(True)
        self.setWordWrap(True)
        self.setHeaderHidden(False)
        self.setObjectName("treeView")
        self.header().setMinimumSectionSize(100)
        self.header().setSortIndicatorShown(False)
        self.header().setStretchLastSection(True)

        self.setSelectionMode(QAbstractItemView.SelectionMode.ExtendedSelection)
        self.setSelectionBehavior(QAbstractItemView.SelectionBehavior.SelectRows)
        self.customContextMenuRequested.connect(lambda point: self.showContextMenu(point))

        self.setDropIndicatorShown(True)

        self._expandedNodes = set()
        self.expanded.connect(self.expandedStateChanged)
        self.collapsed.connect(self.expandedStateChanged)

    def expandNodes(self, node_paths: list[str]):
        for node_path in node_paths:
            try:
                node = Resolver('name').get(self.model().root_item, node_path)
            except ResolverError:
                continue
            index = self.model().index_from_node_unique_indentifier(node)
            try:
                self.expand(index)
            except TypeError:
                continue

    def expandedStateChanged(self, index: QModelIndex):
        node: ResourceNode = index.internalPointer()
        if self.isExpanded(index):
            self._expandedNodes.add(node)
        else:
            self._expandedNodes.discard(node)
        self.nodeExpansionChanged.emit()

    @property
    def expandedNodes(self) -> list[str]:
        return ['/' + '/'.join([n.name for n in node.path]) for node in self._expandedNodes]

    def setModel(self, model: QAbstractItemModel):
        super(ResourceTreeView, self).setModel(model)

    def showContextMenu(self, pos: QPoint) -> None:
        isRoot: bool = False
        node = self.indexAt(pos).internalPointer()
        if not isinstance(node, ResourceNode):
            node = self.model().root_item
            isRoot = True

        menu = QMenu(self.parent)

        titre = QLabel(node.name)
        titre.setAlignment(Qt.AlignmentFlag.AlignCenter)
        action_titre = QWidgetAction(menu)
        action_titre.setDefaultWidget(titre)
        font = QApplication.font()
        font.setStyle(QFont.Style.StyleOblique)
        font.setWeight(60)
        titre.setFont(font)
        menu.addAction(action_titre)
        menu.addSeparator()

        # Action Forcer le téléchargement des ressources
        if node.isResource():
            action_force_download = QAction(f"Forcer le téléchargement", self)
            action_force_download.setIcon(QIcon(pM.icon("download.svg")))
            menu.addAction(action_force_download)
            action_force_download.triggered.connect(lambda: self.forceDownloadRequired.emit(node))

        if not isRoot:
            # Action Éditer
            if node.isContainer():
                action_edit = QAction(f"Éditer le conteneur", self)
            else:
                action_edit = QAction(f"Éditer la ressource", self)
            action_edit.setIcon(QIcon(pM.icon("edit.svg")))
            menu.addAction(action_edit)
            action_edit.triggered.connect(lambda: self.editNode(node))
            menu.addSeparator()

            # Action Afficher les collisions
            if node.isResource():
                action_show_collisions = QAction(f"Rechercher les collisions", self)
                action_show_collisions.setIcon(QIcon(pM.icon("collisions.svg")))
                menu.addAction(action_show_collisions)
                action_show_collisions.triggered.connect(lambda: self.showCollisions.emit(node))
                if not node.extra_url:
                    action_show_collisions.setDisabled(True)
            menu.addSeparator()

            # Menu contextuel principal. Le parent déclaré est le parent du treeview et pas le treeview
            # lui-même car ce dernier a son propre style qui cause un bug sur les menus (les items survolés
            # ne sont pas mis en surbrillance...)
            subMenuBefore = QMenu(f"Ajouter avant...", self.parent)
            subMenuBefore.setIcon(QIcon(pM.icon("add.svg")))
            menu.addMenu(subMenuBefore)
            # Action Ajouter Conteneur Avant
            action_add_container_before = QAction(f"un conteneur", self)
            action_add_container_before.setIcon(
                QIcon(QApplication.style().standardIcon(QStyle.StandardPixmap.SP_DirIcon)))
            subMenuBefore.addAction(action_add_container_before)
            action_add_container_before.triggered.connect(lambda: self.addNodeBefore(node, container=True))
            # Action Ajouter Ressource Avant
            action_add_resource_before = QAction(f"une ressource", self)
            action_add_resource_before.setIcon(QIcon(pM.icon("resource.svg")))
            subMenuBefore.addAction(action_add_resource_before)
            action_add_resource_before.triggered.connect(lambda: self.addNodeBefore(node))

            subMenuAfter = QMenu(f"Ajouter après...", self.parent)
            subMenuAfter.setIcon(QIcon(pM.icon("add.svg")))
            menu.addMenu(subMenuAfter)
            # Action Ajouter Conteneur Après
            action_add_container_after = QAction(f"un conteneur", self)
            action_add_container_after.setIcon(
                QIcon(QApplication.style().standardIcon(QStyle.StandardPixmap.SP_DirIcon)))
            subMenuAfter.addAction(action_add_container_after)
            action_add_container_after.triggered.connect(lambda: self.addNodeAfter(node, container=True))
            # Action Ajouter Ressource Après
            action_add_resource_after = QAction(f"une ressource", self)
            action_add_resource_after.setIcon(QIcon(pM.icon("resource.svg")))
            subMenuAfter.addAction(action_add_resource_after)
            action_add_resource_after.triggered.connect(lambda: self.addNodeAfter(node))

        if node.isContainer():
            subMenuIn = QMenu(f"Ajouter dans...", self.parent)
            subMenuIn.setIcon(QIcon(pM.icon("add.svg")))
            menu.addMenu(subMenuIn)
            # Action Ajouter Conteneur dans
            action_add_container_in = QAction(f"un conteneur", self)
            action_add_container_in.setIcon(QIcon(QApplication.style().standardIcon(QStyle.StandardPixmap.SP_DirIcon)))
            subMenuIn.addAction(action_add_container_in)
            action_add_container_in.triggered.connect(lambda: self.addNodeIn(node, container=True))
            # Action Ajouter Ressource dans
            action_add_resource_in = QAction(f"une ressource", self)
            action_add_resource_in.setIcon(QIcon(pM.icon("resource.svg")))
            subMenuIn.addAction(action_add_resource_in)
            action_add_resource_in.triggered.connect(lambda: self.addNodeIn(node))

        if not isRoot:
            menu.addSeparator()
            # Action Supprimer
            action_remove = QAction(f"Supprimer", self)
            action_remove.setIcon(QIcon.fromTheme("edit-delete"))
            menu.addAction(action_remove)
            action_remove.triggered.connect(lambda: self.deleteNode(node))

        menu.popup(self.viewport().mapToGlobal(pos))

    def mousePressEvent(self, event: QMouseEvent):
        if event.button() == Qt.MouseButton.LeftButton:
            self.dragStartPosition = event.pos()
            super(ResourceTreeView, self).mousePressEvent(event)
            if QGuiApplication.keyboardModifiers() != Qt.KeyboardModifier.ControlModifier:
                self.clearOtherTreeViewSelection.emit()

    def mouseMoveEvent(self, event: QMouseEvent) -> None:
        # If the mouse does not move far enough when the left mouse button is held down,
        # do not start a drag and drop operation.
        # The code below overrides the global app setting (which can be set by 'QApplication.setStartDragDistance(...)')
        if (event.pos() - self.dragStartPosition).manhattanLength() < 20:
            return
        super(ResourceTreeView, self).mouseMoveEvent(event)

    def dragMoveEvent(self, event: QDragMoveEvent):
        super(ResourceTreeView, self).dragMoveEvent(event)
        if self.dropIndicatorPosition() in (QAbstractItemView.DropIndicatorPosition.AboveItem,
                                            QAbstractItemView.DropIndicatorPosition.BelowItem):
            event.acceptProposedAction()
            return
        hoveredNode_idx: QModelIndex = self.indexAt(event.pos())
        hoveredNode: ResourceNode = hoveredNode_idx.internalPointer() or self.model().root_item
        if hoveredNode is self.model().root_item:
            event.acceptProposedAction()
            return
        if not hoveredNode.isContainer():
            event.ignore()

    # def paintEvent(self, event: QPaintEvent):
    #     self.setDropIndicatorShown(self.dropIndicatorPosition() in (QAbstractItemView.AboveItem,
    #                                                                 QAbstractItemView.BelowItem))
    #     super(ResourceTreeView, self).paintEvent(event)
    #     self.setDropIndicatorShown(True)

    def dragEnterEvent(self, event: QDragMoveEvent):
        if event.source() is self:
            event.accept()
        else:
            event.ignore()

    def dragLeaveEvent(self, event: QDragLeaveEvent):
        event.accept()

    def dropEvent(self, event: QDropEvent):
        model: ResourceTreeModel = self.model()
        moved_nodes: list[ResourceNode] = model.nodeListFromMimeData(event.mimeData())

        hoveredNode_idx: QModelIndex = self.indexAt(event.pos())
        hoveredNode: ResourceNode = hoveredNode_idx.internalPointer()
        parentNode_idx = hoveredNode_idx.parent()
        parentNode = parentNode_idx.internalPointer()

        selection: list[QModelIndex] = list()

        # Si la sélection comporte à la fois un node père et un de ses enfants, on enlève cet enfant
        descendants = set()
        for node in moved_nodes:
            descendants |= set(node.descendants).intersection(set(moved_nodes))
        for node in descendants:
            moved_nodes.remove(node)

        for moved_node in moved_nodes:
            match self.dropIndicatorPosition():
                case QAbstractItemView.DropIndicatorPosition.AboveItem:
                    model.moveNode(moved_node, parentNode, hoveredNode_idx.row())
                case QAbstractItemView.DropIndicatorPosition.BelowItem:
                    model.moveNode(moved_node, parentNode, hoveredNode_idx.row() + 1)
                case QAbstractItemView.DropIndicatorPosition.OnItem:
                    if hoveredNode.isContainer() and hoveredNode is not moved_node.parent:
                        model.moveNode(moved_node, hoveredNode)
                    else:
                        event.ignore()
                        return
                case QAbstractItemView.DropIndicatorPosition.OnViewport:
                    model.moveNode(moved_node, model.root_item)
                case _:
                    return

        self.resourceTreeStructureChanged.emit()
        self.selectionModel().clearSelection()

        for moved_node in moved_nodes:
            selection.append(model.index_from_node_name(moved_node))

        event.accept()

    @pyqtSlot()
    def editNode(self, node: ResourceNode):
        if node.uid is None:
            dialog = ContainerNodeEditor(node, parent=self, flags=Qt.WindowType.Dialog)
        else:
            dialog = ResourceNodeEditor(node, parent=self, flags=Qt.WindowType.Dialog)

        if dialog.exec() != QDialog.DialogCode.Accepted:
            return
        self.resourceTreeStructureChanged.emit()
        self.nodeEdited.emit()

    @pyqtSlot()
    def addNodeBefore(self, node: ResourceNode, container: bool = False):
        """
        Adds :
          + a new resource (ResourceNode with uid) if not 'conatiner' OR
          + a new container (ResourceNode without uid) if container
        BEFORE 'node' within 'node.parent.children'
        """
        new_node = ResourceNode(name="Nouveau conteneur" if container else "Nouvelle ressource",
                                uid=None if container else 0)
        row = node.parent.children.index(node)
        self.model().insertNode(new_node, node.parent, row)
        self.editNode(new_node)
        self.resourceTreeStructureChanged.emit()

    @pyqtSlot()
    def addNodeAfter(self, node: ResourceNode, container: bool = False):
        """
        Adds :
          + a new resource (ResourceNode with uid) if not 'conatiner' OR
          + a new container (ResourceNode without uid) if container
        AFTER 'node' within 'node.parent.children'
        """
        new_node = ResourceNode(name="Nouveau conteneur" if container else "Nouvelle ressource",
                                uid=None if container else 0)
        row = node.parent.children.index(node)
        self.model().insertNode(new_node, node.parent, row + 1)
        self.editNode(new_node)
        self.resourceTreeStructureChanged.emit()

    @pyqtSlot()
    def addNodeIn(self, parent: ResourceNode, container: bool = False):
        """
        Appends :
          + a new resource (ResourceNode with uid) if not 'conatiner' OR
          + a new container (ResourceNode without uid) if container
        to 'parent.children'
        """
        new_node = ResourceNode(name="Nouveau conteneur" if container else "Nouvelle ressource",
                                uid=None if container else 0)
        row = len(parent.children)
        self.model().insertNode(new_node, parent, row)
        self.editNode(new_node)
        self.resourceTreeStructureChanged.emit()

    @pyqtSlot()
    def deleteNode(self, node: ResourceNode):
        titre = "Suppression d'une ressource"
        text = f"Voulez-vous vraiment supprimer <b>{node.name}</b> "
        if node.isContainer():
            titre = "Suppression d'un conteneur"
            text += f"et tout son contenu ? ({len([n for n in node.descendants if n.isResource()])} ressource(s))"
        reply = QMessageBox.question(self, titre, text, QMessageBox.StandardButton.Yes | QMessageBox.StandardButton.No)
        if reply != QMessageBox.StandardButton.Yes:
            return
        self.model().removeNode(node)
        self.resourceTreeStructureChanged.emit()


    def selectionChanged(self, selected: QItemSelection, deselected: QItemSelection) -> None:
        if not self.sender():
            return
        model = self.model()
        all_selected: set[QModelIndex] = set(selected.indexes())
        all_deselected: set[QModelIndex] = set(deselected.indexes())
        model: ResourceTreeModel = self.model()
        # On parcourt les index désélectionnés à la recherche de descendants à désélectionner aussi.
        for idx in deselected.indexes():
            all_deselected |= model.all_children_indexes(idx)
        for idx in all_deselected:
            deselected.merge(QItemSelection(idx, idx), QItemSelectionModel.SelectionFlag.Select)
            assert isinstance(idx.internalPointer(), ResourceNode)
            idx.internalPointer().isSelected = False
        # On parcourt les index sélectionnés à la recherche de descendants à sélectionner aussi.
        for idx in selected.indexes():
            all_selected |= model.all_children_indexes(idx)
        for idx in all_selected:
            selected.merge(QItemSelection(idx, idx), QItemSelectionModel.SelectionFlag.Select)
            assert isinstance(idx.internalPointer(), ResourceNode)
            idx.internalPointer().isSelected = True

        # On bloque les signaux le temps de modifier la sélection pour
        # empêcher le selectionModel() de rappeler cette même fonction plusieurs fois
        # avec pour conséquence autant de fois le signal 'self.itemSelectionChanged'
        self.selectionModel().blockSignals(True)
        self.selectionModel().select(deselected, QItemSelectionModel.SelectionFlag.Deselect)
        self.selectionModel().select(selected, QItemSelectionModel.SelectionFlag.Select)
        self.selectionModel().blockSignals(False)

        super(ResourceTreeView, self).selectionChanged(selected, deselected)
        self.itemSelectionChanged.emit()

    def selectedResources(self):
        return [idx.internalPointer() for idx in self.selectedIndexes() if idx.internalPointer().isResource()]


class ResourceNodeEditor(QDialog):
    _NAME_AND_SHORT_REGEX = re.compile(r'^\s*(.*?)\s*(\((\w+)\)\s*)?$')

    def __init__(self, node: ResourceNode, *args, **kwargs):
        super(ResourceNodeEditor, self).__init__(*args, **kwargs)
        self.node = node
        try:
            long = ResourceNodeEditor._NAME_AND_SHORT_REGEX.search(self.node.name).group(1).strip()
            short = ResourceNodeEditor._NAME_AND_SHORT_REGEX.search(self.node.name).group(3)
        except AttributeError:
            long = self.node.name
            short = ""

        self.setWindowTitle("Édition de la ressource")
        form = QFormLayout(self)
        self.lineEdit_name = QLineEdit(long)
        self.lineEdit_name.selectAll()
        self.label_name = QLabel("Nom de la ressource")
        form.addRow(self.label_name, self.lineEdit_name)
        self.lineEdit_short = QLineEdit(short)
        self.label_short = QLabel("Nom court (optionnel)")
        form.addRow(self.label_short, self.lineEdit_short)
        self.lineEdit_uid = QLineEdit(str(self.node.uid))
        self.lineEdit_uid.setValidator(QIntValidator(0, 2147483647))
        self.label_uid = QLabel("Identifiant de la ressource")
        form.addRow(self.label_uid, self.lineEdit_uid)

        line = QWidget()
        line.setFixedHeight(1)
        line.setSizePolicy(QSizePolicy.Policy.Expanding, QSizePolicy.Policy.Fixed)
        line.setStyleSheet("background-color: #c0c0c0;")
        form.addRow(line)
        self.label_extra_provider = QLabel("Agendas externes (optionnels)")
        self.pushButton_new_extra_provider = QPushButton("Ajouter")
        self.pushButton_new_extra_provider.setSizePolicy(QSizePolicy.Policy.Fixed, QSizePolicy.Policy.Preferred)
        self.pushButton_new_extra_provider.clicked.connect(lambda: self.appendExtraAgenda(edit_now=True))
        form.addRow(self.label_extra_provider, self.pushButton_new_extra_provider)
        self.extraAgendasButtonHolder = QWidget()
        self.extraAgendasButtonHolder.setLayout(QVBoxLayout())
        form.addRow(self.extraAgendasButtonHolder)
        self.extra_buttons: list[QPushButton] = list()
        self.extraAgendasData = ExtraAgendasData(node.extra_url)
        for extra_agenda_data in self.extraAgendasData.extra_agendas:
            self.appendExtraAgenda(extra_agenda_data)

        buttonBox = QDialogButtonBox(QDialogButtonBox.StandardButton.Ok |
                                     QDialogButtonBox.StandardButton.Cancel, Qt.Orientation.Horizontal, self)
        form.addRow(buttonBox)
        buttonBox.accepted.connect(self.accept)
        buttonBox.rejected.connect(self.reject)

    def popupExtraAgendaEditor(self, button=None):
        button = button or self.sender()
        assert isinstance(button, QPushButton)
        idx = self.extra_buttons.index(button)
        extra_agenda_data = self.extraAgendasData.extra_agendas[idx]
        extraEditor = ExtraAgendaEditor(extra_agenda_data)
        extraEditor.exec()
        status = extraEditor.status
        if status == ExtraAgendaEditor.DialogCode.Rejected:
            return
        if status == ExtraAgendaEditor.DialogCode.Accepted:
            extra_agenda_data.name = extraEditor.name
            extra_agenda_data.url = extraEditor.url
            extra_agenda_data.user = extraEditor.user
            extra_agenda_data.password = extraEditor.password
            extra_agenda_data.color = extraEditor.color
            self.refresh_button(button, extra_agenda_data)
            return
        if status == ExtraAgendaEditor.Removed:
            if not QMessageBox.question(self, "Suppression d'un agenda externe",
                                        f"Voulez-vous vraiment supprimer l'agenda externe "
                                        f"intitulé {extra_agenda_data.name} ?") in (QMessageBox.StandardButton.Yes,
                                                                                    QMessageBox.StandardButton.Ok):
                return
            self.extraAgendasData.extra_agendas.pop(idx)
            self.extra_buttons.pop(idx)
            button.deleteLater()

    def appendExtraAgenda(self, extra_agenda_data=None, edit_now: bool = False):
        if not extra_agenda_data:
            extra_agenda_data = ExtraAgendaData()
            self.extraAgendasData.extra_agendas.append(extra_agenda_data)
        button = QPushButton()
        button.setStyleSheet("""QPushButton {
                                    padding: 5px;
                                    border-color: rgb(136, 138, 133);
                                    border-style: outset;
                                    border-width: 1px;
                                    color: black;
                                    text-align:left;}
                                QPushButton:hover {
                                    background-color: white;}
                                QPushButton:pressed {
                                    background-color: orange;}
                            """)
        button.setFlat(True)
        self.refresh_button(button, extra_agenda_data)
        self.extra_buttons.append(button)
        self.extraAgendasButtonHolder.layout().addWidget(button)
        button.clicked.connect(self.popupExtraAgendaEditor)
        if edit_now:
            self.popupExtraAgendaEditor(button)

    @staticmethod
    def refresh_button(button, extra_agenda_data):
        # Mise à jour du texte
        button.setText(f"{extra_agenda_data.name} ({urlparse(extra_agenda_data.url).netloc})")

        # Mise à jour de l'icône (couleur)
        size = QSize(25, 25)
        pixmap = QPixmap(size)
        pixmap.fill(QColor(255, 0, 0, 0))
        painter = QPainter(pixmap)
        painter.setRenderHint(QPainter.RenderHint.Antialiasing, True)
        painter.setBrush(QColor(extra_agenda_data.color))
        painter.setPen(Qt.PenStyle.NoPen)
        painter.drawEllipse(0, 0, size.width(), size.height())
        painter.end()
        button.setIcon(QIcon(pixmap))
        button.setIconSize(size)

    def accept(self) -> None:
        if self.lineEdit_name.text().strip() != "":
            self.node.name = f"{self.lineEdit_name.text().strip().replace('/', '_')}"
            if self.lineEdit_short.text().strip():
                self.node.name += f" ({self.lineEdit_short.text().strip()})"
        self.node.uid = self.lineEdit_uid.text().lstrip('0') or self.node.uid
        self.node.extra_url = self.extraAgendasData.serialize
        super(ResourceNodeEditor, self).accept()


class ContainerNodeEditor(QDialog):

    def __init__(self, node: ResourceNode, *args, **kwargs):
        super(ContainerNodeEditor, self).__init__(*args, **kwargs)
        self.node = node
        self.setWindowTitle("Édition du conteneur")
        form = QFormLayout(self)
        self.lineEdit_name = QLineEdit(node.name)
        self.lineEdit_name.selectAll()
        self.label_name = QLabel("Nom du conteneur")
        form.addRow(self.label_name, self.lineEdit_name)
        buttonBox = QDialogButtonBox(QDialogButtonBox.StandardButton.Ok | QDialogButtonBox.StandardButton.Cancel,
                                     Qt.Orientation.Horizontal, self)
        form.addRow(buttonBox)
        buttonBox.accepted.connect(self.accept)
        buttonBox.rejected.connect(self.reject)

    def accept(self) -> None:
        if self.lineEdit_name.text().strip() != "":
            self.node.name = f"{self.lineEdit_name.text().strip().replace('/', '_')}"
        super(ContainerNodeEditor, self).accept()


@dataclass
class ExtraAgendaData:
    url: str = ''
    name: str = ''
    user: str = ''
    password: str = ''
    color: str = field(default_factory=lambda: randomcolor.RandomColor(random()).generate(luminosity="bright")[0])

    @property
    def credentials(self) -> typing.Iterable[str] | None:
        return self.user, self.password if self.user else None


class ExtraAgendasData:
    """
    Classe gérant l'ensemble des agendas externes d'une même ressource.
    """

    def __init__(self, serialized_data: str):
        self.extra_agendas: list[ExtraAgendaData] = list()
        if not serialized_data:
            return
        for line in serialized_data.split('|'):
            frags = line.split('\t')
            agenda_info = ExtraAgendaData(*frags)
            self.extra_agendas.append(agenda_info)

    @property
    def serialize(self):
        return "|".join(["\t".join([info.url, info.name, info.user, info.password, info.color])
                         for info in self.extra_agendas])


class ExtraAgendaEditor(QDialog, Ui_ExtraAgendaEditor):
    signalDelete = pyqtSignal()
    color_Y_threshold = 0.4
    Removed = 2

    def __init__(self, agenda_data: ExtraAgendaData):
        super(ExtraAgendaEditor, self).__init__()
        self.setupUi(self)

        self.setWindowModality(Qt.WindowModality.ApplicationModal)
        self.status = QDialog.DialogCode.Rejected
        self.agenda_data = agenda_data
        self.url = self.agenda_data.url
        self.name = self.agenda_data.name
        self.user = self.agenda_data.user
        self.password = self.agenda_data.password
        self._color = self.agenda_data.color
        self.updateIconColor()
        self.pushButton_color.clicked.connect(self.colorSelection)
        delete = QPushButton("Supprimer l'agenda")
        delete.setIcon(QApplication.style().standardIcon(QStyle.StandardPixmap.SP_TrashIcon))
        self.buttonBox.addButton(delete, QDialogButtonBox.ButtonRole.DestructiveRole)
        self.buttonBox.clicked.connect(self._clicked)

    def _clicked(self, button: QAbstractButton):
        match self.buttonBox.buttonRole(button):
            case QDialogButtonBox.ButtonRole.AcceptRole:
                self.status = QDialog.DialogCode.Accepted
            case QDialogButtonBox.ButtonRole.RejectRole:
                self.status = QDialog.DialogCode.Rejected
            case QDialogButtonBox.ButtonRole.DestructiveRole:
                self.status = ExtraAgendaEditor.Removed
        self.close()

    def colorSelection(self):
        color = QColorDialog.getColor(QColor(self._color), self, "Choisir une couleur pour l'agenda")
        assert isinstance(color, QColor)
        if not color.isValid():
            return
        Y, I, Q = colorsys.rgb_to_yiq(color.redF(), color.greenF(), color.blueF())
        if Y < self.color_Y_threshold:
            b = QMessageBox.warning(self, "Couleur trop foncée",
                                    "<p>La couleur que vous avez choisie est trop foncée et le texte en noir risque "
                                    "d'être illisible.</p>"
                                    "<p>Cliquer sur <tt>OK</tt> pour éclaircir cette couleur en conservant la teinte "
                                    "ou sur <tt>Ignorer</tt> pour conserver votre choix.</p>",
                                    QMessageBox.StandardButton.Ignore | QMessageBox.StandardButton.Ok,
                                    QMessageBox.StandardButton.Ok)
            if b == QMessageBox.StandardButton.Ok:
                color = QColor.fromRgbF(*colorsys.yiq_to_rgb(self.color_Y_threshold, I, Q))

        self._color = '#' + format(color.rgb(), 'x')
        self.updateIconColor()
        QMessageBox.information(self, "Couleur modifiée",
                                f"<p>Le changement de couleur pour l'agenda {self.agenda_data.name} a bien été pris "
                                f"en compte. Il sera effectif après redémarrage de l'application.</p>",
                                QMessageBox.StandardButton.Ok, QMessageBox.StandardButton.Ok)

    def updateIconColor(self):
        size = QSize(25, 25)
        pixmap = QPixmap(size)
        pixmap.fill(QColor(255, 0, 0, 0))
        painter = QPainter(pixmap)
        painter.setRenderHint(QPainter.RenderHint.Antialiasing, True)
        painter.setBrush(QColor(self._color))
        painter.setPen(Qt.PenStyle.NoPen)
        painter.drawEllipse(0, 0, size.width(), size.height())
        painter.end()
        self.pushButton_color.setIcon(QIcon(pixmap))
        self.pushButton_color.setIconSize(size)

    @property
    def name(self):
        return self.lineEdit_name.text()

    @name.setter
    def name(self, val):
        self.lineEdit_name.setText(val)

    @property
    def url(self):
        return self.lineEdit_url.text()

    @url.setter
    def url(self, val):
        self.lineEdit_url.setText(val)

    @property
    def user(self):
        return self.lineEdit_user.text()

    @user.setter
    def user(self, val):
        self.lineEdit_user.setText(val)

    @property
    def password(self):
        return self.lineEdit_password.text()

    @password.setter
    def password(self, val):
        self.lineEdit_password.setText(val)

    @property
    def color(self):
        return self._color

    @color.setter
    def color(self, val):
        self._color = val
        self.updateIconColor()


if __name__ == '__main__':
    app = QApplication([])
    window = QMainWindow()
    tree_view = ResourceTreeView()
    resource_tree = ResourceTreeStructure('./CODES_RESSOURCES (copie).csv', delimiter=',')

    sub_tree = resource_tree.categories[1]
    model_collegues = ResourceTreeModel(sub_tree)
    tree_view.setModel(model_collegues)
    tree_view.setContextMenuPolicy(Qt.ContextMenuPolicy.CustomContextMenu)

    tree_view.expandAll()
    tree_view.resizeColumnToContents(0)
    window.setCentralWidget(tree_view)
    # window.show()
    window.resize(300, 550)

    extra = ResourceNodeEditor()
    extra.show()
    app.exec()
