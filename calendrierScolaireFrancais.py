import csv
import datetime
from collections import defaultdict
from dataclasses import dataclass, astuple

import requests


@dataclass(frozen=False)
class CalendarLine:
    description: str
    population: str
    _start_date: str
    _end_date: str
    location: str
    zones: str
    annee_scolaire: str

    @property
    def start_date(self):
        return datetime.datetime.fromisoformat(self._start_date).date()

    @property
    def end_date(self):
        return datetime.datetime.fromisoformat(self._end_date).date()

    def __iter__(self):
        return iter(astuple(self))

    def __hash__(self):
        return hash(astuple(self))

    def __lt__(self, other):
        return self.start_date < other.start_date


class SchoolCalendar:
    STABLE_URL = "https://www.data.gouv.fr/fr/datasets/r/9957d723-346e-4317-8cb3-293c94e19b2d"

    def __init__(self, cache):
        self.data: list[CalendarLine] = []
        self.default_dict: defaultdict[str, set[CalendarLine]] = defaultdict(set)
        try:
            self.download()
            self.write_cache(file=cache)
        except requests.exceptions.RequestException:
            self.read_cache(file=cache)
        self.fill_dict()

    def fill_dict(self):
        for calendar_line in self.data:
            for field in calendar_line:
                self.default_dict[field].add(calendar_line)

    def download(self):
        download = requests.get(SchoolCalendar.STABLE_URL)
        decoded_content = download.content.decode('utf-8')
        cr = csv.reader(decoded_content.splitlines(), delimiter=';')
        self.data = [CalendarLine(*line) for line in cr]
        self.data.pop(0)

    def write_cache(self, file):
        with open(file, 'w') as csvfile:
            cw = csv.writer(csvfile, delimiter=';')
            cw.writerows(self.data)

    def read_cache(self, file):
        with open(file, 'r') as csvfile:
            cr = csv.reader(csvfile, delimiter=';')
            self.data = [CalendarLine(*line) for line in cr]
            self.data.pop(0)

    def find(self, *args) -> list[CalendarLine]:
        return list(set.intersection(*[self.default_dict[arg] for arg in args]))


if __name__ == '__main__':
    sc = SchoolCalendar('/home/vincent/tmp/toto.csv')
    debut_annee = sc.find('2021-2022', 'Clermont-Ferrand', "Vacances d'Été")[0].end_date
    try:
        fin_annee = sc.find('2022-2023', 'Clermont-Ferrand', "Vacances d'Été")[0].start_date
    except IndexError:
        fin_annee = sc.find('2022-2023', 'Clermont-Ferrand', "Début des Vacances d'Été")[0].start_date
    print(debut_annee - datetime.timedelta(days=1), fin_annee + datetime.timedelta(days=1))
