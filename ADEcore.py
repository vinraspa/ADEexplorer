#!/usr/bin/python3
# -*- coding: utf-8 -*-
import bisect
import datetime
import hashlib
import io
import json
import pickle
import re
import typing
import uuid
import urllib.parse
from collections import OrderedDict
from collections import defaultdict
from enum import Flag, auto
from functools import cached_property
from os import path, makedirs, linesep, stat

import arrow
import pyAesCrypt
import requests
import tabulate
from arrow import Arrow
from ics import Calendar, Event
from unidecode import unidecode

import pathsManagement as pM


class DownloadError(Exception):
    """Raised when the input value is too small"""
    pass


class DataStatus(Flag):
    none = 0
    fromDisk = auto()
    downloaded = auto()
    failedDownload = auto()
    corruptedCache = auto()
    noData = auto()


class ADEevent(Event):
    re_module_code = re.compile(r'\b(M\s*[\d.?]+\w{1,2}/?)\b', re.IGNORECASE)
    re_SAE_code = re.compile(r'\b(SA[ÉE]\s*\d.*?\d{1,2})\b', re.IGNORECASE)
    re_Ressource_code = re.compile(r'\b(R\s*\d.*?\d{1,2})\b', re.IGNORECASE)
    re_Type_code = re.compile(r'\b(C/TD|CM/TD|C|CM|Cours|'
                              r'TD/TP|TP/TD|TD|TP|'
                              r'Exam|Examen|Partiel|Devoir|DS)(?=\W|$|\d)', re.IGNORECASE)
    re_Family_code = re.compile(r'ADE\d+d\d+d\d+d(\d+)d\d+d\d+')
    re_Clean_html_tags = re.compile('<.*?>|&([a-z0-9]+|#[0-9]{1,6}|#x[0-9a-f]{1,6});')
    comments_file = ""
    comments_dict = dict()

    # TODO: laisser l'utilisateur choisir sa ou ses Regex pour chaque catégorie (module/SAÉ/ressource et type)
    def __init__(self, event: Event = None, color: str = None, calendar_name: str = "", *args, **kwargs):
        if not event:
            super(ADEevent, self).__init__(*args, **kwargs)
        else:
            super(ADEevent, self).__init__()
            for attr in ("name", "begin", "end", "duration", "uid", "description", "created", "last_modified", "url",
                         "location", "transparent", "alarms", "attendees", "categories", "status", "organizer"):
                setattr(self, attr, getattr(event, attr))

        self.calendar_name = calendar_name or "ADE"
        self._seance = ""
        self._color = color
        self.colliding_events: set[ADEevent] = set()

    @cached_property
    def nom(self) -> str:
        nom = self.name
        nom = nom.replace('_', ' ')
        for mod_regex in (ADEevent.re_module_code, ADEevent.re_SAE_code, ADEevent.re_Ressource_code):
            nom = re.sub(mod_regex, '', nom).strip(' :,-')
        nom = re.sub(ADEevent.re_Type_code, '', nom).strip(' :,-')
        nom = nom.replace('()', '')
        nom = re.sub(r'\s+', ' ', nom)
        return nom

    @cached_property
    def module(self) -> str:
        nom = self.name
        nom = nom.replace('_', ' ')
        module = ""
        for mod_regex in (ADEevent.re_module_code, ADEevent.re_SAE_code, ADEevent.re_Ressource_code):
            module += "".join(mod_regex.findall(nom))
        return module

    @cached_property
    def type(self) -> str:
        nom = self.name
        nom = nom.replace('_', ' ')
        return " ".join(ADEevent.re_Type_code.findall(nom))

    @cached_property
    def dureeJJHHMM(self) -> str:
        dureeJ = int(self.duration.total_seconds() // 86_400)
        dureeH = int((self.duration.total_seconds() - 86_400 * dureeJ) // 3600)
        dureeM = round((self.duration.total_seconds() % 3600) / 60)
        if dureeJ:
            dureeJJHHMM = str(dureeJ) + "j"
            dureeJJHHMM += str(dureeH).zfill(2) + "h" + str(dureeM).zfill(2)
        else:
            dureeJJHHMM = str(dureeH) + "h" + str(dureeM).zfill(2)
        return dureeJJHHMM

    @cached_property
    def horaireDMYHm(self) -> str:
        return self.begin.to(ADEcalendar.TimeZone).format("DD/MM/YYYY à HH:mm")

    @cached_property
    def dateYMD(self) -> str:
        return self.begin.to(ADEcalendar.TimeZone).format("YYYY/MM/DD")

    @cached_property
    def dateDMY(self) -> str:
        return self.begin.to(ADEcalendar.TimeZone).format("DD/MM/YYYY")

    @cached_property
    def heure(self) -> str:
        return self.begin.to(ADEcalendar.TimeZone).format("HH:mm")

    @cached_property
    def jour(self) -> str:
        return self.begin.to(ADEcalendar.TimeZone).format("ddd.", "fr_fr")

    @cached_property
    def mois(self) -> str:
        return self.begin.to(ADEcalendar.TimeZone).format("MMMM", "fr_fr")

    @cached_property
    def isoWeekDay(self) -> str:
        return str(self.begin.to(ADEcalendar.TimeZone).isocalendar()[2])

    @cached_property
    def isoWeekNumber(self) -> str:
        return str(self.begin.to(ADEcalendar.TimeZone).isocalendar()[1])

    @cached_property
    def fin(self) -> str:
        return self.end.to(ADEcalendar.TimeZone).format("HH:mm")

    @cached_property
    def lieu(self) -> str:
        return self.location if self.location is not None else ""

    @cached_property
    def participants(self) -> str:
        try:
            return linesep.join([s for s in self.description.splitlines()
                                 if s and not s.startswith(("(Modifié le",
                                                            "(Exported",
                                                            "(Exporté le",
                                                            "(Updated :"))]) if not None else ""
        except AttributeError:
            return ""

    @cached_property
    def participantsOneLine(self) -> str:
        return self.participants.replace('\n', ', ').replace('\r', '')

    @cached_property
    def participantsRichText(self) -> str:
        rich = "<ul><li>"
        rich += "</li><li>".join([s for s in self.description.splitlines()
                                  if s and not s.startswith(("(Modifié le:", "(Exported :"))]) if not None else ""
        rich += "</li></ul>"
        return rich

    @cached_property
    def participants_sorted(self) -> str:
        return ', '.join(sorted(self.participants.split(linesep)))

    @property
    def seance(self) -> str:
        return self._seance

    @seance.setter
    def seance(self, val):
        self._seance = val

    @property
    def color(self):
        return self._color

    @color.setter
    def color(self, val):
        self._color = val

    @cached_property
    def modifie(self) -> str:
        try:
            return str(self.last_modified.to(ADEcalendar.TimeZone).format("DD/MM/YYYY à HH:mm"))
        except AttributeError:
            return ""

    @cached_property
    def uidFamily(self) -> None | str:
        return self.getUidFamily(self.uid)

    @cached_property
    def isADEevent(self) -> bool:
        return bool(self.uidFamily)

    @property
    def comments(self) -> str:
        try:
            return self.comments_dict[self.uid]
        except KeyError:
            return ""

    @cached_property
    def clipboard_data(self) -> list[list[str]]:
        clip = [['Nom', self.nom],
                ['Type', self.type],
                ['Séance', self.seance],
                ['Module', self.module],
                ['Horaire', self.horaireDMYHm],
                ['Durée', self.dureeJJHHMM],
                ['Lieu', self.location]]
        if self.isADEevent:
            clip += [['Participants', self.participantsOneLine]]
        elif self.description:
            clip += [['Description', re.sub(self.re_Clean_html_tags, '', self.description)]]
        return clip

    @cached_property
    def clipboard_simple(self) -> str:
        return '\n'.join([' : '.join(l) for l in self.clipboard_data])

    @cached_property
    def clipboard_simple_mono(self) -> str:
        w = max([len(t[0]) for t in self.clipboard_data])
        ljust_data = [[(t[0] + ' :').ljust(w + 2), t[1]] for t in self.clipboard_data]
        return '\n'.join([' '.join(l) for l in ljust_data])

    @cached_property
    def clipboard_framed_mono(self) -> str:
        splitted = self.clipboard_simple_mono.split('\n')
        w = max([len(t) for t in splitted])
        text = '┌─' + '─' * w + '─┐\n'
        for line in splitted:
            _line = line.ljust(w)
            text += '│ ' + _line + ' │\n'
        text += '└─' + '─' * w + '─┘'
        return text

    def clipboard_html(self) -> str:
        return tabulate.tabulate(self.clipboard_data, tablefmt='html')

    def tool_tip(self) -> str:
        data = [[t[0], ':', t[1]] for t in self.clipboard_data]
        if self.comments:
            data.extend([[], ["Commentaires", ':', self.comments]])
        return tabulate.tabulate(data, tablefmt='html')

    @cached_property
    def widget_text(self) -> str:
        text = self.nom + '\n'
        if self.type:
            text += self.type + ' '
        text += (self.seance + '\n')
        text += self.lieu + '\n'
        if self.isADEevent:
            text += self.participants
        return text

    @staticmethod
    def getUidFamily(uid: str):
        """
        Récupère l'identifiant 'family' à partir de l'uid d'un événement créé par ADE.
        La famille d'un uid correspond au 3e élément en partant de la fin après découpage selon le caractère 'd'
        ADE60323032312d323032322d5543412d363636332d302d36
                                         ---------
                                          family

        ADE60323032312d32303232556e69766572736974e96465426f7572676f676e652d37373634362d31302d30
                                                                           -----------
                                                                              family
        """
        if not uid.startswith('ADE'):
            return None
        if len(uid.split('d')) < 4:
            return None
        return uid.split('d')[-3]

    @staticmethod
    def updateComment(uid, comment: str):
        if comment.strip(' \n\r'):
            ADEevent.comments_dict |= {uid: comment}
        else:
            ADEevent.comments_dict.pop(uid, None)
        with open(pM.comments_file, "w") as file:
            json.dump(ADEevent.comments_dict, file, indent=4, ensure_ascii=False)


class ADEcalendar(Calendar):
    TimeZone: str = "Europe/Paris"
    date_key_format = "YYYY-MM-DD"
    date_ADE_url_format = "YYYY-MM-DD"
    re_title_altTitle = re.compile(r"^(.*?)\s*\((.*?)\).*")
    ADE_url_path = 'jsp/custom/modules/plannings/anonymous_cal.jsp'

    def __init__(self, cache_directory: str = "", ressource_id: int | str = None, project_id: int = None,
                 first_date: Arrow = None, last_date: Arrow = None, peremption: Arrow = None, url_root: str = None,
                 extra_provider: bool = False, credentials=None, color=None, name=None):

        self.name = name
        self.color = color
        self.dataStatus = DataStatus.none

        # Dictionnaire des événements par date.
        #  -- Les clés sont des dates.
        #  -- Les valeurs sont des listes contenant les événements ayant lieu à ces mêmes dates.
        self.events_sorted_by_date: defaultdict[arrow.Arrow, list[ADEevent]] = defaultdict(list)
        # Dictionnaire des événements par nom.
        #  -- Les clés sont des noms d'événements.
        #  -- Les valeurs sont des listes contenant les événements portant ces mêmes noms.
        self.events_sorted_by_name: defaultdict[str, list[ADEevent]] = defaultdict(list)
        # Dictionnaire des événements par module.
        #  -- Les clés sont des modules.
        #  -- Les valeurs sont des listes contenant les événements portant ces mêmes noms.
        self.events_sorted_by_module: defaultdict[str, list[ADEevent]] = defaultdict(list)
        # Dictionnaire des événements par UID.
        #  -- Les clés sont les UID d'événements.
        #  -- Les valeurs sont les événements portant ces mêmes UID (un seul car un UID est unique !).
        self.events_sorted_by_uid: dict[str, ADEevent] = dict()
        # Dictionnaire des événements par famille d'uid.
        #  -- Les clés sont des familles d'uid.
        #  -- Les valeurs sont des listes contenant les événements portant ces mêmes noms.
        self.events_sorted_by_uid_family: defaultdict[str, list[ADEevent]] = defaultdict(list)

        if ressource_id is None:
            super(ADEcalendar, self).__init__()
            return

        self.url_root = url_root
        self.ressource_id = ressource_id
        self.url = self.url_root
        self.extra_provider = extra_provider
        self.cacheDirectory = cache_directory

        # Amendement de l'url si source ADE (not extra_provider)
        if not extra_provider:
            urlparsed = urllib.parse.urlparse(self.url)
            queries = {}
            if ADEcalendar.idIsDataField(str(self.ressource_id)):
                queries |= {"data": self.ressource_id}
            elif not self.extra_provider:
                urlparsed = urlparsed._replace(path=self.ADE_url_path)
                self.project_id = project_id
                self.first_date = first_date.format(self.date_ADE_url_format)
                self.lastDate = last_date.format(self.date_ADE_url_format)
                queries |= {
                    "resources": self.ressource_id,
                    "projectId": self.project_id,
                    "firstDate": self.first_date,
                    "lastDate": self.lastDate,
                    "calType": "ical"}
            query = "&".join([f"{k}={v}" for k, v in queries.items()])
            urlparsed = urlparsed._replace(query=query)
            self.url = urlparsed.geturl()

        # Construction du nom du fichier ics pour stockage en cache
        if extra_provider:
            # Le nom de fichier correspond à l'url hachée pour préserver le secret
            self.pikcleFile = (self.cacheDirectory + str(self.ressource_id) + '_' +
                               hashlib.md5(self.url.encode()).hexdigest() + '.pickle')
        else:
            # Le nom de fichier correspond au id de la ressource
            self.pikcleFile = self.cacheDirectory + str(self.ressource_id) + '.pickle'

        self.altTitle = None
        self.title = ""
        self.fileTimeStamp: arrow.Arrow = None
        self.setTitle("ADECalendar")
        self.errorMessage = ""
        if isinstance(peremption, int):
            _ref = arrow.Arrow.utcnow()
            self.peremption = _ref.shift(seconds=peremption) - _ref
        else:
            self.peremption = peremption

        self.dataStatus = DataStatus.none

        # Si le dossier n'existe pas on le crée
        if not path.exists(path.dirname(self.pikcleFile)):
            makedirs(path.dirname(self.pikcleFile))

        # Si le fichier n'existe pas ou est périmé ou est vide, on télécharge le Calendar et on écrit le fichier
        if (not path.isfile(self.pikcleFile)) or self.fileIsTooOld() or stat(self.pikcleFile).st_size == 0:
            try:
                self.download(credentials=credentials)
                self.fileTimeStamp = arrow.utcnow()
                self.dataStatus = DataStatus.downloaded
            except (requests.RequestException, UnicodeError, Exception) as e:
                print('error:', type(e), e)
                self.errorMessage = f"URL: {self.url}\n\n{e}"
                Calendar.__init__(self)
                self.dataStatus = DataStatus.failedDownload
            try:
                self.writeCache()
            except TypeError as e:
                print('error:', type(e), e)

        # Si le fichier existe, n'est pas périmé et n'est pas vide, on charge depuis le disque
        else:
            try:
                self.readCache()
                self.fileTimeStamp = arrow.get(path.getmtime(self.pikcleFile))
                self.dataStatus = DataStatus.fromDisk
            except Exception as e:
                print('error:', type(e), e)
                self.errorMessage = f"File: {self.pikcleFile}\n\n{e}"
                Calendar.__init__(self)
                self.dataStatus = DataStatus.corruptedCache

        # Si une erreur de téléchargement a eu lieu, on se rabat sur le cache,
        if self.dataStatus == DataStatus.failedDownload:
            # on force la lecture du fichier s'il existe
            try:
                self.readCache()
                self.fileTimeStamp = arrow.get(path.getmtime(self.pikcleFile))
                self.dataStatus = DataStatus.failedDownload | DataStatus.fromDisk
            # sinon,
            except:
                self.dataStatus = DataStatus.noData

        # S'il s'agit d'un agenda externe, il faut éliminer les événements hors année universitaire
        if self.extra_provider:
            self.events = set([e for e in self.timeline.overlapping(first_date.floor('day'), last_date.ceil('day'))])

        # Substitution des 'Event's par des 'ADEevent's
        self.events = set([ADEevent(event=event,
                                    color=self.color,
                                    calendar_name=self.name) for event in self.events])

        # Construction des sous-dictionnaires
        if self.dataStatus != DataStatus.noData:
            self.fillSortedEventDictionaries()

    def download(self, credentials: typing.Iterable[str] | None = None):
        get = requests.get(self.url, timeout=3, headers={"User-Agent": "Mozilla/5.0"}, auth=credentials)
        get.raise_for_status()
        content = get.content.decode(encoding='utf8', errors='replace')
        Calendar.__init__(self, content)
        self._timezones.clear()

    def readCache(self):
        with open(self.pikcleFile, 'rb') as _pickle_file:
            data = _pickle_file.read()
        # Le cache issu d'agendas personnels est déchiffré avant d'être chargé
        if self.extra_provider:
            bufferSize = 64 * 1024
            fIn = io.BytesIO(data)
            fDec = io.BytesIO()
            pyAesCrypt.decryptStream(fIn, fDec, hex(uuid.getnode()), bufferSize)
            data = fDec.getvalue()
        self.__dict__.update(pickle.loads(data).__dict__)

    def writeCache(self):
        data = pickle.dumps(self, pickle.HIGHEST_PROTOCOL)
        # Les données issues d'agendas personnels sont chiffrées avant d'être stockées en cache
        if self.extra_provider:
            bufferSize = 64 * 1024
            fIn = io.BytesIO(data)
            fCiph = io.BytesIO()
            pyAesCrypt.encryptStream(fIn, fCiph, hex(uuid.getnode()), bufferSize)
            data = fCiph.getvalue()
        with open(self.pikcleFile, 'wb') as _pickle_file:
            _pickle_file.write(data)

    def setTitle(self, title):
        """ Défines the 'ADEcalendar' title.
        If 'title' follows this format: 'xxx (yyy)',
        'xxx' will be assigned to the title et
        'yyy' to the altTitle

        Parameters
        ----------
        title: str
        """
        search = self.re_title_altTitle.search(title)
        if search:
            self.title = search.group(1)
            self.altTitle = search.group(2)
        else:
            self.title = title
            self.altTitle = title

    def fileIsTooOld(self) -> bool:
        if self.peremption is None:
            return False
        try:
            fileAge: datetime.timedelta = arrow.utcnow() - arrow.get(path.getmtime(self.pikcleFile))
            if not (self.peremption and fileAge):
                return True
            return self.peremption < fileAge
        except FileNotFoundError:
            return True

    def fillSortedEventDictionaries(self):
        """
        Ajoute les événements aux différents dictionnaires de tri.
        Chaque entrée d'un dictionnaire est une liste dont les éléments sont ajoutés un à un en maintenant
        l'ordre chronologique grâce à "bisect.insort(liste, élément)"
        (sauf self.events_sorted_by_uid qui ne contient qu'un événement par clé)
        """
        self.events_sorted_by_date.clear()
        self.events_sorted_by_name.clear()
        self.events_sorted_by_module.clear()
        self.events_sorted_by_uid.clear()
        self.events_sorted_by_uid_family.clear()
        for event in self.events:
            # Les événements créés pour durer "toute la journée" ont des begin et end "Timezone-naïves"
            if event.all_day:
                event.begin = event.begin.replace(tzinfo=ADEcalendar.TimeZone)
                event.end = event.end.replace(tzinfo=ADEcalendar.TimeZone)

            # Les événements qui s'étendent sur plusieurs jours doivent être ajoutés à toutes les dates incluses
            # entre le jour de début et le jour de fin
            _arrow = event.begin
            while True:
                bisect.insort(self.events_sorted_by_date[_arrow.format(self.date_key_format)], event)
                _arrow += datetime.timedelta(days=1)
                if _arrow.date() >= event.end.date():
                    break
            bisect.insort(self.events_sorted_by_name[event.name], event)
            if event.module:
                bisect.insort(self.events_sorted_by_module[event.module], event)
            self.events_sorted_by_uid[event.uid] = event
            if event.uidFamily:
                bisect.insort(self.events_sorted_by_uid_family[event.uidFamily], event)
            else:
                bisect.insort(self.events_sorted_by_uid_family[event.name], event)
        for family, liste in self.events_sorted_by_uid_family.items():
            if family is None:
                continue
            for num, event in enumerate(liste, start=1):
                event.seance = f"{num}/{len(liste)}"

    def firstLastDates(self):
        the_list = list(self.timeline)
        return the_list[0].begin, the_list[-1].begin

    @staticmethod
    def idIsDataField(uid: str) -> bool:
        if len(uid) > 100:
            try:
                _ = int(uid, 16)
                return True
            except ValueError:
                return False
        return False

    @classmethod
    def commonEvents(cls, *args):
        _calResult = cls()
        # S'il n'y a pas d'argument ou une liste vide, on retourne une calendrier vide
        if not args or not len(args):
            return _calResult
        # Si la liste ne contient qu'un calendrier, on le renvoit tel quel
        if len(args) == 1:
            return args[0]
        # Sinon, on cherche les événements communs (par intersection de 'sets' d'événements)
        _set_of_uid = set(args[0].events_sorted_by_uid)
        for _cal in args[1:]:
            _set_of_uid &= set(_cal.events_sorted_by_uid)

        for _uid in _set_of_uid:
            _calResult.events.add(args[0].events_sorted_by_uid[_uid])
        _calResult.fillSortedEventDictionaries()

        _title = " & ".join([cal.altTitle for cal in args])
        _calResult.setTitle(_title)
        return _calResult

    @classmethod
    def freeTime(cls, cal_list, date_debut: Arrow, date_fin: Arrow, plages_horaires_autorisees=None,
                 dates_interdites=None, granularite=15):
        if dates_interdites is None:
            dates_interdites = list()
        if plages_horaires_autorisees is None:
            plages_horaires_autorisees = dict()
        _calResult = cls()
        dict_tranchesReservees = dict()
        dict_tranchesPotentielles = OrderedDict()

        # Les éléments de la liste 'datesInterdites' peuvent être :
        #  -- des dates (arrow.Arrow)
        #  -- des tuples de dates (arrow.Arrow, arrow.Arrow) marquant le début et la fin d'une période
        # On va donc reconstituer une liste de dates uniquement en traitant les tuples.
        liste_dates_interdites = list()
        for elt in dates_interdites:
            if isinstance(elt, arrow.Arrow):
                liste_dates_interdites.append(elt)
            elif isinstance(elt, tuple):
                try:
                    liste_dates_interdites.extend(arrow.Arrow.range('day', *elt))
                except:
                    raise
            else:
                print("ERROR: 'freetime'", elt, "est du type", type(elt))

        # On dresse la liste des dates :
        #  -- comprises entre 'dateDebut' et 'dateFin'
        #  -- qui correspondent à des jours de la semaine contenant des horaires autorisés
        #  -- qui ne font pas partie des dates interdites
        dates: list[arrow.Arrow] = [date for date in arrow.Arrow.range('day', date_debut, date_fin)
                                    if (date.isoweekday() in plages_horaires_autorisees
                                        and date not in liste_dates_interdites)]

        # On crée toutes les tranches sur les dates viables sélectionnées précédemment,
        # On les place dans le dictionnaire 'dict_tranchesPotentielles' :
        #  -- les clés sont le début (Arrow)
        #  -- les valeurs sont ladite tranche: tuple(debut, fin)
        for date in dates:
            for (debutPlage, finPlage) in plages_horaires_autorisees[date.isoweekday()]:
                (debut, fin) = (date.replace(hour=debutPlage.hour, minute=debutPlage.minute),
                                date.replace(hour=finPlage.hour, minute=finPlage.minute))

                time = debut
                while time < fin:
                    dict_tranchesPotentielles[time] = [time, time + datetime.timedelta(minutes=granularite)]
                    time += datetime.timedelta(minutes=granularite)

        # On recherche toutes les tranches réservées (événements présents dans tous les
        # agendas de la liste.
        # On place l'horaire de début (Arrow) dans le dictionnaire. La clé est le debut (Arrow).
        # *** En fait, on se fiche des valeurs, seules les clés nous intéresseront. ***
        for c in cal_list:
            for e in c.events:
                d = e.begin
                while d < e.end:
                    dict_tranchesReservees[d] = d
                    d += datetime.timedelta(minutes=granularite)

        # On recherche les clés
        #  -- présentes dans 'dict_tranchesPotentielles'
        #  -- absentes  dans 'dict_tranchesReservees'
        tranchesConservees = [v for k, v in dict_tranchesPotentielles.items()
                              if k not in dict_tranchesReservees]

        _last = [Arrow(1900, 1, 1), Arrow(1900, 1, 1)]
        for tranche in tranchesConservees:
            # _calResult.events.add(e)
            if _last[1] == tranche[0]:
                _last[1] = tranche[1]
                continue
            e = ADEevent(name="Créneau libre", begin=_last[0], end=_last[1])
            _calResult.events.add(e)
            _last = tranche

        _title = "Free [" + " & ".join([cal.altTitle for cal in cal_list]) + "]"
        _calResult.setTitle(_title)
        _calResult.setTitle(_title)

        _calResult.fillSortedEventDictionaries()
        return _calResult


class EventFilter:
    def __init__(self):
        self._contentIncludeText = ""
        self._contentIncludeLogical = "any"
        self._contentExcludeText = ""
        self._contentExcludeLogical = "any"
        self._participantsIncludeText = ""
        self._participantsIncludeLogical = "any"
        self._participantsExcludeText = ""
        self._participantsExcludeLogical = "any"
        self._lieuIncludeText = ""
        self._lieuIncludeLogical = "any"
        self._lieuExcludeText = ""
        self._lieuExcludeLogical = "any"
        self._duration = datetime.timedelta(seconds=0.0)
        self._beginDate: arrow.Arrow = None
        self._endDate: arrow.Arrow = None
        self._durationLogical = ">="
        self._weekdays = list(range(6))
        self.isEnabled = True

    def setDateRange(self, begin_date: arrow.Arrow, end_date: arrow.Arrow):
        self._beginDate = begin_date
        self._endDate = end_date

    def setWeekdays(self, liste):
        self._weekdays = liste

    def setEnabled(self, enabled: bool):
        self.isEnabled = enabled

    def setContentInclude(self, string: str, logical: str):
        self._contentIncludeText = string
        self._contentIncludeLogical = logical.lower()

    def setContentExclude(self, string: str, logical: str):
        self._contentExcludeText = string
        self._contentExcludeLogical = logical.lower()

    def setParticipantsInclude(self, string: str, logical: str):
        self._participantsIncludeText = string
        self._participantsIncludeLogical = logical.lower()

    def setParticipantsExclude(self, string: str, logical: str):
        self._participantsExcludeText = string
        self._participantsExcludeLogical = logical.lower()

    def setLieuInclude(self, string: str, logical: str):
        self._lieuIncludeText = string
        self._lieuIncludeLogical = logical.lower()

    def setLieuExclude(self, string: str, logical: str):
        self._lieuExcludeText = string
        self._lieuExcludeLogical = logical.lower()

    def setDuration(self, duration: datetime.timedelta, logical: str):
        self._duration = duration
        self._durationLogical = logical.lower()

    def isAttendeesValid(self, event: ADEevent):
        _split_include = self._participantsIncludeText.split()
        if not _split_include:
            _valid_include = True
        else:
            lst = [re.search(r'\b' + unidecode(re.escape(word)), unidecode(event.participantsOneLine),
                             flags=re.IGNORECASE | re.UNICODE) for word in _split_include]
            match self._participantsIncludeLogical:
                case 'any':
                    _valid_include = any(lst)
                case 'all':
                    _valid_include = all(lst)
                case _:
                    _valid_include = True

        _split_exclude = self._participantsExcludeText.split()
        if not _split_exclude:
            _valid_exclude = False
        else:
            lst = [re.search(r'\b' + unidecode(re.escape(word)), unidecode(event.participantsOneLine),
                             flags=re.IGNORECASE | re.UNICODE) for word in _split_exclude]
            match self._participantsExcludeLogical:
                case 'any':
                    _valid_exclude = any(lst)
                case 'all':
                    _valid_exclude = all(lst)
                case _:
                    _valid_exclude = False

        return _valid_include and not _valid_exclude

    def isLocationValid(self, event: ADEevent):
        _split_include = self._lieuIncludeText.split()
        if not _split_include:
            _valid_include = True
        else:
            lst = [re.search(r'\b' + unidecode(re.escape(word)), unidecode(event.location),
                             flags=re.IGNORECASE | re.UNICODE) for word in _split_include]
            match self._lieuIncludeLogical:
                case 'any':
                    _valid_include = any(lst)
                case 'all':
                    _valid_include = all(lst)
                case _:
                    _valid_include = True

        _split_exclude = self._lieuExcludeText.split()
        if not _split_exclude:
            _valid_exclude = False
        else:
            lst = [re.search(r'\b' + unidecode(re.escape(word)), unidecode(event.location),
                             flags=re.IGNORECASE | re.UNICODE) for word in _split_exclude]
            match self._lieuExcludeLogical:
                case 'any':
                    _valid_exclude = any(lst)
                case 'all':
                    _valid_exclude = all(lst)
                case _:
                    _valid_exclude = False

        return _valid_include and not _valid_exclude

    def isContentValid(self, event: ADEevent):
        _split_include = self._contentIncludeText.split()
        if not _split_include:
            _valid_include = True
        else:
            lst = [re.search(r'\b' + unidecode(re.escape(word)), unidecode(event.name),
                             flags=re.IGNORECASE | re.UNICODE) for word in _split_include]
            match self._contentIncludeLogical:
                case 'any':
                    _valid_include = any(lst)
                case 'all':
                    _valid_include = all(lst)
                case _:
                    _valid_include = True

        _split_exclude = self._contentExcludeText.split()
        if not _split_exclude:
            _valid_exclude = False
        else:
            lst = [re.search(r'\b' + unidecode(re.escape(word)), unidecode(event.name),
                             flags=re.IGNORECASE | re.UNICODE) for word in _split_exclude]
            match self._contentExcludeLogical:
                case 'any':
                    _valid_exclude = any(lst)
                case 'all':
                    _valid_exclude = all(lst)
                case _:
                    _valid_exclude = False

        return _valid_include and not _valid_exclude

    def isDurationValid(self, event: ADEevent):
        match self._durationLogical:
            case '=':
                return event.duration == self._duration
            case '>':
                return event.duration > self._duration
            case '>=':
                return event.duration >= self._duration
            case '<':
                return event.duration < self._duration
            case '<=':
                return event.duration <= self._duration
            case _:
                return True

    def isWeekdayValid(self, event: ADEevent):
        return (event.begin.isoweekday() - 1) in self._weekdays

    def isDateValid(self, event: ADEevent):
        return self._beginDate.floor('day') <= event.begin <= self._endDate.ceil('day')

    def isValid(self, event: ADEevent):
        return self.isContentValid(event) and \
            self.isAttendeesValid(event) and \
            self.isLocationValid(event) and \
            self.isDurationValid(event) and \
            self.isWeekdayValid(event) and \
            self.isDateValid(event)
        # self.isWeekdayValid(event) and \

    @property
    def weekdays(self):
        return self._weekdays
